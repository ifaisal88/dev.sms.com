@extends('layouts.template')
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Teachers</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Teachers</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>All Teachers</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('status'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <table class="display nowrap" id="teachers">
                                <thead>
                                    <tr>
                                        <th>Employee Number</th>
                                        <th>PMDC Reg. No.</th>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Subject</th>
                                        <th>Designation</th>
                                        <th>Qualification</th>
                                        <th>Finger Print</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($teachers as $teacher)
                                        <tr>
                                            <td>{{ $teacher->employee_number ?? 'N/A' }}</td>
                                            <td>{{ $teacher->pmdc_reg_no ?? 'N/A' }}</td>
                                            <td>{{ $teacher->full_name ?? 'N/A' }}</td>
                                            <td>{{ $teacher->department->name ?? 'N/A' }}</td>
                                            <td>{{ $teacher->subject->name ?? 'N/A' }}</td>
                                            <td>{{ $teacher->designation ?? 'N/A' }}</td>
                                            <td>{{ $teacher->qualification ?? 'N/A' }}</td>
                                            <td
                                                class="{{ $teacher->finger_print ? 'badge badge-pill badge-success d-block mg-t-8' : 'badge badge-pill badge-danger d-block mg-t-8' }}">
                                                {{ $teacher->finger_print ? 'Active' : 'In-Active' }}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No Teacher to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
@section('extra-js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#teachers').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>

@endsection
