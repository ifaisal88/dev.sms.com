@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    @livewireStyles
    @livewireScripts
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Subjects</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Subjects</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->

    <!-- Class Table Area Start Here -->
    <div class="row">
        <div class="col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Add Subjects</h3>
                        </div>
                    </div>
                    @if (session()->has('status'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <form class="new-added-form" action="{{ route('subjects.submit.update', $data) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Name *</label>
                                <input type="text" placeholder="" class="form-control" name="name" id="name" value="{{ old('name', $data->name) }}">
                                @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Status *</label>
                                <select class="select2" name="status" id="status">
                                    <option value="">Please Select</option>
                                    <option value="1" {{ '1' == old("status", $data->status) ? "selected" : "" }}>Active</option>
                                    <option value="0" {{ '0' == old("status", $data->status) ? "selected" : "" }}>In-Active</option>
                                </select>
                                @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit"  class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
    <!-- Data Table Js -->
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
@endsection
