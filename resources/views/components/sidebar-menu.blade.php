<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color" style="padding-bottom: 500px">
    <div class="mobile-sidebar-header d-md-none">
        <div class="header-logo">
            <a href="{{ route('dashboard') }}"><img src="{{ asset('assets/img/logo1.png') }}" alt="logo"></a>
        </div>
    </div>
    <div class="sidebar-menu-content">
        <ul class="nav nav-sidebar-menu sidebar-toggle-view">
            <li class="nav-item">
                <a href="{{ route('dashboard') }}" class="nav-link"><i class="flaticon-dashboard"></i><span>Dashboard</span></a>
            </li>
            @if(auth()->user()->role->slug == 'super-admin')
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-settings"></i><span>Settings</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('grades.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>Classes</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('departments.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>Departments</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('sessions.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>Session</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('subjects.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>Subjects</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('gradeSubjects.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>Class-Subjects</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(auth()->user()->role->slug == 'mbbs-student-affairs' || auth()->user()->role->slug == 'super-admin' || auth()->user()->role->slug == 'bds-student-affairs')
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-user-graduate"></i><span>Students</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('students.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>All Students</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-user-md"></i><span>Teachers</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('teachers.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>All Teachers</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-graduation-cap"></i><span>Promote Students</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('students.load.students') }}" class="nav-link"><i class="fas fa-angle-right"></i>Promote</a>
                    </li>
                </ul>
            </li>
            @endif
            @if((auth()->user()->role->slug == 'bds-timetable' ||  auth()->user()->role->slug == 'mbbs-timetable') || auth()->user()->role->slug == 'super-admin')
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-calendar-alt"></i><span>Timetable</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('timetable.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>Create Timetable</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('timetable.print')}}" class="nav-link"><i class="fas fa-angle-right"></i>Print Timetable</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(auth()->user()->role->slug == 'super-admin')
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-users-cog"></i><span>Users</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('users.read.index') }}" class="nav-link"><i class="fas fa-angle-right"></i>All Users</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(auth()->user()->role->slug == 'super-admin' || auth()->user()->role->slug == 'mbbs-attendance' || auth()->user()->role->slug == 'bds-attendance' || auth()->user()->role->slug == 'admin' )
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-file-alt"></i><span>Reports</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        @if(auth()->user()->role->slug == 'super-admin' || auth()->user()->role->slug == 'admin' )
                            <a href="{{ route('reports.read.students') }}" class="nav-link"><i class="fas fa-angle-right"></i>Students</a>
                            <a href="{{ route('reports.read.teachers') }}" class="nav-link"><i class="fas fa-angle-right"></i>Teachers</a>
                            <a href="{{ route('reports.read.attendance') }}" class="nav-link"><i class="fas fa-angle-right"></i>Attendance</a>
                        @else
                            <a href="{{ route('reports.read.attendance') }}" class="nav-link"><i class="fas fa-angle-right"></i>Attendance</a>
                        @endif
                    </li>
                </ul>
            </li>
            @endif
            @if(auth()->user()->role->slug == 'super-admin')
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="fas fa-tasks"></i><span>Attendance</span></a>
                <ul class="nav sub-group-menu">
                    <li class="nav-item">
                        <a href="{{ route('grades.read.attendacne') }}" class="nav-link"><i class="fas fa-angle-right"></i>Custom Attendance</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
 </div>
