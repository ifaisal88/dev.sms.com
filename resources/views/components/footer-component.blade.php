
    <!-- jquery-->
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <!-- Plugins js -->
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <!-- Popper js -->
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <!-- Select 2 Js -->
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <!-- Date Picker Js -->
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker.en.js') }}"></script>
    <!-- Scroll Up Js -->
    <script src="{{ asset('assets/js/jquery.scrollUp.min.js') }}"></script>
    <!-- Data Table Js -->
    {{-- <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script> --}}
    <!-- Custom Js -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <!-- Timepicker Js -->
    <script src="{{ asset('assets/js/mdtimepicker.min.js') }}"></script>
    <!-- CSRF Token Global -->
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function(){
            $('.start_time').mdtimepicker();
            $('.end_time').mdtimepicker();
            $('.air-datepicker').datepicker({
                dateFormat: 'yyyy-mm-dd'
            });
        });
    </script>
</body>
</html>
