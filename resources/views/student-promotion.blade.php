@extends('layouts.template')
@section('extra-css')
<style>
    .select2-container {
        width: 100% !important;
    }
</style>
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Students</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Promote Students</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->

    <!-- Class Table Area Start Here -->
    <div class="row">
        <div class="col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Promote Student</h3>
                        </div>
                    </div>
                    <form class="new-added-form" method="post" action="{{ route('students.show.students') }}">
                        @csrf
                        <div class="row">
                            <div class="col-3 form-group">
                                <label>Department *</label>
                                <select class="select2" name="department_id" id="department_id" required>
                                    <option value="all">Select Departments</option>
                                    @foreach ($departments as $index => $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3 form-group" required>
                                <label>Current Class *</label>
                                    <select name="current_grade" id="current_grade">
                                </select>
                            </div>
                            <div class="col-3 form-group" required>
                                <label>Promote To *</label>
                                    <select name="promote_to" id="promote_to">
                                </select>
                            </div>
                            <div class="col-3 form-group">
                                <label class="hidden-label">Submit</label>
                                <button type="submit"  class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Load Students</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(isset($students) && !@empty($students))
    <div class="row">
        <div class="col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>All Students</h3>
                        </div>
                    </div>
                    <div class="table-responsive">

                    <form class="new-added-form" method="post" action="{{ route('students.update.grade') }}">
                        @csrf
                        <input type="hidden" value="{{ $current_grade }}" name="current_grade" >
                        <input type="hidden" value="{{ $promote_to }}" name="promote_to" >
                        <table class="table display data-table text-nowrap">
                        {{-- <table id="students" class="display"> --}}
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Sr</th>
                                    <th>Reg. No.</th>
                                    <th>Full Name</th>
                                    <th>Department</th>
                                    <th>Current Class</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $count = 1;
                                @endphp
                                @forelse ($students as $student)
                                <tr>
                                    <td><input type="checkbox" name="checked[]" value="{{ $student->registration_number }}" checked />&nbsp;</td>
                                    <td>{{ $count }}</td>
                                    <td>{{ $student->registration_number ?? '' }}</td>
                                    <td>{{ $student->full_name ?? '' }}</td>
                                    <td>{{ $student->department->name ?? '' }}</td>
                                    <td>{{ $student->grade->name ?? '' }}</td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @empty
                                <tr>
                                    <td colspan="4">No Student to Display</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="col-4 form-group">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Promote Students</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif()
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
<script>
    $(document).ready(function() {
        $('#current_grade').select2({
            placeholder: 'Current Class',
            ajax: {
                url: function() {
                    return "{{url('')}}"+"/departments/" + $("#department_id").val() + "/class";
                },
                dataType: 'json',
                processResults: function(data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data.items
                    };
                }
            }
        });
        $('#promote_to').select2({
            placeholder: 'Promote To',
            ajax: {
                url: function() {
                    return "{{url('')}}"+"/departments/" + $("#department_id").val() + "/class";
                },
                dataType: 'json',
                processResults: function(data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data.items
                    };
                }
            }
        });
    });
</script>
@endsection
