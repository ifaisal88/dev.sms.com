@extends('layouts.template')
@section('extra-css')
    <style>
        .select2-container {
            width: 100% !important;
        }

    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Timetable</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Timetable</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Edit Timetable</h3>
                            </div>
                        </div>

                        @if (session()->has('status'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form class="new-added-form" method="post"
                            action="{{ route('timetable.submit.update', $data->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-4 form-group">
                                    <label>Session *</label>
                                    <select class="select2" name="session_id" id="session_id">
                                        <option value="">Please Select</option>
                                        @foreach ($sessions as $session)
                                            <option value="{{ $session->id }}"
                                                {{ $session->id == old('session_id', $data->session_id) ? 'selected' : '' }}>
                                                {{ $session->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('session_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-4 form-group">
                                    <label>Day *</label>
                                    <select class="select2" name="day" id="day">
                                        <option value="">Please Select</option>
                                        <option value="Monday" {{ 'Monday' == old('day', $data->day) ? 'selected' : '' }}>
                                            Monday</option>
                                        <option value="Tuesday"
                                            {{ 'Tuesday' == old('day', $data->day) ? 'selected' : '' }}>Tuesday</option>
                                        <option value="Wednesday"
                                            {{ 'Wednesday' == old('day', $data->day) ? 'selected' : '' }}>Wednesday
                                        </option>
                                        <option value="Thursday"
                                            {{ 'Thursday' == old('day', $data->day) ? 'selected' : '' }}>Thursday
                                        </option>
                                        <option value="Friday"
                                            {{ 'Friday' == old('day', $data->day) ? 'selected' : '' }}>Friday</option>
                                        <option value="Saturday"
                                            {{ 'Saturday' == old('day', $data->day) ? 'selected' : '' }}>Saturday
                                        </option>
                                        <option value="Sunday"
                                            {{ 'Sunday' == old('day', $data->day) ? 'selected' : '' }}>Sunday</option>
                                    </select>
                                    @error('day') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-4 form-group">
                                    <label>Class *</label>
                                    <select class="select2" name="grade_id" id="grade_id" disabled>
                                        <option value="">Select Class</option>
                                        @foreach ($grades as $grade)
                                            <option value="{{ $grade->id }}"
                                                {{ $grade->id == old('grade_id', $data->grade_id) ? 'selected' : '' }}>
                                                {{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 form-group">
                                    <label>Subject *</label>
                                    <select class="select2" name="subject_id" id="subject_id">
                                        <option value="">Select Subjects</option>
                                        @foreach ($subjects as $subject)
                                            <option value="{{ $subject->id }}"
                                                {{ $subject->id == $data->subject_id ? 'selected' : '' }}>
                                                {{ $subject->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-4 form-group">
                                    <label>Teacher *</label>
                                    <select class="select2" name="teacher_id" id="teacher_id">
                                        <option value="">Select Teacher</option>
                                        @foreach ($teachers as $teacher)
                                            <option value="{{ $teacher->id }}"
                                                {{ $teacher->id == $data->teacher_id ? 'selected' : '' }}>
                                                {{ $teacher->full_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('teacher_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-4 form-group">
                                    <label for="topic_name">Topic Name</label>
                                    <textarea class="form-control" name="topic_name">{{ old('address', $data->topic_name) }}</textarea>
                                    @error('topic_name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 form-group">
                                    <label for="timepicker">Start Time</label>
                                    <input placeholder="Start Time" type="text"
                                        value="{{ old('start_time', $data->start_time) }}"
                                        class="form-control start_time" name="start_time" id="start_time">
                                    @error('start_time') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-4 form-group">
                                    <label for="timepicker">End Time</label>
                                    <input placeholder="End Time" type="text"
                                        value="{{ old('end_time', $data->end_time) }}" class="form-control end_time"
                                        name="end_time" id="end_time">
                                    @error('end_time') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-4 form-group">
                                    <label>Date *</label>
                                    <input type="text" placeholder="yyyy-mm-dd" value="{{ old('date', $data->date) }}"
                                        class="form-control air-datepicker dateTest" data-position='top right' name="date"
                                        id="date">
                                    <i class="far fa-calendar-alt"></i>
                                    @error('date') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4-xxxl col-lg-6 col-12 form-group">
                                    <label>Status *</label>
                                    <select class="select2" name="status" id="status">
                                        <option value="">Please Select</option>
                                        <option value="1" {{ 1 == old('status', $data->status) ? 'selected' : '' }}>
                                            Active</option>
                                        <option value="0" {{ 0 == old('status', $data->status) ? 'selected' : '' }}>
                                            In-Active</option>
                                    </select>
                                    @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit"
                                        class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        <script>
            var myDataPicker = $('#date').datepicker({
                language: 'en',
                dateFormat: 'yyyy-mm-dd',
                autoClose: true
            }).data('datepicker');
            myDataPicker.selectDate(new Date($('#date').val()));

            $(document).ready(function() {

            });
        </script>

    @endsection
