@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Students</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Students</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-4">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Add Students</h3>
                            </div>
                        </div>
                        <form class="new-added-form" method="post" action="{{ route('students.submit.store') }}">
                            <div class="row">
                                @csrf
                                <div class="col-12 form-group">
                                    <label for="registration_number">Registration Number *</label>
                                    <input placeholder="Registration Number" type="text"
                                        value="{{ old('registration_number', '') }}" class="form-control"
                                        name="registration_number" id="registration_number">
                                    @error('registration_number') <span
                                        class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="full_name">Full Name *</label>
                                    <input placeholder="Full Name" type="text" value="{{ old('full_name', '') }}"
                                        class="form-control" name="full_name" id="full_name">
                                    @error('full_name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Department *</label>
                                    <select class="select2" name="department_id" id="department_id">
                                        @foreach ($department as $item)
                                            <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('department_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Class *</label>
                                    <select class="select2" name="grade_id" id="grade_id">
                                        <option value="">Select Class</option>
                                        @foreach ($grades as $grade)
                                            <option value="{{ $grade->id }}"
                                                {{ $grade->id == old('grade_id', '') ? 'selected' : '' }}>
                                                {{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Mobile Number</label>
                                    <input placeholder="Mobile Number" type="text"
                                        value="{{ old('mobile_number', '') }}" class="form-control"
                                        name="mobile_number" id="mobile_number">
                                    @error('mobile_number') <span
                                        class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Address</label>
                                    <input placeholder="Address" type="text"
                                        value="{{ old('address', '') }}" class="form-control"
                                        name="address" id="address">
                                    @error('address') <span
                                        class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="remarks">Remarks</label>
                                    <textarea placeholder="Add Remarks Here" class="textarea form-control" rows="4"
                                        name="remarks" id="remarks">{{ old('remarks', '') }}</textarea>
                                    @error('remarks') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit"
                                        class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>All Students</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('status'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            {{-- <table class="table display data-table text-nowrap"> --}}
                            <table id="students" class="display nowrap">
                                <thead>
                                    <tr>
                                        <th>Reg. No.</th>
                                        <th>Full Name</th>
                                        <th>Department</th>
                                        <th>Class</th>
                                        <th>Finger Print</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($students as $student)
                                        <tr>
                                            <td>{{ $student->registration_number ?? '' }}</td>
                                            <td>{{ $student->full_name ?? '' }}</td>
                                            <td>{{ $student->department->name ?? '' }}</td>
                                            <td>{{ $student->grade->name ?? '' }}</td>
                                            <td
                                                class="{{ $student->finger_print ? 'badge badge-pill badge-success d-block mg-t-8' : 'badge badge-pill badge-danger d-block mg-t-8' }}">
                                                {{ $student->finger_print ? 'Active' : 'In-Active' }}
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-lg"
                                                    href="{{ route('students.submit.edit', $student) }}">Edit</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No Student to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        <!-- Data Table Js -->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function() {
                $('#students').DataTable({
                    "scrollX": true,
                });
            });
        </script>
    @endsection
