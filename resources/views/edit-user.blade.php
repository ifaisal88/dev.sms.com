@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    @livewireStyles
    @livewireScripts
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Users</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Users</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Add User</h3>
                            </div>
                        </div>
                        @if (session()->has('status'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form class="new-added-form" method="post"
                            action="{{ route('users.submit.update', $employee->id) }}">
                            <div class="row">
                                @csrf
                                @method('PATCH')
                                <div class="col-12 form-group">
                                    <label for="name">Full Name *</label>
                                    <input placeholder="Full Name" type="text" value="{{ old('name', $employee->name) }}"
                                        class="form-control" name="name" id="name">
                                    @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="email">Email *</label>
                                    <input placeholder="Email" type="text" value="{{ old('email', $employee->email) }}"
                                        class="form-control" name="email" id="email">
                                    @error('email') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Role *</label>
                                    <select class="select2" name="role_id" id="role_id">
                                        <option value="">Select Role</option>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}"
                                                {{ $role->id == $employee->role_id ? 'selected' : '' }}>
                                                {{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('role_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="password">Password *</label>
                                    <input placeholder="Password" type="password" value="{{ old('password', '') }}"
                                        class="form-control" name="password" id="password">
                                    @error('password') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit"
                                        class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        <!-- Data Table Js -->
        <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    @endsection
