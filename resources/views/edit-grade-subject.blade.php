@extends('layouts.template')
@section('extra-css')
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Class-Subjects</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Class-Subjects</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Class Table Area Start Here -->
    <div class="row">
    <div class="col-12">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Alot Grade Subject</h3>
                    </div>
                </div>
                @if (session()->has('status'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form class="new-added-form" action="{{ route('gradeSubjects.submit.update', $data->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Grade *</label>
                            <select class="select2" name="grade_id">
                                <option value="">Select Class</option>
                                @foreach ($grades as $grade)
                                    <option value="{{ $grade->id }}" {{ $grade->id == old("grade_id", $data->grade_id) ? "selected" : "" }}>{{ $grade->name }}</option>
                                @endforeach
                            </select>
                            @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Subject *</label>
                            <select class="select2" name="subject_id">
                                <option value="">Select Subject</option>
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}" {{ $subject->id == old("subject_id", $data->subject_id) ? "selected" : "" }}>{{ $subject->name }}</option>
                                @endforeach
                            </select>
                            @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Status *</label>
                            <select class="select2" name="status" id="status">
                                <option value="">Please Select</option>
                                <option value="1" {{ '1' == old("status", $data->status) ? "selected" : "" }}>Active</option>
                                <option value="0" {{ '0' == old("status", $data->status) ? "selected" : "" }}>In-Active</option>
                            </select>
                            @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
@endsection
