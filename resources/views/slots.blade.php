@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    @livewireStyles
    @livewireScripts
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Slots</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Slots</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->

    <!-- Class Table Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            @livewire('slots')
        </div>
    </div>
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
    <!-- Data Table Js -->
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
@endsection
