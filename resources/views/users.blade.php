@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    <style>
        .select2-container {
            width: 100% !important;
        }

    </style>
    @livewireStyles
    @livewireScripts
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Users</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Users</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-4">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Add User</h3>
                            </div>
                        </div>
                        <form class="new-added-form" method="post" action="{{ route('users.submit.store') }}">
                            <div class="row">
                                @csrf
                                <div class="col-12 form-group">
                                    <label for="name">Full Name *</label>
                                    <input placeholder="Full Name" type="text" value="{{ old('name', '') }}"
                                        class="form-control" name="name" id="name">
                                    @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="email">Email *</label>
                                    <input placeholder="Email" type="text" value="{{ old('email', '') }}"
                                        class="form-control" name="email" id="email">
                                    @error('email') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Role *</label>
                                    <select name="role_id" id="role_id">
                                    </select>
                                    @error('role_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="password">Password *</label>
                                    <input placeholder="Password" type="password" value="{{ old('password', '') }}"
                                        class="form-control" name="password" id="password">
                                    @error('password') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="password">Password Confirmation*</label>
                                    <input placeholder="Password Confirmation" type="password"
                                        value="{{ old('password_confirmation', '') }}" class="form-control"
                                        name="password_confirmation" id="password_confirmation">
                                    @error('password_confirmation') <span
                                        class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit"
                                        class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>All Users</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('status'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <table class="table display data-table text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Email Address</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($users as $user)
                                        <tr>
                                            <td>{{ $user->name ?? '' }}</td>
                                            <td>{{ $user->email ?? '' }}</td>
                                            <td>
                                                <a class="btn btn-primary btn-lg"
                                                    href="{{ route('users.submit.edit', $user) }}">Edit</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No User to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
@section('extra-js')
    <!-- Data Table Js -->
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $.ajax("{{ route('roles.read.allData') }}", {
                type: 'GET', // http method
                success: function(data, status, xhr) {
                    $('#role_id').select2({
                        placeholder: 'Select Role',
                        data: data.items
                    });
                },
                error: function(jqXhr, textStatus, errorMessage) {
                    $('#role_id').select2({
                        placeholder: 'Select Role',
                        data: []
                    });
                }
            });
        });
    </script>
@endsection
