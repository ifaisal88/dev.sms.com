@extends('layouts.template')
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <style>
        .select2-container {
            width: 100% !important;
        }

    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Teachers</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Teachers</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-4">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Add Teacher</h3>
                            </div>
                        </div>
                        <form class="new-added-form" method="post" action="{{ route('teachers.submit.store') }}">
                            <div class="row">
                                @csrf
                                <div class="col-12 form-group">
                                    <label for="employee_number">Employee Number *</label>
                                    <input placeholder="Employee Number" type="text"
                                        value="{{ old('employee_number', '') }}" class="form-control"
                                        name="employee_number" id="employee_number">
                                    @error('employee_number') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="pmdc_reg_no">PMDC Registration Number *</label>
                                    <input placeholder="PMDC Registration Number" type="text"
                                        value="{{ old('pmdc_reg_no', '') }}" class="form-control" name="pmdc_reg_no"
                                        id="pmdc_reg_no">
                                    @error('pmdc_reg_no') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="full_name">Full Name *</label>
                                    <input placeholder="Full Name" type="text" value="{{ old('full_name', '') }}"
                                        class="form-control" name="full_name" id="full_name">
                                    @error('full_name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="designation">Designation</label>
                                    <input placeholder="Designation" type="text" value="{{ old('designation', '') }}"
                                        class="form-control" name="designation" id="designation">
                                    @error('designation') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="qualification">Qualification</label>
                                    <input placeholder="Qualification" type="text" value="{{ old('qualification', '') }}"
                                        class="form-control" name="qualification" id="qualification">
                                    @error('qualification') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Mobile Number</label>
                                    <input placeholder="Mobile Number" type="text"
                                        value="{{ old('mobile_number', '') }}" class="form-control"
                                        name="mobile_number" id="mobile_number">
                                    @error('mobile_number') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Address</label>
                                    <input placeholder="Address" type="text"
                                        value="{{ old('address', '') }}" class="form-control"
                                        name="address" id="address">
                                    @error('address') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Department *</label>
                                    <select name="department_id" id="department_id" class="select2">
                                        @foreach ($department as $node)
                                            
                                        <option value="{{ $node->id }}" selected>{{ $node->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('department_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Subject *</label>
                                    <select name="subject_id" id="subject_id">
                                    </select>
                                    @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="remarks">Remarks</label>
                                    <textarea placeholder="Add Remarks Here" class="textarea form-control" rows="4"
                                        name="remarks" id="remarks">{{ old('remarks', '') }}</textarea>
                                    @error('remarks') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit"
                                        class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>All Teachers</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('status'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <table class="display nowrap" id="teachers">
                                <thead>
                                    <tr>
                                        <th>Employee Number</th>
                                        <th>PMDC Reg. No.</th>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Subject</th>
                                        <th>Designation</th>
                                        <th>Qualification</th>
                                        <th>Finger Print</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($teachers as $teacher)
                                        <tr>
                                            <td>{{ $teacher->employee_number ?? 'N/A' }}</td>
                                            <td>{{ $teacher->pmdc_reg_no ?? 'N/A' }}</td>
                                            <td>{{ $teacher->full_name ?? 'N/A' }}</td>
                                            <td>{{ $teacher->department->name ?? 'N/A' }}</td>
                                            <td>{{ $teacher->subject->name ?? 'N/A' }}</td>
                                            <td>{{ $teacher->designation ?? 'N/A' }}</td>
                                            <td>{{ $teacher->qualification ?? 'N/A' }}</td>
                                            <td
                                                class="{{ $teacher->finger_print ? 'badge badge-pill badge-success d-block mg-t-8' : 'badge badge-pill badge-danger d-block mg-t-8' }}">
                                                {{ $teacher->finger_print ? 'Active' : 'In-Active' }}
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-lg"
                                                    href="{{ route('teachers.submit.edit', $teacher) }}">Edit</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No Teacher to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
@section('extra-js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#teachers').DataTable({
                "scrollX": true,
            });

            // $.ajax("{{route('departments.read.data')}}", {
            //     type: 'GET', // http method
            //     success: function(data, status, xhr) {
            //         var department = $('#department_id').select2({
            //             placeholder: 'Select Department',
            //             data: data
            //         });
            //     },
            //     error: function(jqXhr, textStatus, errorMessage) {
            //         $('#department_id').select2({
            //             placeholder: 'Select Department',
            //             data: []
            //         });
            //     }
            // });

            $.ajax("{{route('subjects.read.data')}}", {
                type: 'GET', // http method
                success: function(data, status, xhr) {
                    $('#subject_id').select2({
                        placeholder: 'Select Subject',
                        data: data.items
                    });
                    subjects();
                },
                error: function(jqXhr, textStatus, errorMessage) {
                    $('#subject_id').select2({
                        placeholder: 'Select Subject',
                        data: []
                    });
                }
            });

            var subjects = function() {

            };
        });
    </script>

@endsection
