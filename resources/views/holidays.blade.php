@extends('layouts.template')
@section('extra-css')
    @livewireStyles
    @livewireScripts
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Holidays</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Holidays</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Class Table Area Start Here -->
        @livewire('holidays')
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
@endsection
