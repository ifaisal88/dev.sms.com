@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
    <style>
        .select2-container {
            width: 100% !important;
        }

    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Teachers</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Reports</li>
                <li>Attendance</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="col-12">
                            <div class="heading-layout1">
                                <div class="item-title">
                                    <h3>Attendance Report</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <form class="new-added-form" id="attendanceData"
                                action="{{ route('reports.read.attendanceList') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-3 form-group">
                                        <label>Department *</label>
                                        <select class="select2" name="department_id" id="department_id" required>
                                            <option value="all">Select Department</option>
                                            @foreach ($departments as $index => $department)
                                                <option value="{{ $department->id }}">{{ $department->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-3 form-group" required>
                                        <label>Classes *</label>
                                        <select class="select2" name="grade_id" id="class_id">
                                            <option value="">All Classes</option>
                                        </select>
                                    </div>
                                    <div class="col-3 form-group" required>
                                        <label>Subjects *</label>
                                        <select class="select2" name="subject_id" id="subject_id">
                                            <option value="">All Subjects</option>
                                        </select>
                                    </div>
                                    <div class="col-3 form-group">
                                        <label>From *</label>
                                        <input type="text" placeholder="yyyy-mm-dd" class=" form-control air-datepicker"
                                            data-position='top right' name="from" id="from">
                                        <input type="hidden" value="view" id="type" name="type" />
                                        <i class="far fa-calendar-alt"></i>
                                        @error('date') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-3 form-group">
                                        <label>To *</label>
                                        <input type="text" placeholder="yyyy-mm-dd" class="form-control air-datepicker"
                                            data-position='top right' name="to" id="to">
                                        <i class="far fa-calendar-alt"></i>
                                        @error('date') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="col-3 form-group" required>
                                        <label>Students</label>
                                        <select class="select2" name="students_id[]" id="students_id"></select>
                                    </div>
                                    <div class="col-3 form-group" required>
                                        <label>Attendance Status</label>
                                        <select class="select2" name="attendacne_status" id="attendacne_status">
                                            <option value="all">All</option>
                                            <option value="present">Present</option>
                                            <option value="absent">Absent</option>
                                        </select>
                                    </div>
                                    <div class="col-3 form-group mt-5">
                                        <button type="submit"
                                            class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark mt-2">Get
                                            Report</button>
                                        <button type="button" id="printReport"
                                            class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark mt-2 mb-3">Print</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area Start Here -->
        @if (isset($data))
            <div class="row">

                <div class="col-12">
                    <div class="card height-auto">
                        <div class="card-body">
                            <div class="heading-layout1">
                                <div class="item-title">
                                    <h3>Attendance</h3>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="students" class="display">
                                    <thead>
                                        <tr>
                                            <th>Registration Number</th>
                                            <th>Full Name</th>
                                            <th>Department</th>
                                            <th>Grade</th>
                                            <th>Subject</th>
                                            <th>Date / Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($data as $data)
                                            @foreach ($data->student_attendances as $attendance)
                                                <tr>
                                                    <td>{{ $data->registration_number }}</td>
                                                    <td>{{ $data->full_name }}</td>
                                                    <td>{{ $data->department->name }}</td>
                                                    <td>{{ $data->grade->name }}</td>
                                                    <td>{{ $attendance->student_timetable->subject->name }}</td>
                                                    <td>
                                                        {{ $attendance->attendance_date->toDateString() . ' - ' . $attendance->attendance_status }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @empty
                                            <tr>
                                                <td colspan="4">No Attendence to Display</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        <!-- Data Table Js -->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf8"
                src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
        </script>
        <script type="text/javascript" charset="utf8"
                src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js">
        </script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js">
        </script>
        <script type="text/javascript" charset="utf8"
                src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#students').DataTable({});
                var grades = function() {
                    $('#class_id').select2({
                        placeholder: 'Select Class',
                        ajax: {
                            url: function() {
                                return "{{ url('') }}" + "/departments/" + $("#department_id")
                                    .val() + "/class";
                            },
                            dataType: 'json',
                            processResults: function(data) {
                                // Transforms the top-level key of the response object from 'items' to 'results'
                                return {
                                    results: data.items
                                };
                            }
                        }
                    });
                };

                var subjects = function() {
                    var url = "{{ url('/') }}" + "/grades/" + $("#class_id").val() + "/subjects";
                    $('#subject_id').select2({
                        ajax: {
                            url: url,
                            processResults: function(data) {
                                // Transforms the top-level key of the response object from 'items' to 'results'
                                return {
                                    results: data.items
                                };
                            }
                        }
                    });
                };

                var students = function() {
                    $('#students_id').select2({
                        placeholder: 'Select Student(s)',
                        multiple: true,
                        ajax: {
                            url: function() {
                                return "{{ url('') }}" + "/students/grade/" + $("#class_id")
                                    .val();
                            },
                            dataType: 'json',
                            processResults: function(data) {
                                // Transforms the top-level key of the response object from 'items' to 'results'
                                return {
                                    results: data.items
                                };
                            }
                        }
                    });
                };


                $('body').on("change", "#class_id", function() {
                    subjects();
                    students();
                });

                $('body').on("change", "#department_id", function() {
                    grades();
                });

                $('body').on("click", "#printReport", function() {
                    $('#type').val('report');
                    $("#attendanceData").submit();
                });
            });
        </script>
    @endsection
