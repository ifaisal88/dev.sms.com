@extends('layouts.template')
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <style>
        .select2-container {
            width: 100% !important;
        }

    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Timetable</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Timetable</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-4">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Create Timetable</h3>
                            </div>
                        </div>

                        <form class="new-added-form" method="post" action="{{ route('timetable.submit.store') }}">
                            <div class="row">
                                @csrf
                                <div class="col-12 form-group">
                                    <label>Session *</label>
                                    <select class="select2" name="session_id" id="session_id">
                                        @foreach ($sessions as $session)
                                            <option value="{{ $session->id }}"
                                                {{ $session->id == old('session_id', '') ? 'selected' : '' }}>
                                                {{ $session->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('session_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Day *</label>
                                    <select class="select2" name="day" id="day">
                                        <option value="">Please Select</option>
                                        <option value="Monday" {{ 'Monday' == old('day', '') ? 'selected' : '' }}>Monday
                                        </option>
                                        <option value="Tuesday" {{ 'Tuesday' == old('day', '') ? 'selected' : '' }}>
                                            Tuesday</option>
                                        <option value="Wednesday" {{ 'Wednesday' == old('day', '') ? 'selected' : '' }}>
                                            Wednesday</option>
                                        <option value="Thursday" {{ 'Thursday' == old('day', '') ? 'selected' : '' }}>
                                            Thursday</option>
                                        <option value="Friday" {{ 'Friday' == old('day', '') ? 'selected' : '' }}>Friday
                                        </option>
                                        <option value="Saturday" {{ 'Saturday' == old('day', '') ? 'selected' : '' }}>
                                            Saturday</option>
                                        <option value="Sunday" {{ 'Sunday' == old('day', '') ? 'selected' : '' }}>Sunday
                                        </option>
                                    </select>
                                    @error('day') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Class *</label>
                                    <select class="select2" name="grade_id" id="grade_id">
                                        <option value="">Select Class</option>
                                        @foreach ($grades as $grade)
                                            <option value="{{ $grade->id }}"
                                                {{ $grade->id == old('grade_id', '') ? 'selected' : '' }}>
                                                {{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Subject *</label>
                                    <select class="select2" name="subject_id" id="subject_id"></select>
                                    @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Teacher *</label>
                                    <select class="select2" name="teacher_id" id="teacher_id">
                                        <option value="">Select Teacher</option>
                                        @foreach ($teachers as $teacher)
                                            <option value="{{ $teacher->id }}"
                                                {{ $teacher->id == old('teacher_id', '') ? 'selected' : '' }}>
                                                {{ $teacher->full_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('teacher_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="topic_name">Topic Name</label>
                                    <textarea class="form-control" name="topic_name">{{ old('address', '') }}</textarea>
                                    @error('topic_name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="timepicker">Start Time</label>
                                    <input placeholder="Start Time" type="text" value="{{ old('start_time', '') }}"
                                        class="form-control start_time" name="start_time" id="start_time">
                                    @error('start_time') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="timepicker">End Time</label>
                                    <input placeholder="End Time" type="text" value="{{ old('end_time', '') }}"
                                        class="form-control end_time" name="end_time" id="end_time">
                                    @error('end_time') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Date *</label>
                                    <input type="text" placeholder="yyyy-mm-dd" value="{{ old('date', '') }}"
                                        class="form-control air-datepicker" data-position='top right' name="date" id="date">
                                    <i class="far fa-calendar-alt"></i>
                                    @error('date') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                    <label>Status *</label>
                                    <select class="select2" name="status" id="status">
                                        <option value="1" {{ '1' == old('status', '') ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ '0' == old('status', '') ? 'selected' : '' }}>In-Active</option>
                                    </select>
                                    @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-8">
                {{-- <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Import Excel</h3>
                            </div>
                        </div>
                        <form class="new-added-form" method="post" action="{{ route('timetable.store.import') }}" enctype="multipart/form-data">
                            <div class="row">
                                @csrf
                                <div class="col-12 form-group">
                                    <label>Select File *</label>
                                    <input type="file" class="form-control" name="excel_file" id="file">
                                </div>
                                <div class="col-12 form-group mg-t-8 text-right">
                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Import</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> --}}

                <div class="card height-auto">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-4 heading-layout1">
                                <div class="item-title">
                                    <h3>All Timetable Enteries</h3>
                                </div>
                            </div>
                            {{-- <div class="col-8 text-right">
                                <a href="{{ route('timetable.read.export') }}"
                                    class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Import Excel</a>
                                <a href="{{ route('timetable.read.export') }}"
                                    class="btn-fill-lg bg-blue-dark btn-hover-yellow">Export Excel
                                    Template</a>
                            </div> --}}
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('status'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <table class="display" id="timetable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Class</th>
                                        <th>Subject</th>
                                        <th>Topic Name</th>
                                        <th>Teacher</th>
                                        <th>Class Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($timetables as $timetable)
                                        <tr class="timetableList">
                                            <td>{{ $timetable->date . ' ' . $timetable->day }}</td>
                                            <td>{{ $timetable->grade->name }}</td>
                                            <td>{{ $timetable->subject->name }}</td>
                                            <td>{{ $timetable->topic_name }}</td>
                                            <td>{{ $timetable->teacher->full_name }}</td>
                                            <td>{{ date('g:i a', strtotime($timetable->start_time)) . ' - ' . date('g:i a', strtotime($timetable->end_time)) }}
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-lg"
                                                    href="{{ route('timetable.submit.edit', $timetable) }}">Edit</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">No timetable to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js">
        </script>
        <script>
            $(document).ready(function() {
                if ($('.timetableList').length > 0) {
                    $('#timetable').DataTable();
                }

                $('#subject_id').select2({
                    ajax: {
                        url: function() {
                            return "{{ url('/') }}" + "/grades/" + $("#grade_id").val() + "/subjects";
                        },
                        placeholder: "Select Subject",
                        processResults: function(data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results: data.items
                            };
                        }
                    }
                });
            });
        </script>
    @endsection
