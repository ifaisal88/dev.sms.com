@extends('layouts.template')
@section('extra-css')

@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Timetable Form</h3>
        <ul>
            <li>Dashboard</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Admit Form Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title">
                    <h3>Add New Timetable</h3>
                </div>
            </div>
            <form class="new-added-form">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Class *</label>
                        <select class="select2">
                            <option value="">Select Class *</option>
                            <option value="1">1st Year MBBS</option>
                            <option value="2">1st Year BDS</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Subject *</label>
                        <select class="select2">
                            <option value="">Select Subject *</option>
                            <option value="1">Subject 1</option>
                            <option value="2">Subject 2</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Slot *</label>
                        <select class="select2">
                            <option value="">Select Slot *</option>
                            <option value="1">09:00 - 10:00</option>
                            <option value="2">10:00 - 11:00</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Teacher *</label>
                        <select class="select2">
                            <option value="">Select Teacher *</option>
                            <option value="1">Teacher 1</option>
                            <option value="2">Teacher 2</option>
                        </select>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Date *</label>
                        <input type="text" placeholder="dd/mm/yyyy" class="form-control air-datepicker"
                            data-position='bottom right'>
                        <i class="far fa-calendar-alt"></i>
                    </div>
                    <div class="col-lg-6 col-12 form-group">
                        <label>Remarks</label>
                        <textarea class="textarea form-control" name="message" id="form-message" cols="10" rows="9"></textarea>
                    </div>
                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                        <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Admit Form Area End Here -->
@endsection
@section('extra-js')

@endsection
