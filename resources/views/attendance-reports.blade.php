@php
use Carbon\CarbonPeriod;
@endphp
<x-header-component title="AIMI"></x-header-component>
<div style="width:100%; padding-top:40px; padding-bottom:40px">
    <div style="width:80%; margin:0px auto">
        <div style="width:100%; text-align:center">
            <img src="{{ asset('assets/img/logo2.jpg') }}"
                style="width: 200px; border: 1px solid; border-radius: 112px; margin-left:auto; margin-right:auto;">
        </div>
        <div style="width:100%;">
           <p><b>Department:</b> {{$department->name}}</p>
           <p><b>Class:</b> {{$class->name}}</p>
           <p><b>Subject:</b> {{$subject->name}}</p>
           <p><b>Date:</b> {{$startDate->format('Y-m-d')}} - {{$endDate->format('Y-m-d')}}</p>
        </div>
        <table class="table bs-table table-striped table-bordered text-nowrap" style="margin-top:30px;">
            <thead>
                {{-- <tr>
                    <th class="text-left">Students</th>
                    @php
                        $period = CarbonPeriod::create($startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
                    @endphp
                    @foreach ($period as $date)
                        <th>{{ $date->format('d') }}</th>
                    @endforeach
                    <th>Total Absent</th>
                    <th>Total Present</th>
                </tr> --}}
            </thead>
            <tbody>
                @foreach ($data as $student)
                    <tr>
                        <td class="text-left">{{ $student->full_name }}</td>
                        @php
                            $presentCount = $absentCount = 0;
                        @endphp
                        @foreach ($student->student_attendances as $attendance)
                            @php
                                if ($attendance->attendance_status == 'present') {
                                    $presentCount++;
                                    $status = $attendance->attendance_date->format('m-d') . '<br><i class="fas fa-check text-success"></i>';
                                } else {
                                    $absentCount++;
                                    $status = $attendance->attendance_date->format('m-d') . '<br><i class="fas fa-times text-danger"></i>';
                                }
                            @endphp
                            <td>{!! $status !!}</td>
                        @endforeach
                        <td><b>Total Absent</b> <br>{{ $absentCount }}</td>
                        <td><b>Total Present</b> <br>{{ $presentCount }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<x-footer-component></x-footer-component>
