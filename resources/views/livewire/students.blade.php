<div class="row">
    <div class="col-4">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add Students</h3>
                    </div>
                </div>
                <form class="new-added-form">
                    <div class="row">
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Registration Number *</label>
                            <input type="text" placeholder="Registration Number" class="form-control" wire:model='registration_number'>
                            @error('registration_number') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Full Name *</label>
                            <input type="text" placeholder="Full Name" class="form-control" wire:model='full_name'>
                            @error('full_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Department *</label>
                            <select class="select2" wire:model='department_id'>
                                <option value="">Select Department</option>
                                @foreach ($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                            @error('department_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Class *</label>
                            <select class="select2" wire:model='grade_id'>
                                <option value="">Select Class</option>
                                @foreach ($grades as $grade)
                                    <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                @endforeach
                            </select>
                            @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Remarks</label>
                            <textarea class="textarea form-control" cols="10" rows="9" wire:model='remarks'></textarea>
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" wire:click.prevent="store()" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Students</h3>
                    </div>
                </div>
                <div class="table-responsive">
                    @if (session()->has('message'))
                        <div class="alert alert-primary" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif

                    <table class="table display data-table text-nowrap">
                        <thead>
                            <tr>
                                <th>Reg. No.</th>
                                <th>Full Name</th>
                                <th>Department</th>
                                <th>Class</th>
                                <th>Remarks</th>
                                <th>Finger Print</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($students as $student)
                            <tr>
                                <td>{{ $student->registration_number ?? '' }}</td>
                                <td>{{ $student->full_name ?? '' }}</td>
                                <td>{{ $student->departments[0]->name ? $student->departments[0]->name : '' }}</td>
                                <td>{{ $student->grades[0]->name ? $student->grades[0]->name : '' }}</td>
                                <td>{{ $student->remarks ?? '' }}</td>
                                <td class="{{ ($student->finger_print) ? "badge badge-pill badge-success d-block mg-t-8" : "badge badge-pill badge-danger d-block mg-t-8" ?? '' }}">{{ ($student->finger_print) ? "Active" : "In-Active" ?? '' }}</td>
                                <td>
                                    <button wire:click="edit({{ $student->id ?? '' }})" class="btn btn-primary btn-lg">Edit</button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4">No Student to Display</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
