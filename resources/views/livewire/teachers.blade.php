<div class="row">
    <div class="col-4">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add Teacher</h3>
                    </div>
                </div>
                <form class="new-added-form">
                    <div class="row">
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Registration Number *</label>
                            <input type="text" placeholder="Registration Number" class="form-control" wire:model='registration_number'>
                            @error('registration_number') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>PMDC Registration Number *</label>
                            <input type="text" placeholder="PMDC Registration Number" class="form-control" wire:model='pmdc_reg_no'>
                            @error('pmdc_reg_no') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Full Name *</label>
                            <input type="text" placeholder="Full Name" class="form-control" wire:model='full_name'>
                            @error('full_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Designation *</label>
                            <input type="text" placeholder="Designation" class="form-control" wire:model='designation'>
                            @error('designation') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Qualification *</label>
                            <input type="text" placeholder="qualification" class="form-control" wire:model='qualification'>
                            @error('qualification') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" wire:click.prevent="store()" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Teachers</h3>
                    </div>
                </div>
                <div class="table-responsive">
                    @if (session()->has('message'))
                        <div class="alert alert-primary" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif

                    <table class="table display data-table text-nowrap">
                        <thead>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input checkAll">
                                        <label class="form-check-label">Sr</label>
                                    </div>
                                </th>
                                <th>Employee Number</th>
                                <th>PMDC Reg. No.</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Qualification</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($teachers as $teacher)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">#{{ $teacher->id ?? '' }}</label>
                                    </div>
                                </td>
                                <td>{{ $teacher->employee_number ?? '' }}</td>
                                <td>{{ $teacher->pmdc_reg_no ?? '' }}</td>
                                <td>{{ $teacher->full_name ?? '' }}</td>
                                <td>{{ $teacher->designation ?? '' }}</td>
                                <td>{{ $teacher->qualification ?? '' }}</td>
                                <td>
                                    <button wire:click="edit({{ $teacher->id ?? '' }})" class="btn btn-primary btn-lg">Edit</button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4">No Teacher to Display</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
