<div class="row">
    <div class="col-4">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Create Timetable</h3>
                    </div>
                </div>
                <form class="new-added-form">
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Session *</label>
                            <select class="select2" wire:model='session_id'>
                                <option value="">Please Select</option>
                                @foreach ($session as $session)
                                    <option value="{{ $session->id }}">{{ $session->name }}</option>
                                @endforeach
                            </select>
                            @error('session_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Day *</label>
                            <select class="select2" wire:model='day'>
                                <option value="">Please Select</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>
                                <option value="Sunday">Sunday</option>
                            </select>
                            @error('day') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Class *</label>
                            <select class="select2" wire:model='grade_id'>
                                <option value="">Select Class</option>
                                @foreach ($grades as $grade)
                                    <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                @endforeach
                            </select>
                            @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Subject *</label>
                            <select class="select2" wire:model='subject_id'>
                                <option value="">Select Subject</option>
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                @endforeach
                            </select>
                            @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Teacher *</label>
                            <select class="select2" wire:model='teacher_id'>
                                <option value="">Please Select</option>
                                @foreach ($teachers as $teacher)
                                    <option value="{{ $teacher->id }}">{{ $teacher->full_name }}</option>
                                @endforeach
                            </select>
                            @error('teacher_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label for="timepicker">Start Time</label>
                            <input placeholder="Selected time" type="text" class="form-control start_time"
                                wire:model='start_time'>
                            @error('start_time') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label for="timepicker">End Time</label>
                            <input placeholder="Selected time" type="text" class="form-control end_time"
                                wire:model='end_time'>
                            @error('end_time') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group">
                            <label>Date *</label>
                            <input type="text" placeholder="yyyy-mm-dd" class="form-control air-datepicker"
                                data-position='top right' wire:model='date'>

                            <i class="far fa-calendar-alt"></i>
                            @error('date') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Status *</label>
                            <select class="select2" wire:model='status'>
                                <option value="">Please Select</option>
                                <option value="1">Active</option>
                                <option value="0">In-Active</option>
                            </select>
                            @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" wire:click.prevent="store()"
                                class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    

                </div>

                <div class="table-responsive">
                    @if (session()->has('message'))
                        <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                            role="alert">
                            <div class="flex">
                                <div>
                                    {{ session('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endif
                    <table class="table display data-table text-nowrap">
                        <thead>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input checkAll">
                                        <label class="form-check-label">Sr</label>
                                    </div>
                                </th>
                                <th>Date</th>
                                <th>Class</th>
                                <th>Subject</th>
                                <th>Teacher</th>
                                <th>Class Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($timetable as $timetable)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input">
                                            <label class="form-check-label">#000{{ $timetable->id }}</label>
                                        </div>
                                    </td>
                                    <td>{{ $timetable->day . ' ' . $timetable->date }}</td>
                                    <td>{{ $timetable->grade->name }}</td>
                                    <td>{{ $timetable->subject->name }}</td>
                                    <td>{{ $timetable->teacher->full_name }}</td>
                                    <td>{{ date('g:i a', strtotime($timetable->start_time)) . ' - ' . date('g:i a', strtotime($timetable->end_time)) }}
                                    </td>
                                    <td>
                                        <button wire:click="edit({{ $timetable->id }})"
                                            class="btn btn-primary btn-lg">Edit</button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No timetable to Display</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
