<div class="heading-layout1">
    <div class="item-title">
        <h3>All Slots</h3>
    </div>
    <div class="dropdown">
        <button wire:click="create()" class="modal-trigger">Create Slot</button>
        @if($isModalOpen)
            @include('livewire.models.slot')
        @endif
    </div>
</div>
<div class="table-responsive">
    @if (session()->has('message'))
        <div class="alert alert-primary" role="alert">
            {{ session('message') }}
        </div>
    @endif

    <table class="table display data-table text-nowrap">
        <thead>
            <tr>
                <th>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input checkAll">
                        <label class="form-check-label">Sr</label>
                    </div>
                </th>
                <th>Name</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($slots as $slot)
            <tr>
                <td>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input">
                        <label class="form-check-label">#{{ $slot->id ?? '' }}</label>
                    </div>
                </td>
                <td>{{ $slot->name ?? '' }}</td>
                <td>{{ $slot->start_time ?? '' }}</td>
                <td>{{ $slot->end_time ?? '' }}</td>
                <td>{{ ($slot->status ?? '' == 1) ? 'Active' : 'In-Active' }}</td>
                <td>
                    <button wire:click="edit({{ $slot->id ?? '' }})" class="btn btn-primary btn-lg">Edit</button>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">No Slot to Display</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
