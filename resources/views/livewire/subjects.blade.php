<div class="row">
    <div class="col-4">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Add Subjects</h3>
                    </div>
                </div>
                <form class="new-added-form">
                    <div class="row">
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Name *</label>
                            <input type="text" placeholder="" class="form-control" wire:model='name'>
                            @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Status *</label>
                            <select class="select2" wire:model='status'>
                                <option value="">Please Select</option>
                                <option value="1">Active</option>
                                <option value="0">In-Active</option>
                            </select>
                            @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" wire:click.prevent="store()" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Subjects</h3>
                    </div>
                </div>
                <div class="table-responsive">
                    @if (session()->has('message'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <table class="table display data-table text-nowrap">
                        <thead>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input checkAll">
                                        <label class="form-check-label">Sr</label>
                                    </div>
                                </th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($subjects as $subject)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input">
                                        <label class="form-check-label">#{{ $subject->id ?? '' }}</label>
                                    </div>
                                </td>
                                <td>{{ $subject->name ?? '' }}</td>
                                <td>{{ ($subject->status ?? '' == 1) ? 'Active' : 'In-Active' }}</td>
                                <td>
                                    <button wire:click="edit({{ $subject->id ?? '' }})" class="btn btn-primary btn-lg">Edit</button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4">No Subject to Display</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

