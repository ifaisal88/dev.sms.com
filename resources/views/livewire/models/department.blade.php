<div class="dropdown">
    <!-- Button trigger modal -->
    <button class="modal-trigger" data-toggle="modal" data-target="#standard-modal">Create Department</button>
    <!-- Modal -->
    <div class="modal fade" id="standard-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="new-added-form">
                        <div class="row">
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Name *</label>
                                <input type="text" placeholder="Departments" class="form-control" wire:model="name">
                            </div>
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Status *</label>
                                <select class="select2" wire:model="status">
                                    <option value="">Please Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">In-Active</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" wire:click="closeModalPopover()" class="footer-btn bg-dark-low" data-dismiss="modal">Close</button>
                    <button type="button" wire:click.prevent="store()" class="footer-btn bg-linkedin">Save Changes</button>
                </div>
                    </form>
            </div>
        </div>
    </div>
</div>
