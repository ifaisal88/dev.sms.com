@extends('layouts.template')
@section('extra-css')
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Class-Subjects</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Class-Subjects</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Class Table Area Start Here -->
    <div class="row">
    <div class="col-4">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>Alot Grade Subject</h3>
                    </div>
                </div>
                <form class="new-added-form" action="{{ route('gradeSubjects.submit.store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Grade *</label>
                            <select class="select2" name="grade_id">
                                <option value="">Select Class</option>
                                @foreach ($grades as $grade)
                                    <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                @endforeach
                            </select>
                            @error('grade_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Subject *</label>
                            <select class="select2" name="subject_id">
                                <option value="">Select Subject</option>
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                @endforeach
                            </select>
                            @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12-xxxl col-lg-6 col-12 form-group">
                            <label>Status *</label>
                            <select class="select2" name="status">
                                <option value="">Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">In-Active</option>
                            </select>
                            @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card height-auto">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>All Class-Subjects</h3>
                    </div>
                </div>
                <div class="table-responsive">
                    @if (session()->has('message'))
                        <div class="alert alert-primary" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    <table class="table display data-table text-nowrap">
                        <thead>
                            <tr>
                                <th>Class</th>
                                <th>Subject</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $number = 1; ?>
                            @forelse ($data as $grade_subject)
                            <tr>
                                <td>{{ $grade_subject->grade->name ?? '' }}</td>
                                <td>{{ $grade_subject->subject->name ?? '' }}</td>
                                <td>{{ ($grade_subject->status ?? ''== 1) ? 'Active' : 'In-Active' }}</td>
                                <td>
                                    <a href="{{ route('gradeSubjects.submit.edit', $grade_subject->id) }}" class="btn btn-primary btn-lg">Edit</a>
                                </td>
                            </tr>
                            <?php $number++; ?>
                            @empty
                            <tr>
                                <td colspan="4">No Class-Subjects to Display</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
@endsection
