@php
use Carbon\CarbonPeriod;
use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html>

<head>
    <title>Table</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
</head>
<style>
    table {
        width: 1400px;
        margin: auto;
        padding: 10px;
        page-break-inside: avoid !important;
    }

    .page-break {
        page-break-after: always;
    }

</style>

<body>
    @foreach ($timetables as $weekOfyear => $days)
        @php
            $week = Carbon::now();
            $year = $week->year;
            $week->setISODate($year, $weekOfyear);
            $period = CarbonPeriod::create($week->startOfWeek()->format('Y-m-d'), $week->endOfWeek()->format('Y-m-d'));
        @endphp
        <table>
            <tbody>
                <tr>
                    <td style=" text-align: center;" colspan="2">
                        <img src="{{ asset('assets/img/logo2.jpg') }}"
                            style="width: 200px; height:200px; border: 1px solid; border-radius: 100px;">
                    </td>
                </tr>
                <tr style="border-top: 1px solid;  font-size:20px">
                    <th style="text-align: left; width: 100px;">Department:</th>
                    <td style="text-align: left;">{{ $department->name }}</td>
                </tr>
                <tr style=" font-size:20px;">
                    <th style="text-align: left;width: 100px;">Class:</th>
                    <td style="text-align: left;">{{ $grade->name }}</td>
                </tr>
                <tr style=" font-size:20px;">
                    <th style="text-align: left;">Date:</th>
                    <td style="text-align: left;">{{ $week->startOfWeek()->format('Y-m-d') }} -
                        {{ $week->endOfWeek()->format('Y-m-d') }}
                    </td>
                </tr>
                <tr>
                    <th style="padding-bottom: 30px;"></th>
                </tr>
            </tbody>

        </table>
        <table style="padding-bottom: 20px">
            <tbody>
                <tr>
                    <th style="border: 1px solid;">Serial Number</th>
                    <th style="text-align: center; border: 1px solid;  font-size:20px">Monday</th>
                    <th style="text-align: center; border: 1px solid;  font-size:20px">Tuesday</th>
                    <th style="text-align: center; border: 1px solid;  font-size:20px">Wednesday</th>
                    <th style="text-align: center; border: 1px solid;  font-size:20px">Thusday</th>
                    <th style="text-align: center; border: 1px solid;  font-size:20px">Friday</th>
                    <th style="text-align: center; border: 1px solid;  font-size:20px">Saturday</th>
                </tr>
                @for ($i = 0; $i < count($subjects); $i++)
                    <tr>
                        <td style="text-align: center; border: 1px solid;">{{ $i + 1 }}</td>
                        @foreach ($period as $day)
                            @if (isset($days[$day->format('Y-m-d')]))
                                @php
                                    $detail = $days[$day->format('Y-m-d')];
                                @endphp
                                <td style="text-align: center; border: 1px solid; font-size:20px">
                                    {{ $detail[$i]['startTime'] }} - {{ $detail[$i]['endTime'] }}<br>
                                    ({{ $detail[$i]['subject'] }})<br>
                                    {{ $detail[$i]['teacher'] }}<br>
                                    {{ $detail[$i]['date']->format('y-m-d') }}
                                </td>
                            @endif
                        @endforeach
                    </tr>
                @endfor
            </tbody>

        </table>
        <table style="page-break-after:always;"></table>
    @endforeach
</body>

</html>
