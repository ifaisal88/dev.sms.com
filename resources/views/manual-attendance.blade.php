@extends('layouts.template')
@section('extra-css')
<style>
    .select2-container {
        width: 100% !important;
    }
</style>
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Students</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Manual Attendance</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    
    @if(isset($students) && !@empty($students))
    <div class="row">
        <div class="col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Timetable Information</h3>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table display data-table text-nowrap">
                            @php
                                $timetable_id = "";
                                $timetable_date = "";
                                $attendance_marked_type = "";
                                $remarks = "";
                                $teacher_id = "";
                            @endphp
                            @foreach ($timetable as $timetable)
                            <tr>
                                <td><strong>Session:</strong> {{ $timetable->session->name }} </td>
                                <td><strong>Grade:</strong> {{ $timetable->grade->name }} </td>
                                <td><strong>Subject:</strong> {{ $timetable->subject->name }} </td>
                            </tr>
                            <tr>
                                <td><strong>Teacher:</strong> {{ $timetable->teacher->full_name }} </td>
                                <td><strong>Date:</strong> {{ $timetable->date }} </td>
                                <td><strong>Day:</strong> {{ $timetable->day }} </td>
                            </tr>
                            @php
                                $student_timetable_id = $timetable->id;
                                $timetable_date = $timetable->date;
                                $attendance_marked_type = 1;
                                $remarks = "1";
                                $teacher_id = $timetable->teacher_id;
                            @endphp
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>List of Students</h3>
                        </div>
                    </div>
                    <div class="table-responsive">
                    <form class="new-added-form" method="POST" action="{{ route('students.store.finalAttendance') }}">
                        @csrf                        
                        <input type="hidden" name="student_timetable_id" value="{{ $student_timetable_id }}" />
                        <input type="hidden" name="timetable_date" value="{{ $timetable_date }}" />
                        <input type="hidden" name="attendance_marked_type" value="{{ $attendance_marked_type }}" />
                        <input type="hidden" name="teacher_id" value="{{ $teacher_id }}" />
                        <input type="hidden" name="remarks" value="{{ $remarks }}" />
                        <table class="table display data-table text-nowrap">
                            <thead>
                                <tr>
                                    <th>Sr</th>
                                    <th>Reg. No.</th>
                                    <th>Full Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $count = 1;
                                @endphp
                                @forelse ($students as $student)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $student->registration_number ?? '' }}</td>
                                    <td>{{ $student->full_name ?? '' }}</td>
                                    <td>
                                        <div class="col-12 form-group">
                                            <select class="select2" name="{{ $count }}">
                                                <option value="">Select Status</option>
                                                <option value="present-{{$student->id}}">Present</option>
                                                <option value="absent-{{$student->id}}">Absent</option>
                                                <option value="leave-{{$student->id}}">Leave</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @empty
                                <tr>
                                    <td colspan="4">No Student to Display</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="col-4 form-group">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Submit Attendance</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif()
    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')

@endsection
