@extends('layouts.template')
@section('extra-css')
    @livewireStyles
    @livewireScripts
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Teachers</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Teachers</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>Edit Teacher</h3>
                            </div>
                        </div>

                        @if (session()->has('status'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form class="new-added-form" method="post"
                            action="{{ route('teachers.submit.update', $data->id) }}">
                            <div class="row">
                                @csrf
                                @method('PUT')
                                <div class="col-12 form-group">
                                    <label for="employee_number">Employee Number *</label>
                                    <input placeholder="Employee Number" type="text"
                                        value="{{ old('employee_number', $data->employee_number) }}" class="form-control"
                                        name="employee_number" id="employee_number">
                                    @error('employee_number') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="pmdc_reg_no">PMDC Registration Number *</label>
                                    <input placeholder="PMDC Registration Number" type="text"
                                        value="{{ old('pmdc_reg_no', $data->pmdc_reg_no) }}" class="form-control"
                                        name="pmdc_reg_no" id="pmdc_reg_no">
                                    @error('pmdc_reg_no') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="full_name">Full Name *</label>
                                    <input placeholder="Full Name" type="text"
                                        value="{{ old('full_name', $data->full_name) }}" class="form-control"
                                        name="full_name" id="full_name">
                                    @error('full_name') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="designation">Designation</label>
                                    <input placeholder="Designation" type="text"
                                        value="{{ old('designation', $data->designation) }}" class="form-control"
                                        name="designation" id="designation">
                                    @error('designation') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="qualification">Qualification</label>
                                    <input placeholder="Qualification" type="text"
                                        value="{{ old('qualification', $data->qualification) }}" class="form-control"
                                        name="qualification" id="qualification">
                                    @error('qualification') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>



                                <div class="col-12 form-group">
                                    <label>Department *</label>
                                    <select name="department_id" id="department_id" class="select2">
                                        @foreach ($department as $item)
                                            <option value="{{ $item->id }}" 
                                            {{ $item->id == old('department_id', $data->department_id) ? 'selected' : '' }}>
                                            {{ $item->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                    @error('department_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>



                                <div class="col-12 form-group">
                                    <label>Subject *</label>
                                    <select name="subject_id" id="subject_id" class="select2">
                                        @foreach ($subjects as $subject)
                                            <option value="{{ $subject->id }}"
                                                {{ $subject->id == $data->subject_id ? 'selected' : '' }}>
                                                {{ $subject->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('subject_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Mobile Number</label>
                                    <input placeholder="Mobile Number" type="text"
                                        value="{{ old('mobile_number', $data->mobile_number) }}" class="form-control"
                                        name="mobile_number" id="mobile_number">
                                    @error('mobile_number') <span
                                        class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label>Address</label>
                                    <input placeholder="Address" type="text"
                                        value="{{ old('address', $data->address) }}" class="form-control"
                                        name="address" id="address">
                                    @error('address') <span
                                        class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group">
                                    <label for="remarks">Remarks</label>
                                    <textarea placeholder="Add Remarks Here" class="textarea form-control" rows="4"
                                        name="remarks" id="remarks">{{ old('remarks', $data->remarks) }}</textarea>
                                    @error('remarks') <span class="text-red-500">{{ $message }}</span>@enderror
                                </div>
                                <div class="col-12 form-group mg-t-8">
                                    <button type="submit"
                                        class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('extra-js')
        <script>
            $(document).ready(function() {});
        </script>
    @endsection
