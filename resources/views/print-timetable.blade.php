@php
use Carbon\Carbon;
@endphp
@extends('layouts.template')
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <style>
        .select2-container {
            width: 100% !important;
        }

    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Timetable</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Timetable</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->

        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-4 heading-layout1">
                                <div class="item-title">
                                    <h3>Print Timetables</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <form class="new-added-form" action="{{ route('timetable.print.filter') }}" method="post"
                                id="printForm">
                                @csrf
                                <div class="row">
                                    <div class="col-3 form-group">
                                        <label>Department *</label>
                                        <select class="select2" name="department_id" id="department_id" required>
                                            <option value="all">Select Department</option>
                                            @foreach ($departments as $index => $department)
                                                <option value="{{ $department->id }}"
                                                    {{ $department->id == $departmentId ? 'selected' : '' }}>
                                                    {{ $department->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-3 form-group" required>
                                        <label>Classes *</label>
                                        <select class="select2" name="grade_id" id="class_id">
                                            <option value="">All Classes</option>
                                        </select>
                                        @error('grade_id') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="col-3 form-group">
                                        <label>Start Date *</label>
                                        <input type="text" placeholder="yyyy-mm-dd" class="form-control"
                                            data-position='top right' name="from" id="from" value="{{ $startDate }}" />
                                        <i class="far fa-calendar-alt"></i>
                                        @error('from') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="col-3 form-group">
                                        <label>End Date *</label>
                                        <input type="text" placeholder="yyyy-mm-dd" class="form-control"
                                            data-position='top right' name="to" id="to" value="{{ $endDate }}" />
                                        <i class="far fa-calendar-alt"></i>
                                        @error('to') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="col-12 form-group mt-1 mb-5 text-right">
                                        <input type="hidden" name="type" value="Report" id="type" />
                                        <button type="submit" class="btn-fill-lg bg-blue-dark btn-hover-yellow mt-2">Get
                                            Report</button>
                                        <button class="btn-fill-lg bg-blue-dark btn-hover-yellow" type="button"
                                            id="printTimetable">Print</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('status'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <table class="display" id="timetable">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Class</th>
                                        <th>Subject</th>
                                        <th>Teacher</th>
                                        <th>Class Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($timetables as $timetable)
                                        @php
                                            $date = new Carbon($timetable->date);
                                        @endphp
                                        <tr>
                                            <td>{{ $date->dayName . ' ' . $date->format('Y-m-d') }}
                                            </td>
                                            <td>{{ $timetable->grade->name }}</td>
                                            <td>{{ $timetable->subject->name }}</td>
                                            <td>{{ $timetable->teacher->full_name }}</td>
                                            <td>{{ date('g:i a', strtotime($timetable->start_time)) . ' - ' . date('g:i a', strtotime($timetable->end_time)) }}
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-lg"
                                                    href="{{ route('timetable.submit.edit', $timetable) }}">Edit</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">No timetable to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js">
        </script>
        @if (count($timetables) > 0)
            <script>
                $('#timetable').DataTable({
                    bSort: false
                });
            </script>
        @endif
        <script>
            $(document).ready(function() {
                var startDate = new Date("{{ $startDate }}");
                var endDate = new Date("{{ $endDate }}");
                var startDatePicker = $("#from").datepicker({
                    dateFormat: 'yyyy-mm-dd',
                    language: 'en',
                }).data('datepicker');
                startDatePicker.selectDate(startDate);

                var endDatePicker = $("#to").datepicker({
                    dateFormat: 'yyyy-mm-dd',
                    language: 'en',
                }).data('datepicker');
                endDatePicker.selectDate(endDate);

                $('#class_id').select2({
                    placeholder: 'Select Class',
                    ajax: {
                        url: function() {
                            return "{{ url('') }}" + "/departments/" + $("#department_id")
                                .val() + "/class";
                        },
                        dataType: 'json',
                        processResults: function(data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results: data.items
                            };
                        }
                    }
                });

                $("#printTimetable").click(function() {
                    $("#type").val("Print");
                    $("#printForm").submit();
                });
            });
        </script>
    @endsection
