@extends('layouts.template')
@section('extra-css')
@endsection
@section('content')
    <div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Sessions</h3>
        <ul>
            <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li>Sessions</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Class Table Area Start Here -->
    <div class="row">
        <div class="col-4">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Add Session</h3>
                        </div>
                    </div>
                    @if (session()->has('status'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <form class="new-added-form" action="{{ route('sessions.submit.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Name *</label>
                                <input type="text" placeholder="" class="form-control" name="name" value="{{ old('name') }}">
                                @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12-xxxl col-lg-6 col-12 form-group">
                                <label>Status *</label>
                                <select class="select2" name="status">
                                    <option value="">Please Select</option>
                                    <option value="1" {{ '1' == old("status", "") ? "selected" : "" }}>Active</option>
                                    <option value="0" {{ '0' == old("status", "") ? "selected" : "" }}>In-Active</option>
                                </select>
                                @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>All Sessions</h3>
                        </div>
                    </div>
                    <div class="table-responsive">
                        @if (session()->has('message'))
                            <div class="alert alert-primary" role="alert">
                                {{ session('message') }}
                            </div>
                        @endif
                        <table class="table display data-table text-nowrap">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($sessions as $session)
                                <tr>
                                    <td>{{ $session->name }}</td>
                                    <td>{{ ($session->status == 1) ? 'Active' : 'In-Active' }}</td>
                                    <td>
                                        <a href="{{ route('sessions.submit.edit', $session->id) }}" class="btn btn-primary btn-lg">Edit</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4">No Session to Display</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Class Table Area End Here -->
@endsection
@section('extra-js')
@endsection
