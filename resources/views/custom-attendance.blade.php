@extends('layouts.template')
@section('extra-css')
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    {{-- <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
    <style>
        .select2-container {
            width: 100% !important;
        }

        .datepicker {
            z-index: 1600 !important;
            /* has to be larger than 1050 */
        }
        .modal-lg{
            max-width: 1200px !important;
        }
    </style>
@endsection
@section('content')
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <h3>Classes</h3>
            <ul>
                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li>Classes Attendance</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        <!-- Class Table Area Start Here -->
        <div class="row">
            <div class="col-12">
                <div class="card height-auto">
                    <div class="card-body">
                        
                        @if (session()->has('status'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <div class="heading-layout1">
                            <div class="item-title">
                                <h3>All Classes</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            @if (session()->has('message'))
                                <div class="alert alert-primary" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif

                            <table class="table display data-table text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($grades as $grade)
                                        <tr>
                                            <td>{{ $grade->name ?? '' }}</td>
                                            <td>{{ $grade->status ?? '' == 1 ? 'Active' : 'In-Active' }}</td>
                                            <td>
                                                <button class="btn btn-primary btn-lg customAttendance"
                                                    id="student-{{ $grade->id }}">Attendance <i
                                                        class="fas fa-user-clock"></i></button>
                                                <button class="btn btn-success btn-lg finalAttendance"
                                                    id="final-{{ $grade->id }}">Complete Attendance <i
                                                        class="fas fa-user-check"></i></button>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No Class to Display</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="attendanceModal" tabindex="-1" role="dialog" aria-labelledby="attendanceModalTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <form method="POST" action="{{ route('students.store.attendance') }}">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="attendanceModalTitle">Student Attendance</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Date *</label>
                                        <input type="hidden" name="grade_id" value="" id="grade_id">
                                        <input type="hidden" name="student_timetable_id" value="" id="student_timetable_id">
                                        <input type="text" placeholder="yyyy-mm-dd" class="form-control" name="date" id="date">
                                    </div>
                                </div>
                                <div class="col-4 form-group">
                                    <label for="timepicker">Start Time</label>
                                    <input placeholder="Start Time" type="text" class="form-control start_time"
                                        name="start_time" id="start_time">
                                </div>
                                <div class="col-4 form-group">
                                    <label for="timepicker">End Time</label>
                                    <input placeholder="End Time" type="text" class="form-control end_time" name="end_time"
                                        id="end_time">
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Subject *</label>
                                        <select class="select2" name="subject_id" id="subject_id"></select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Teacher *</label>
                                        {{-- {{$teachers}} --}}
                                        <select name="teacher_id" id="teacher_id">
                                            <option value="">Select Teacher</option>
                                                @foreach ($teachers as $teacher)
                                                    <option value="{{ $teacher->id }}"
                                                        {{ $teacher->id == old('teacher_id', '') ? 'selected' : '' }}>
                                                        {{ $teacher->full_name }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="studentsList">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="finalAttendanceModal" tabindex="-1" role="dialog"
            aria-labelledby="finalAttendanceModalTitle" aria-hidden="true">
            <div class="modal-dialog">
                <form method="POST" action="{{ route('students.store.finalAttendance') }}">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="attendanceModalTitle">Finalize Attendance</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Date *</label>
                                        <input type="hidden" name="grade_id" value="" id="final_grade_id">
                                        <input type="text" placeholder="yyyy-mm-dd" class="form-control" name="date"
                                            id="final_date">
                                    </div>
                                </div>
                                <div class="col-12 form-group">
                                    <label for="timepicker">Start Time</label>
                                    <input placeholder="Start Time" type="text" class="form-control start_time"
                                        name="start_time" id="final_start_time">
                                </div>
                                <div class="col-12 form-group">
                                    <label for="timepicker">End Time</label>
                                    <input placeholder="End Time" type="text" class="form-control end_time" name="end_time"
                                        id="final_end_time">
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Subject *</label>
                                        <select class="select2" name="subject_id" id="final_subject_id"></select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Teacher *</label>
                                        
                                        <select class="select2" name="teacher_id" id="final_teacher_id">
                                            <option value="">Select Teacher</option>
                                            @foreach ($teachers as $teacher)
                                                <option value="{{ $teacher->id }}"
                                                    {{ $teacher->id == old('teacher_id', '') ? 'selected' : '' }}>
                                                    {{ $teacher->full_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-lg">Finalize Attendance</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Class Table Area End Here -->
    @endsection
    @section('extra-js')
        Data Table Js
        <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>

        <script>
            $(document).ready(function() {
                $('#teacher_id').select2({
                    dropdownParent: $('#attendanceModal')
                });
            });
            $('#date, #final_date').datepicker({
                dateFormat: 'yyyy-mm-dd',
                language: 'en',
                maxDate: new Date(),
            });
            var gradeId;
            $('.customAttendance').click(function() {
                gradeId = $(this).attr('id').split('-')[1];
                $("#attendanceModal").modal("show");
                $("#grade_id").val(gradeId);
                subjects();
            });
            $('.finalAttendance').click(function() {
                gradeId = $(this).attr('id').split('-')[1];
                $("#finalAttendanceModal").modal("show");
                $("#final_grade_id").val(gradeId);
                finalSubjects();
            });

            var subjects = function() {
                var url = "{{ url('/') }}" + "/grades/" + gradeId + "/subjects";
                $('#subject_id').select2({
                    ajax: {
                        url: url,
                        placeholder: "Select Subject",
                        processResults: function(data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results: data.items
                            };
                        }
                    }
                });
            };

            var finalSubjects = function() {
                var url = "{{ url('/') }}" + "/grades/" + gradeId + "/subjects";
                $('#final_subject_id').select2({
                    ajax: {
                        url: url,
                        placeholder: "Select Subject",
                        processResults: function(data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results: data.items
                            };
                        }
                    }
                });
            };
            
            // var teachers = function() {
            //     var url = "{{ url('/') }}" + "/subjects/" + $("#subject_id").val() + "/teachers";
            //     $('#teacher_id').select2({
            //         ajax: {
            //             url: url,
            //             placeholder: "Select Teachers",
            //             processResults: function(data) {
            //                 // Transforms the top-level key of the response object from 'items' to 'results'
            //                 return {
            //                     results: data.items
            //                 };
            //             }
            //         }
            //     });
            // };

            var finalTeachers = function() {
                var url = "{{ url('/') }}" + "/subjects/" + $("#final_subject_id").val() + "/teachers";
                $('#final_teacher_id').select2({
                    ajax: {
                        url: url,
                        placeholder: "Select Teachers",
                        processResults: function(data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results: data.items
                            };
                        }
                    }
                });
            };

            // $("body").on('change', '#subject_id', function() {
            //     teachers();
            // });

            $("body").on('change', '#final_subject_id', function() {
                finalTeachers();
            });

            // $("body").on('change', '#teacher_id', function() {
            //     console.log('In here');
            //     $("#studentsList").html("");
            //     var url = "{{ url('') }}/grades/" + gradeId + "/students";

            //     $.ajax({
            //         type: "GET",
            //         url: url,
            //         data: {
            //             date: $("#date").val(),
            //             start_time: $("#start_time").val(),
            //             end_time: $("#end_time").val(),
            //             teacher_id: $("#teacher_id").val(),
            //             subject_id: $("#subject_id").val(),
            //             grade_id: $("#grade_id").val(),
            //         },
            //         success: function(response) {

            //             var students = "";
            //             $("#student_timetable_id").val(response.student_timetable_id);
            //             if (response.data.length > 0) {
            //                 response.data.forEach(student => {
            //                     students = students +
            //                         '<div class="col-3"><div class="form-group form-check">';
            //                     if ((typeof(student.student_attendances[0]) !=
            //                             "undefined" && student.student_attendances[0] !== null)) {
            //                         students = students +
            //                             '<p>' + student.name +
            //                             ' (' +
            //                             student
            //                             .student_attendances[0]
            //                             .attendance_status + ')</p>';

            //                     } else {
            //                         students = students +
            //                             '<input type="checkbox" class="form-check-input" id="studentAttendance-' +
            //                             student.id + '" name="students[]" value="' + student
            //                             .id + '">';
            //                         students = students +
            //                             '<label class="form-check-label" for="studentAttendance-' +
            //                             student
            //                             .id + '">' + student.name + '</label>';
            //                     }


            //                     students = students + '</div></div>';
            //                 });
            //             } else {
            //                 students =
            //                     "<div class='col-12 text-center'><h4>No timetable exist for this subject and class</h4></div>";
            //             }
            //             $("#studentsList").html(students);

            //         },
            //         error: function(xhr, status, error) {
            //             console.log(xhr.statusText);
            //             var errorMessage = xhr.status + ': ' + xhr.statusText
            //             alert('Error - ' + errorMessage);
            //         }
            //     });
            // });
        </script>
    @endsection
