<x-header-component title="AIMI"></x-header-component>
<!-- Header Menu Area Start Here -->
<x-header-menu></x-header-menu>
<!-- Header Menu Area End Here -->
<!-- Page Area Start Here -->
<div class="dashboard-page-one">
    @yield('extra-css')
    <!-- Sidebar Area Start Here -->
    <x-sidebar-menu></x-sidebar-menu>
    <!-- Sidebar Area End Here -->
    @yield('content')
    <!-- Social Media End Here -->
    <x-footer-copyrights></x-footer-copyrights>
</div>
<x-footer-component></x-footer-component>
    @yield('extra-js')
