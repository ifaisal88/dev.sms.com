<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('department_id')->nullable()->constrained('departments', 'id');
            $table->foreignId('subject_id')->nullable()->constrained('subjects', 'id');
            $table->string('employee_number')->required();
            $table->string('pmdc_reg_no')->nullable()->default(null);
            $table->string('full_name')->required();
            $table->string('designation')->nullable()->default(null);
            $table->string('qualification')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('mobile_number')->nullable()->default(null);
            $table->string('remarks')->nullable()->default(null);
            $table->string('finger_print')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
