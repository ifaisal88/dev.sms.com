<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->foreignId('grade_id')->nullable()->constrained('grades')->after('id');
            $table->foreignId('department_id')->nullable()->constrained('departments')->after('grade_id');
            $table->string('registration_number')->required();
            $table->string('full_name')->required();
            $table->string('address')->nullable()->default(null);
            $table->string('mobile_number')->nullable()->default(null);
            $table->string('guardian_number')->nullable()->default(null);
            $table->string('remarks')->nullable()->default(null);
            $table->string('finger_print')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
