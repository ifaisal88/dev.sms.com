-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2021 at 06:56 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aiminew`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_statuses`
--

CREATE TABLE `attendance_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` tinyint(4) DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `parent_id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'BDS', 1, '2021-06-02 11:17:04', '2021-06-02 11:17:10', NULL),
(2, 0, 'MBBS', 1, '2021-06-02 11:17:26', '2021-06-02 11:17:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `department_id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '1st Year MBBS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(2, 2, '2nd Year MBBS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(3, 2, '3rd Year MBBS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(4, 2, '4th Year MBBS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(5, 2, 'Final Year MBBS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(6, 1, '1st Year BDS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(7, 1, '2nd Year BDS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(8, 1, '3rd Year BDS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(9, 1, 'Final Year BDS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(14, 1, 'Passout BDS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(15, 2, 'Passout MBBS', 1, '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grade_subjects`
--

CREATE TABLE `grade_subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_subjects`
--

INSERT INTO `grade_subjects` (`id`, `subject_id`, `grade_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:09', NULL),
(2, 2, 1, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(3, 3, 1, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(4, 4, 1, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(5, 1, 2, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(6, 2, 2, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(7, 3, 2, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(8, 5, 3, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(9, 6, 3, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(10, 7, 3, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(11, 8, 4, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(12, 9, 4, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(13, 10, 4, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(14, 11, 4, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(15, 12, 5, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(16, 13, 5, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(17, 14, 5, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(18, 15, 5, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(19, 3, 6, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(20, 16, 6, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(21, 17, 6, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(22, 18, 6, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(23, 20, 7, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(24, 21, 7, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(25, 22, 7, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(26, 23, 7, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(27, 24, 8, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(28, 25, 8, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(29, 26, 8, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(30, 27, 8, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(31, 28, 8, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(32, 19, 9, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(33, 29, 9, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(34, 30, 9, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL),
(35, 31, 9, 1, '2021-06-08 05:38:07', '2021-06-08 05:38:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `holiday_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE `leave_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_11_000000_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2021_03_21_135326_create_departments_table', 1),
(6, '2021_03_21_135329_create_grades_table', 1),
(7, '2021_03_21_135602_create_user_types_table', 1),
(8, '2021_03_21_135619_create_modules_table', 1),
(10, '2021_03_21_135651_create_permissions_table', 1),
(11, '2021_03_21_135704_create_subjects_table', 1),
(12, '2021_03_21_135713_create_sessions_table', 1),
(13, '2021_03_21_135728_create_attendance_statuses_table', 1),
(14, '2021_03_21_135738_create_holidays_table', 1),
(16, '2021_03_21_135927_create_grade_subjects_table', 1),
(17, '2021_03_21_135947_create_slots_table', 1),
(18, '2021_03_21_140001_create_student_timetables_table', 1),
(19, '2021_03_21_140015_create_leave_types_table', 1),
(20, '2021_03_21_140033_create_student_leaves_table', 1),
(21, '2021_03_21_140211_create_user_roles_table', 1),
(22, '2021_03_21_140226_create_user_module_permissions_table', 1),
(23, '2021_03_21_140248_create_student_attendances_table', 1),
(24, '2021_03_21_140302_create_student_departments_table', 1),
(25, '2021_03_21_140314_create_student_grades_table', 1),
(26, '2021_03_21_192627_create_user_login_histories_table', 1),
(27, '2021_03_21_135629_create_students_table', 2),
(28, '2021_03_21_135746_create_teachers_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 1, 'super-admin', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(2, 'BDS Timetable', 1, 'bds-admin', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(3, 'MBBS Timetable', 1, 'mbbs-admin', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(4, 'Registration Admin', 1, 'registration-admin', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(5, 'Admin', 1, 'admin', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2021-22', 1, '2021-07-31 14:10:53', '2021-07-31 09:57:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE `slots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `registration_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finger_print` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `grade_id`, `department_id`, `registration_number`, `full_name`, `address`, `mobile_number`, `guardian_number`, `remarks`, `finger_print`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, '2501', 'Shaifa Ilyas ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(2, 1, 2, '2502', 'Nimra Shahid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(3, 1, 2, '2503', 'Syeda Laiba  Azhar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(4, 1, 2, '2504', 'Urooj Sahibzada ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(5, 1, 2, '2505', 'Akasha Maqsood ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(6, 1, 2, '2506', 'Maheen  Sattar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(7, 1, 2, '2507', 'Mahrukh Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(8, 1, 2, '2508', 'Zain Zia ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(9, 1, 2, '2509', 'Qurat  Ul Ain Sehrish ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(10, 1, 2, '2510', 'Romaisa Ahmed  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(11, 1, 2, '2511', 'Tamana  Salam', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(12, 1, 2, '2512', 'Aliya Zain ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(13, 1, 2, '2513', 'Mehroz Gulzar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(14, 1, 2, '2514', 'Fatima Noor ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(15, 1, 2, '2515', 'Syeda Fatima Bukhari', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(16, 1, 2, '2516', 'EISHA ZAHID', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(17, 1, 2, '2517', 'Areeba Aurangzeb ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(18, 1, 2, '2518', ' Urooj Subhani', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(19, 1, 2, '2519', 'Maryam Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(20, 1, 2, '2520', 'Hafsa Noor Bibi ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(21, 1, 2, '2521', 'Almas Bukhari  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(22, 1, 2, '2522', 'Afaf Ayaz Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(23, 1, 2, '2523', 'Ghania Binte Abaid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(24, 1, 2, '2524', 'Ushna Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(25, 1, 2, '2525', 'Bisma Zahoor ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(26, 1, 2, '2526', 'Syeda Aliza Fatima ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(27, 1, 2, '2527', 'Irsa Aman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(28, 1, 2, '2528', 'Aleeza Tariq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(29, 1, 2, '2529', 'Dua  Nusrat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(30, 1, 2, '2530', 'Manahil Jehngir ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(31, 1, 2, '2531', 'Maryam Binte Imran ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(32, 1, 2, '2532', 'Maria shahid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(33, 1, 2, '2533', 'Samia Shohail ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(34, 1, 2, '2534', 'Fatima Hanifa ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(35, 1, 2, '2535', 'Masooma Ajmal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(36, 1, 2, '2536', 'Hussna Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(37, 1, 2, '2537', 'Waqas Hassan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(38, 1, 2, '2538', 'Muhammad Taimur Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(39, 1, 2, '2539', 'Ali Khan Jadoon', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(40, 1, 2, '2540', 'Uzair Ullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(41, 1, 2, '2541', 'Muhammad Ali Javaid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(42, 1, 2, '2542', 'Syed Mohiz Haider ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(43, 1, 2, '2543', 'Ahmed Tanveer ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(44, 1, 2, '2544', 'Malik Muhammad Dawood Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(45, 1, 2, '2545', 'Faizan Muhamamd ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(46, 1, 2, '2546', 'Muhammad Abubakar  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(47, 1, 2, '2547', 'M. Hamza Ali Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(48, 1, 2, '2548', 'Waqar Ahmed ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(49, 1, 2, '2549', 'Shahab Ahmed ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(50, 1, 2, '2550', 'Zarak Iqbal ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(51, 1, 2, '2551', 'M.Ahmed Sameeh ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(52, 1, 2, '2552', 'Muhammad Ibrahim ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(53, 1, 2, '2553', 'Naveed Ahmed ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(54, 1, 2, '2554', 'Muhammad Shayan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(55, 1, 2, '2555', 'Kashif Riaz ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(56, 1, 2, '2556', 'Shah Rehman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(57, 1, 2, '2557', 'Muhammad Shayan Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(58, 1, 2, '2558', 'Hafeez ur Rehman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(59, 1, 2, '2559', 'Haseeb Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(60, 1, 2, '2560', 'Muhammad Humayoun Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(61, 1, 2, '2561', 'Malik Ihsan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(62, 1, 2, '2562', 'Shayan Yousaf ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(63, 1, 2, '2563', 'Ahbab Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(64, 1, 2, '2564', 'M. Usama ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(65, 1, 2, '2565', 'Muhammad Kashif ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(66, 1, 2, '2566', 'Muhammad Zaid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(67, 1, 2, '2567', 'Aamir Raza Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(68, 1, 2, '2568', 'Abdullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(69, 1, 2, '2569', 'Shahab Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(70, 1, 2, '2570', 'Mohammad Zakriya  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(71, 1, 2, '2571', 'Muhammad Izaz Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(72, 1, 2, '2572', 'Khayyam Ali Siddiqui ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(73, 1, 2, '2573', 'Afaq Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(74, 1, 2, '2574', 'Tauqeer Nisar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(75, 1, 2, '2575', 'Moeen Rabbani ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(76, 1, 2, '2576', 'Yaseen ullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(77, 1, 2, '2577', 'Jawad Wahab ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(78, 1, 2, '2578', 'Usama Shamshad ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(79, 1, 2, '2579', 'M.Mugheer  Khan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(80, 1, 2, '2580', 'Malik Sohaib ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(81, 1, 2, '2581', 'Azlan Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(82, 1, 2, '2582', 'Khizer Hayat ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(83, 1, 2, '2583', 'Muhammad Sardar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(84, 1, 2, '2584', 'Muhammad Hasnain ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(85, 1, 2, '2585', 'Talha Ilyas khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(86, 1, 2, '2586', 'Muhammad Fawad Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(87, 1, 2, '2587', 'Muhammad Saddozai ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(88, 1, 2, '2588', 'Syed Aman e rome', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(89, 1, 2, '2589', 'Baber Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(90, 1, 2, '2590', 'Bemal Kapoor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(91, 1, 2, '2591', 'Muhammad Faiq ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(92, 1, 2, '2592', 'Abdul Qadeer ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(93, 1, 2, '2593', 'Ayaan Zahoor  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(94, 1, 2, '2594', 'Usama Sarfaraz ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(95, 1, 2, '2595', 'Yasir Arfat ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(96, 1, 2, '2596', 'Obaid Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(97, 1, 2, '2597', 'Asif Muhammad ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(98, 1, 2, '2598', 'Umar Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(99, 1, 2, '2599', 'Usman Baig', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(100, 1, 2, '2600', 'Dawood Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(101, 2, 2, '2300', 'Humaira', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(102, 2, 2, '2401', 'Khadeeja Amjad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(103, 2, 2, '2402', 'Malaika Waheed Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(104, 2, 2, '2403', 'Aymen Aman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(105, 2, 2, '2404', 'Samreen Fatima', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(106, 2, 2, '2405', 'Natasha bibi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(107, 2, 2, '2406', 'Rimsha Amin', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(108, 2, 2, '2407', 'Mahnoor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(109, 2, 2, '2408', 'Sandal Rafiq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(110, 2, 2, '2409', 'Yusra', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(111, 2, 2, '2410', 'Anoosha', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(112, 2, 2, '2411', 'Jaweria Ahmad Khattak', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(113, 2, 2, '2412', 'Kiran Bashir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(114, 2, 2, '2413', 'Kashmala  Ali Yousafzai', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(115, 2, 2, '2414', 'Aleeza Habib', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(116, 2, 2, '2415', 'Safa Khalid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(117, 2, 2, '2416', 'Hina Gul', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(118, 2, 2, '2417', 'Makhzoona Khattak', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(119, 2, 2, '2418', 'Zara Khalid Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(120, 2, 2, '2419', 'Ayesha Ihsan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(121, 2, 2, '2420', 'Rabia Qahar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(122, 2, 2, '2421', 'Iqra Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(123, 2, 2, '2422', 'Muniba Sami', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(124, 2, 2, '2423', 'Momna Khan Tanoli', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(125, 2, 2, '2424', 'Urooj farooqi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(126, 2, 2, '2425', 'Talbia Arif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(127, 2, 2, '2426', 'Ayesha Arooj', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(128, 2, 2, '2427', 'Javeria Kamil', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(129, 2, 2, '2428', 'Malaika Noor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(130, 2, 2, '2429', 'Shakra Aneesa Javed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(131, 2, 2, '2430', 'Syeda Rija Umair', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(132, 2, 2, '2431', 'Laveeza Sultan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(133, 2, 2, '2432', 'Maryam Safdar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(134, 2, 2, '2433', 'Nishwa Tul Eman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(135, 2, 2, '2434', 'Hoor Qazi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(136, 2, 2, '2435', 'Summyia Rafique', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(137, 2, 2, '2436', 'Sadaf Rani', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(138, 2, 2, '2437', 'Eman Akbar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(139, 2, 2, '2438', 'Tooba Samavia Qasim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(140, 2, 2, '2439', 'Rukhma Yaqoob', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(141, 2, 2, '2440', 'Esha Kumari', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(142, 2, 2, '2441', 'Maha Malik', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(143, 2, 2, '2442', 'Khadija Noor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(144, 2, 2, '2443', 'Izza Noor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(145, 2, 2, '2444', 'Hira Bibi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(146, 2, 2, '2445', 'Moqadis Asghar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(147, 2, 2, '2446', 'Shahab Gul', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(148, 2, 2, '2447', 'Muhammad Haris', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(149, 2, 2, '2448', 'Abdal Ahmad Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(150, 2, 2, '2449', 'Mansoor Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(151, 2, 2, '2450', 'Qazi Arshad Farid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(152, 2, 2, '2451', 'Sabir Hussain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(153, 2, 2, '2452', 'Muhammad Umar Saif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(154, 2, 2, '2453', 'Muhammad Zargham Haider', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(155, 2, 2, '2454', 'Afaq Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(156, 2, 2, '2455', 'Khalid Yousaf', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(157, 2, 2, '2456', 'Akhlaq Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(158, 2, 2, '2457', 'Muhammad Hashim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(159, 2, 2, '2458', 'Moeen Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(160, 2, 2, '2459', 'Malik Muhammad Umair', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(161, 2, 2, '2460', 'Muhammad Shabbir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(162, 2, 2, '2461', 'Waliullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(163, 2, 2, '2462', 'Qaisar Zaman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(164, 2, 2, '2463', 'Muhammad usama', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(165, 2, 2, '2464', 'Yahya Basharat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(166, 2, 2, '2465', 'Muhammad Asif Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(167, 2, 2, '2466', 'Salman Khan khattak', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(168, 2, 2, '2467', 'Mohsin Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(169, 2, 2, '2468', 'Muhammad Muneeb Zia Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(170, 2, 2, '2469', 'Muhammad Uzair', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(171, 2, 2, '2470', 'Syed Suleman Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(172, 2, 2, '2471', 'Hasnain Akhtar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(173, 2, 2, '2472', 'Ihtesham Naseer', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(174, 2, 2, '2473', 'Qazi Faizan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(175, 2, 2, '2474', 'Syed Muhammad Anees Badshah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(176, 2, 2, '2475', 'Taufeeq Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(177, 2, 2, '2476', 'Muhammad Hassan Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(178, 2, 2, '2477', 'Muhammad Asim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(179, 2, 2, '2478', 'Abdur Rauf', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(180, 2, 2, '2479', 'Hamza Zubair', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(181, 2, 2, '2480', 'Muhammad Adnan Sani', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(182, 2, 2, '2481', 'Umar Saleem', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(183, 2, 2, '2482', 'Muhammad Ibaad Ur  Rehman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(184, 2, 2, '2483', 'Muhammad irfan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(185, 2, 2, '2484', 'Asim ur Rahman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(186, 2, 2, '2485', 'Abdul Hayee', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(187, 2, 2, '2486', 'Ghayoor Izzat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(188, 2, 2, '2487', 'Shakir Ali Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(189, 2, 2, '2488', 'Kamran ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(190, 2, 2, '2489', 'Ahmad Mustafa Khalid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(191, 2, 2, '2490', 'Taqweem Ul Haq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(192, 2, 2, '2491', 'Muhammad Awais', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(193, 2, 2, '2492', 'Umer Hilal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(194, 2, 2, '2493', 'Abdul Basit', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(195, 2, 2, '2494', 'Osama Hidayat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(196, 2, 2, '2495', 'Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(197, 2, 2, '2496', 'Muhammad Qasim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(198, 2, 2, '2497', 'Assad Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(199, 2, 2, '2498', 'Arshad Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(200, 2, 2, '2499', 'Hassan Ahmed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(201, 2, 2, '2500', 'Ikhtiar Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(202, 3, 2, '2023', 'Bibi Fatima', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(203, 3, 2, '2056', 'Mashal Tanveer', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(204, 3, 2, '2106', 'Muhammad Asim Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(205, 3, 2, '2219', 'Zunera Hassan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(206, 3, 2, '2299', 'Ijaz Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(207, 4, 2, '2201', 'Mahnoor   ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(208, 4, 2, '2202', 'Rimsha Farooq  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(209, 4, 2, '2203', 'Fouzia Kalsoom', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(210, 4, 2, '2204', 'Zohaa Zulfiqar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(211, 4, 2, '2205', 'Marina Khan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(212, 4, 2, '2206', 'Nimra Malik', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(213, 4, 2, '2207', 'Somia Anwar  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(214, 4, 2, '2208', 'Sana Khattak ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(215, 4, 2, '2209', 'Sabiha Nasir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(216, 4, 2, '2210', 'Laiba Khan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(217, 4, 2, '2211', 'Wajeeha Inayat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(218, 4, 2, '2212', 'Hajra ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(219, 4, 2, '2213', 'Sara Mumtaz Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(220, 4, 2, '2214', 'Alveena Natasha', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(221, 4, 2, '2215', 'Hira Iqbal ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(222, 4, 2, '2216', 'Mahrukh ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(223, 4, 2, '2217', 'Lubaba Zulfiqar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(224, 4, 2, '2218', 'Huma Ishaq  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(225, 4, 2, '2221', 'Zainab Nasrulllah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(226, 4, 2, '2222', 'Mahnoor Malik  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(227, 4, 2, '2224', 'Mahnoor Fatima ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(228, 4, 2, '2225', 'Hafsa Karim ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(229, 4, 2, '2226', 'Saeeda Bibi ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(230, 4, 2, '2227', 'Kalsoom', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(231, 4, 2, '2228', 'Shawal Fatima ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(232, 4, 2, '2229', 'Fatima', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(233, 4, 2, '2230', 'Mahnoor Aamir   ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(234, 4, 2, '2231', 'Sana Salim  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(235, 4, 2, '2232', 'Shifa Jonathan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(236, 4, 2, '2233', 'Madiha Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(237, 4, 2, '2234', 'Komal Shehzad  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(238, 4, 2, '2235', 'Laiba Marooj Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(239, 4, 2, '2236', 'Malik Muzaffar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(240, 4, 2, '2237', 'Hamza Anwar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(241, 4, 2, '2238', 'M.Haris Shabbir ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(242, 4, 2, '2239', 'Faraz Ali Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(243, 4, 2, '2240', ' Hamza Khan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(244, 4, 2, '2241', 'Muhammad Waheed  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(245, 4, 2, '2242', 'Sibghat Ullah  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(246, 4, 2, '2243', 'Muhammad Hamza Tariq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(247, 4, 2, '2244', 'Zayab Waheed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(248, 4, 2, '2245', 'Abdul Nasir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(249, 4, 2, '2246', 'Hamza Saleh ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(250, 4, 2, '2247', 'Waleed Ishtiaq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(251, 4, 2, '2248', 'Sikandar Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(252, 4, 2, '2249', 'Khizar Rashid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(253, 4, 2, '2250', 'Muhammad Ibrahim Masood', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(254, 4, 2, '2251', 'Riaz Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(255, 4, 2, '2253', 'Muhammad Adeel Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(256, 4, 2, '2254', 'Muhammad Ahsan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(257, 4, 2, '2257', ' Muhammad Shahzad ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(258, 4, 2, '2258', 'Hasnat Zafar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(259, 4, 2, '2259', 'Shahab Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(260, 4, 2, '2260', 'Hazrat Umar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(261, 4, 2, '2261', 'Hassan Jawed  Shehryar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(262, 4, 2, '2262', 'Yasir Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(263, 4, 2, '2263', 'Muhammad Haris Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(264, 4, 2, '2264', 'Wasee Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(265, 4, 2, '2265', 'Owais Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(266, 4, 2, '2266', 'Syed Talha Shahzad ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(267, 4, 2, '2267', 'Arshan Azeem ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(268, 4, 2, '2268', 'Abdul Rashid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(269, 4, 2, '2269', 'Muhammad Umar Nayab  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(270, 4, 2, '2270', 'M. Zeeshan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(271, 4, 2, '2271', 'Moin Ud din', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(272, 4, 2, '2272', ' M. Ameer Hamza ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(273, 4, 2, '2273', 'M. Tayyab ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(274, 4, 2, '2274', 'Ghayoor Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(275, 4, 2, '2275', 'Shoaib-Ur-Rehman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(276, 4, 2, '2276', 'Ahsan Iqbal ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(277, 4, 2, '2277', 'Muhammad Khayam', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(278, 4, 2, '2278', 'Nafees ur  Rehman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(279, 4, 2, '2279', 'Zeeshan Iqbal ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(280, 4, 2, '2280', 'Malik Fawad Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(281, 4, 2, '2281', 'Malik Imran Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(282, 4, 2, '2282', 'Muhammad Ilyas ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(283, 4, 2, '2283', 'Syed Shah Zeb Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(284, 4, 2, '2284', 'Faridullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(285, 4, 2, '2285', 'Hamza Hamayun Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(286, 4, 2, '2286', 'Usama Hamayoun Khan  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(287, 4, 2, '2287', 'Muhammad Hamza Javed ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(288, 4, 2, '2288', 'Tauseef Ullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(289, 4, 2, '2289', 'Syed Hamza Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(290, 4, 2, '2290', 'Abdul Moiz Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(291, 4, 2, '2291', 'Faizan Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(292, 4, 2, '2292', 'Hussain Nawaz Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(293, 4, 2, '2293', 'Shehzad Sajid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(294, 4, 2, '2294', 'Fahad Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(295, 4, 2, '2296', 'M. Ahsan Jamil ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(296, 4, 2, '2297', 'M. Mohsin Jamil ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(297, 4, 2, '2298', 'Raja Obaid ur Rehman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(298, 4, 2, '2008', 'Hifza Naz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(299, 4, 2, '2009', 'Ayesha Farooq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(300, 4, 2, '2015', 'Hafiza Mariam', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(301, 4, 2, '2016', 'Ayman Ali ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(302, 4, 2, '2017', 'Waqar un Nisa', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(303, 4, 2, '2022', 'Shiza Rafique', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(304, 4, 2, '2029', 'Haleema Sharif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(305, 4, 2, '2036', 'Anees Ahmed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(306, 4, 2, '2038', 'M. Siddique', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(307, 4, 2, '2044', 'Waleed Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(308, 4, 2, '2046', 'Uzair Salam', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(309, 4, 2, '2047', 'Syed Saad Zakir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(310, 4, 2, '2049', 'Aizaz Rashid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(311, 4, 2, '2057', 'Muhammad Kashif Babar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(312, 4, 2, '2061', 'Awais Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(313, 4, 2, '2077', 'Taimoor Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(314, 4, 2, '2082', 'Hizbullah Khan Qureshi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(315, 4, 2, '1818', 'Zahwa', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(316, 4, 2, '1820', 'Aliya Bakhsh', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(317, 4, 2, '1852', 'Mujahid Mumtaz Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(318, 4, 2, '1890', 'Mamoon Rashid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(319, 4, 2, '1896', 'Haris Aizaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(320, 4, 2, '2019', 'Marya Bibi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(321, 4, 2, '2033', 'Khalil Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(322, 4, 2, '2052', 'M. Bilal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(323, 4, 2, '2065', 'Muhammad Awais', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(324, 4, 2, '2067', 'Muhammad Ahmed Iftikhar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(325, 4, 2, '2090', 'Naveed Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(326, 4, 2, '2096', 'Haris Jan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(327, 4, 2, '2101', 'Muhammad Ali Nouman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(328, 4, 2, '2102', 'Uzair Ali Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(329, 4, 2, '1826', 'Rabia Wajid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(330, 4, 2, '1837', 'Zubaida Khanum ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(331, 4, 2, '1840', 'Mohammad Asad Abbas ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(332, 4, 2, '1847', 'Umer Abbas', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(333, 4, 2, '1849', 'Ubaid Ur Rehman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(334, 4, 2, '1855', 'Waqas Ahmad Burki', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(335, 4, 2, '1857', 'Muhammad Nauman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(336, 4, 2, '1873', 'Muhammad Bacha Ghani ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(337, 4, 2, '1878', 'Irfan Ullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(338, 4, 2, '1892', 'Saffiullah Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(339, 4, 2, '1894', 'Mudasar Islam Bajwa ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(340, 4, 2, '1900', 'Abbas Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(341, 4, 2, '1916', 'Aamir Ali Jehangir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(342, 4, 2, '1657', 'Waseem Gohar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(343, 4, 2, '1665', 'Syed Faiz Mujtaba', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(344, 4, 2, '1697', 'M. Salman Abid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(345, 4, 2, '1415', 'Ayesha', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(346, 4, 2, '745', 'Sadia Afzal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(347, 5, 2, '2001', 'Iqra Riaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(348, 5, 2, '2002', 'Tehmina Bashir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(349, 5, 2, '2003', 'Mahnoor Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(350, 5, 2, '2004', 'Maira Bashir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(351, 5, 2, '2005', 'Anita Bibi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(352, 5, 2, '2006', 'Kainat Imtiaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(353, 5, 2, '2007', 'Fajar Saleem Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(354, 5, 2, '2010', 'Pakiza Rafiq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(355, 5, 2, '2011', 'Nazish ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(356, 5, 2, '2012', 'Adeela Urooj', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(357, 5, 2, '2013', 'Muzdalifa', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(358, 5, 2, '2014', 'Saveera Parkash', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(359, 5, 2, '2018', 'Momina Abdul Karim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(360, 5, 2, '2020', 'Rysaeva Safia ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(361, 5, 2, '2021', 'Fareeha Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(362, 5, 2, '2024', 'Sumbal Haleem', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(363, 5, 2, '2025', 'Zahra Abbasi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(364, 5, 2, '2026', 'Fatima Abbasi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(365, 5, 2, '2027', 'Masooma Atta', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(366, 5, 2, '2028', 'Amina Qureshi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(367, 5, 2, '2030', 'Kiran Aziz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(368, 5, 2, '2031', 'Hamza Akbar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(369, 5, 2, '2032', 'Zain Ul Abdeen', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(370, 5, 2, '2034', 'Fazal Amin', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(371, 5, 2, '2035', 'Muhammad Sohail Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(372, 5, 2, '2037', 'Muhammad Suleman Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(373, 5, 2, '2039', 'Azlan Qaisar Afridi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(374, 5, 2, '2040', 'Abdullah Altaf', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(375, 5, 2, '2041', 'Muhammad Awais Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(376, 5, 2, '2042', 'Syed Hamza Ali Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(377, 5, 2, '2043', 'Ismail Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(378, 5, 2, '2045', 'Fazal Malik', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(379, 5, 2, '2048', 'Kamran Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(380, 5, 2, '2050', 'Abdul Hanan Malik', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(381, 5, 2, '2051', 'Muhammad Saghir ul Hassan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(382, 5, 2, '2053', 'Syed Mueez Ali Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(383, 5, 2, '2054', 'Muhammad Asad Mehmood', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(384, 5, 2, '2055', 'Zia-ur-Rahim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(385, 5, 2, '2058', 'Wajid Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(386, 5, 2, '2059', 'Hassan Mansoor Awan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(387, 5, 2, '2060', 'Abdullah Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(388, 5, 2, '2062', 'Waris Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(389, 5, 2, '2063', 'Anwar Ali Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(390, 5, 2, '2064', 'Azaz Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(391, 5, 2, '2066', 'Ali Bashir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(392, 5, 2, '2068', 'Khalil Ahmad Hashmi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(393, 5, 2, '2069', 'Mohsin Rasheed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(394, 5, 2, '2070', 'Aqeel Baseer', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(395, 5, 2, '2071', 'Syed Haseeb Hassan Bukhari', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(396, 5, 2, '2072', 'Nisarzada', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(397, 5, 2, '2073', 'Adil Nawaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(398, 5, 2, '2074', 'Waqas Ahmed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(399, 5, 2, '2075', 'Muhammad Talha Naveed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(400, 5, 2, '2076', 'Usama Ubaid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(401, 5, 2, '2078', 'Basit Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(402, 5, 2, '2079', 'M. Mansoor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(403, 5, 2, '2080', 'Muhammad Ahsan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(404, 5, 2, '2081', 'Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(405, 5, 2, '2083', 'Syed Khawer Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(406, 5, 2, '2084', 'Muhammad Musab Bilal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(407, 5, 2, '2085', 'Qazi Nouman Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(408, 5, 2, '2086', 'Javed-ur-Rahman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(409, 5, 2, '2087', 'Muhammad Osama', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(410, 5, 2, '2088', 'Abdullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(411, 5, 2, '2089', 'Irfan Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(412, 5, 2, '2091', 'Fraz Ahmed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(413, 5, 2, '2092', 'Oussama Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(414, 5, 2, '2093', 'Mian Muhammad Jalal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(415, 5, 2, '2094', 'Hamza Amjad Ali', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(416, 5, 2, '2095', 'Muhammad Usman Khalid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(417, 5, 2, '2097', 'Roshan Nazik', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(418, 5, 2, '2098', 'Muhammad Ibrahim Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(419, 5, 2, '2099', 'Zarrar Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(420, 5, 2, '2100', 'Umair Amjad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(421, 5, 2, '2103', 'Ubaid Ullah Khan Khalil', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(422, 5, 2, '2104', 'Muhammad Usman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(423, 5, 2, '2105', 'Sajjad Ameer Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(424, 5, 2, '1803', 'Sheery Qureshi ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(425, 5, 2, '1806', 'Saeeda ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(426, 5, 2, '1808', 'Zainab Iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(427, 5, 2, '1816', 'Urooj Fatima ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(428, 5, 2, '1817', 'Hoor-ul-Ain Raja', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(429, 5, 2, '1824', 'Amna Binte Ashraf ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(430, 5, 2, '1828', 'Atiqa Khalid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(431, 5, 2, '1832', 'Aqsa Anwar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(432, 5, 2, '1835', 'Sana ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(433, 5, 2, '1854', 'Kamran Hassan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(434, 5, 2, '1856', 'Abdullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(435, 5, 2, '1865', 'Waqar Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(436, 5, 2, '1884', 'Hashim Younas', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(437, 5, 2, '1886', 'Syed Mehroze Ali Shah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(438, 5, 2, '1904', 'M. Imran Ullah Turabi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(439, 5, 2, '1912', 'Waheedullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(440, 5, 2, '1631', 'Iqra Sarwar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(441, 5, 2, '1648', 'Sajjad Hassan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(442, 5, 2, '1654', 'Asif Nawaz Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(443, 5, 2, '1438', 'Haq Nawaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(444, 5, 2, '1440', 'Faizan Saleem', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(445, 5, 2, '1850', 'MUSHTAQ AHMAD', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(446, 5, 2, '1907', 'WAJAHAT HUSSAIN', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(447, 5, 2, '1921', 'NASIR IQBAL', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(448, 5, 2, '1604', 'Tayyaba Qayyum', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(449, 5, 2, '1620', 'Sayyada Khushboo Zainab', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL);
INSERT INTO `students` (`id`, `grade_id`, `department_id`, `registration_number`, `full_name`, `address`, `mobile_number`, `guardian_number`, `remarks`, `finger_print`, `created_at`, `updated_at`, `deleted_at`) VALUES
(450, 5, 2, '1625', 'Rida Asif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(451, 5, 2, '1647', 'Hajra Javed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(452, 5, 2, '1649', 'Shah Khalid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(453, 5, 2, '1678', 'Maqsood Rana', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(454, 5, 2, '1406', 'Summera Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(455, 5, 2, '1447', 'Sheryar Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(456, 5, 2, '1450', 'Ahmed Ali Nawaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(457, 5, 2, '1480', 'Arslan Rasheed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(612, 6, 1, 'BDS-20/454\r\n', 'Atta Ur Rehman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(613, 6, 1, 'BDS-20/453\r\n', 'Azmat Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(614, 6, 1, 'BDS-20/458\r\n', 'Dawood Bacha ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(615, 6, 1, 'BDS-20/448\r\n', 'Uzair Akram ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(616, 6, 1, 'BDS-20/471\r\n', 'Eshant Singh ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(617, 6, 1, 'BDS-20/470\r\n', 'Syed Nouman Ali Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(618, 6, 1, 'BDS-20/457\r\n', 'Iftikhar Azeem ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(619, 6, 1, 'BDS-20/464\r\n', 'Sardar Ijlal Riaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(620, 6, 1, 'BDS-20/455\r\n', ' Muddasir Ali Shah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(621, 6, 1, 'BDS-20/437\r\n', ' Ariba Khalid ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(622, 6, 1, 'BDS-20/446\r\n', ' Imbisat Usman sawati ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(623, 6, 1, 'BDS-20/424\r\n', ' Syeda Ramsha Noor', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(624, 6, 1, 'BDS-20/430\r\n', 'Maria Waheed ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(625, 6, 1, 'BDS-20/461\r\n', 'Tafweez ur Rehman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(626, 6, 1, 'BDS-20/450\r\n', 'Ali Kamal Shah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(627, 6, 1, 'BDS-20/441\r\n', ' Rida Aman ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(628, 6, 1, 'BDS-20/460\r\n', 'Kaleem Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(629, 6, 1, 'BDS-20/456\r\n', 'Syed Hamza Hussain Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(630, 6, 1, 'BDS-20/431\r\n', 'Sarah Imran', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(631, 6, 1, 'BDS-20/426\r\n', ' Iqra tul ain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(632, 6, 1, 'BDS-20/466\r\n', 'Muhammad Shaheryar Akram', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(633, 6, 1, 'BDS-20/468\r\n', 'Arif Ullah Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(634, 6, 1, 'BDS-20/472\r\n', 'Muhammad Ahmed Zubair', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(635, 6, 1, 'BDS-20/462\r\n', 'Akhtisham Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(636, 6, 1, 'BDS-20/440\r\n', 'Khadeeja Zaheer ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(637, 6, 1, 'BDS-20/445\r\n', 'Rameen Jamal  ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(638, 6, 1, 'BDS-20/469\r\n', 'Saad Danish ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(639, 6, 1, 'BDS-20/423\r\n', 'Noor Ul Huda Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(640, 6, 1, 'BDS-20/444\r\n', 'Twinkle Hidayatullah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(641, 6, 1, 'BDS-20/463\r\n', 'Ihtisham Hassan Khan ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(642, 6, 1, 'BDS-20/438\r\n', 'S Anmol Gul ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(643, 6, 1, 'BDS-20/434\r\n', 'Tanzeela Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(644, 6, 1, 'BDS-20/459\r\n', 'Syed Sheheryar Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(645, 6, 1, 'BDS-20/428\r\n', 'Bisma Amin ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(646, 6, 1, 'BDS-20/425\r\n', 'Palwasha Mahnoor ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(647, 6, 1, 'N/A', 'Zarwa Syed ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(648, 6, 1, 'BDS-20/436\r\n', 'Shandana  Mehmood', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(649, 6, 1, 'BDS-20/452\r\n', 'Talha Shamim ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(650, 6, 1, 'BDS-20/465\r\n', 'Hamza Saghar ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(651, 6, 1, 'BDS-20/449\r\n', 'Junaid  Iqbal Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(652, 6, 1, 'BDS-20/427\r\n', 'Faryal Muhammad Fayyaz ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(653, 6, 1, 'BDS-20/443\r\n', 'Hubab Arif ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(654, 6, 1, 'BDS-20/442\r\n', 'Khadija Shah ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(655, 6, 1, 'BDS-20/439\r\n', 'Mehmona Shehzadi ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(656, 6, 1, 'BDS-20/451\r\n', 'Usman Tariq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(657, 6, 1, 'BDS-20/435\r\n', 'Sarah Shakeel ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(658, 6, 1, 'BDS-20/432\r\n', 'Sapogmai Khattak', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(659, 6, 1, 'BDS-20/433\r\n', 'Nimra Gul', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(660, 7, 1, 'BDS-19/373\r\n', 'Sabina Begum', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(661, 7, 1, 'BDS-19/374\r\n', 'shandana jan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(662, 7, 1, 'BDS-19/375\r\n', 'Ramla Zaman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(663, 7, 1, 'BDS-19/376\r\n', 'Kashf Sana', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(664, 7, 1, 'BDS-19/377\r\n', 'Kainat Hussain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(665, 7, 1, 'BDS-19/378\r\n', 'Marwa Arshad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(666, 7, 1, 'BDS-19/379\r\n', 'AFIFA HAMID', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(667, 7, 1, 'BDS-19/380\r\n', 'Dua Javaid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(668, 7, 1, 'BDS-19/381\r\n', 'AYESHA TILAWAT', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(669, 7, 1, 'BDS-19/383\r\n', 'Maham Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(670, 7, 1, 'BDS-19/384\r\n', 'Amna Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(671, 7, 1, 'BDS-19/385\r\n', 'Zenia khattak', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(672, 7, 1, 'BDS-19/386\r\n', 'Khudija jan qureshi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(673, 7, 1, 'BDS-19/387\r\n', 'NABIGHA ABID', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(674, 7, 1, 'BDS-19/388\r\n', 'SANA NAZAKAT', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(675, 7, 1, 'BDS-19/389\r\n', 'Rohma Syed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(676, 7, 1, 'BDS-19/390\r\n', 'Zuhaa Mushtaq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(677, 7, 1, 'BDS-19/391\r\n', 'Marwa Nazir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(678, 7, 1, 'BDS-19/392\r\n', 'Muskaan azeem', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(679, 7, 1, 'BDS-19/393\r\n', 'Urooj Rehmani', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(680, 7, 1, 'BDS-19/394\r\n', 'Manahil Siraj', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(681, 7, 1, 'BDS-19/395\r\n', 'Laraib Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(682, 7, 1, 'BDS-19/396\r\n', 'Mahnoor Arif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(683, 7, 1, 'BDS-19/397\r\n', 'Hina Ayub', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(684, 7, 1, 'BDS-19/398\r\n', 'Mariam Mustafa', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(685, 7, 1, 'BDS-19/399\r\n', 'Mina Bahar Durrani', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(686, 7, 1, 'BDS-19/400\r\n', 'INSHA ALI', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(687, 7, 1, 'BDS-19/401\r\n', 'Umra Abid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(688, 7, 1, 'BDS-19/402\r\n', 'Usman Jamal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(689, 7, 1, 'BDS-19/403\r\n', 'Hamza Arif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(690, 7, 1, 'BDS-19/404\r\n', 'Mudasir iqbal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(691, 7, 1, 'BDS-19/405\r\n', 'Wahab Riaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(692, 7, 1, 'BDS-19/406\r\n', 'Waqas Ahmad Farooqi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(693, 7, 1, 'BDS-19/407\r\n', 'Haider Ali Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(694, 7, 1, 'BDS-19/408\r\n', 'Hamza Hassan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(695, 7, 1, 'BDS-19/409\r\n', 'Faisal Zaman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(696, 7, 1, 'BDS-19/410\r\n', 'Afrasiyab rahim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(697, 7, 1, 'BDS-19/411\r\n', 'Hozaifa Rauf', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(698, 7, 1, 'BDS-19/412\r\n', 'Mohsin Bacha', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(699, 7, 1, 'BDS-19/413\r\n', 'Junaid Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(700, 7, 1, 'BDS-19/414\r\n', 'Anees Ullah Jan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(701, 7, 1, 'BDS-19/415\r\n', 'Mazhar Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(702, 7, 1, 'BDS-19/416\r\n', 'Muhammad Furqan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(703, 7, 1, 'BDS-19/417\r\n', 'ABDULLAH', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(704, 7, 1, 'BDS-19/418\r\n', 'Imam Hussain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(705, 7, 1, 'BDS-19/419\r\n', 'M. Afaq Zafar Marwat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(706, 7, 1, 'BDS-19/420\r\n', 'Akhtar Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(707, 7, 1, 'BDS-19/421\r\n', 'Muhammad Essa Fayyaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(708, 7, 1, 'BDS-19/422\r\n', 'Baizad Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(766, 7, 1, '18/351\r\n', 'Noor ul Bashar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(767, 7, 1, '18/353\r\n', 'Faiq Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(768, 7, 1, '18/364\r\n', 'Muhammad Zulqarnain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(769, 8, 1, 'BDS-18/326\r\n', 'Fatima Hassan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(770, 8, 1, 'BDS-18/327\r\n', 'Tuba Fatima', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(771, 8, 1, 'BDS-18/328\r\n', 'Muhammad Haris Mahboob', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(772, 8, 1, 'BDS-18/329\r\n', 'Muzaffar Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(773, 8, 1, 'BDS-18/331\r\n', 'Muhammad Abdullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(774, 8, 1, 'BDS-18/332\r\n', 'Hasnat Ahsan Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(775, 8, 1, 'BDS-18/333\r\n', 'Nasar Ali', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(776, 8, 1, 'BDS-18/334\r\n', 'Muhammad Suleman Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(777, 8, 1, 'BDS-18/335\r\n', 'Ahmad Muhammad Sohail', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(778, 8, 1, 'BDS-18/336\r\n', 'Ubaidullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(779, 8, 1, 'BDS-18/337\r\n', 'Syed Usama Saeed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(780, 8, 1, 'BDS-18/338\r\n', 'Salman Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(781, 8, 1, 'BDS-18/339\r\n', 'Afaq Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(782, 8, 1, 'BDS-18/340\r\n', 'Muhammad Asim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(783, 8, 1, 'BDS-18/341\r\n', 'Abdul Wakeel Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(784, 8, 1, 'BDS-18/342\r\n', 'Hamza Ahmad Kaka Khel', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(785, 8, 1, 'BDS-18/343\r\n', 'Misbah Ulllah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(786, 8, 1, 'BDS-18/344\r\n', 'Mehtab Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(787, 8, 1, 'BDS-18/345\r\n', 'Aqeel Said Arab Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(788, 8, 1, 'BDS-18/346\r\n', 'Waleed Ahmad Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(789, 8, 1, 'BDS-18/347\r\n', 'Toheed Ur Rehman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(790, 8, 1, 'BDS-18/348\r\n', 'Muhammad Najam ud Din', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(791, 8, 1, 'BDS-18/349\r\n', 'Usam Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(792, 8, 1, 'BDS-18/350\r\n', 'Tabish Hussain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(793, 8, 1, 'BDS-18/352\r\n', 'Syed Atta Ullah  Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(794, 8, 1, 'BDS-18/354\r\n', 'Muhammad Ismail', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(795, 8, 1, 'BDS-18/355\r\n', 'Muhammad Ahsan Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(796, 8, 1, 'BDS-18/356\r\n', 'Atif Ghufran', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(797, 8, 1, 'BDS-18/357\r\n', 'Nauman Naeem', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(798, 8, 1, 'BDS-18/358\r\n', 'Amin Ghani', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(799, 8, 1, 'BDS-18/359\r\n', 'Muhammad Obaid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(800, 8, 1, 'BDS-18/360\r\n', 'Syed Izhar Ali Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(801, 8, 1, 'BDS-18/361\r\n', 'Muhammad Arham Zarif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(802, 8, 1, 'BDS-18/362\r\n', 'Ahmad Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(803, 8, 1, 'BDS-18/363\r\n', 'Muhammad Fahid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(804, 8, 1, 'BDS-18/365\r\n', 'Osama Afridi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(805, 8, 1, 'BDS-18/366\r\n', 'Muhammad Hisham', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(806, 8, 1, 'BDS-18/367\r\n', 'Yashab James', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(807, 8, 1, 'BDS-18/368\r\n', 'Mamun Ur Rashid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(808, 8, 1, 'BDS-18/369\r\n', 'Safeer Hussain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(809, 8, 1, 'BDS-18/370\r\n', 'Fawad Jan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(810, 8, 1, 'BDS-18/371\r\n', 'Hassan Ali', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(811, 8, 1, 'BDS-18/372\r\n', 'Arbab Shahkar Hamid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(812, 8, 1, 'BDS-17/387\r\n', 'Aqsa Nabeel', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(813, 8, 1, 'BDS-17/309\r\n', 'Faiq Liaqat', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(814, 8, 1, 'BDS-16/260\r\n', 'Fawad-ul-Hassan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(815, 8, 1, 'BDS-17/296\r\n', 'Hafiz Shahzeb Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(816, 8, 1, 'BDS-17/280\r\n', 'Maria Burhan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(817, 8, 1, 'BDS-17/295\r\n', 'Sahibzada Hamed Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(818, 8, 1, 'BDS-17/286\r\n', 'Sonaina Arbab', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(819, 8, 1, 'BDS-17/291\r\n', 'Zeeshan Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(820, 8, 1, 'BDS-17/277\r\n', 'Zulekha Sattar Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(821, 8, 1, '17/293\r\n', 'Hafiz Usama Akhtar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(822, 8, 1, '17/294\r\n', 'Maaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(823, 8, 1, '17/300\r\n', 'M.Waleed Salar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(824, 8, 1, '17/303\r\n', 'Roman Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(825, 8, 1, '17/312\r\n', 'Syed M. Sohaib', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(826, 9, 1, '17/278\r\n', 'Ayesha  Bibi  Shahid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(827, 9, 1, '17/279\r\n', 'Rushba Syed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(828, 9, 1, '17/281\r\n', 'Mariyam  Aurangzeb', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(829, 9, 1, '17/282\r\n', 'Aimen Rubab', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(830, 9, 1, '17/283\r\n', 'Sanam  Arif', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(831, 9, 1, '17/284\r\n', 'Ramla  Fajar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(832, 9, 1, '17/285\r\n', 'Roolan  Baseer', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(833, 9, 1, '17/288\r\n', 'Hajra   Mushtaq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(834, 9, 1, '17/289\r\n', 'Umaiya   Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(835, 9, 1, '17/290\r\n', 'Amina Islam', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(836, 9, 1, '17/292\r\n', 'Ammar Bin Fazal', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(837, 9, 1, '17/297\r\n', 'Arsalan  Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(838, 9, 1, '17/298\r\n', 'Mohsin Saeed', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(839, 9, 1, '17/299\r\n', 'M.Umar  Khalid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(840, 9, 1, '17/301\r\n', 'Syed  Babar  Ali', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(841, 9, 1, '17/302\r\n', 'Hamza  Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(842, 9, 1, '17/304\r\n', 'M. Jiyad  Khalil', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(843, 9, 1, '17/305\r\n', 'Hamza Shabir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(844, 9, 1, '17/306\r\n', 'Ihtasham-ul-Haq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(845, 9, 1, '17/307\r\n', 'Anees Alam', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(846, 9, 1, '17/308\r\n', 'Muhammad Tayyab', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(847, 9, 1, '17/310\r\n', 'Masood  Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(848, 9, 1, '17/311\r\n', 'M.Umar  Munir', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(849, 9, 1, '17/313\r\n', 'Askar  Ali', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(850, 9, 1, '17/314\r\n', 'M.Haseeb  Kalim', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(851, 9, 1, '17/315\r\n', 'Umair Ali', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(852, 9, 1, '17/317\r\n', 'Shumail Bin Usman', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(853, 9, 1, '17/318\r\n', 'Hasnain  Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(854, 9, 1, '17/319\r\n', 'Abdul  Basit (Malakand)', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(855, 9, 1, '17/320\r\n', 'Umair Shafiq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(856, 9, 1, '17/321\r\n', 'Haroon Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(857, 9, 1, '17/322\r\n', 'M.  Uzair  Ayaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(858, 9, 1, '17/323\r\n', 'Syed  Gohar  Hussain', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(859, 9, 1, '17/324\r\n', 'Talha  Akbar', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(860, 9, 1, '17/325\r\n', 'Abdul  Basit (Atd)', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(861, 9, 1, '16/228\r\n', 'Baila Urooj Malik', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(862, 9, 1, '16/235\r\n', 'Rimsha Malang', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(863, 9, 1, '16/240\r\n', 'Abbas Ahmad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(864, 9, 1, '16/245\r\n', 'Muhanunad Rizwan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(865, 9, 1, '16/246\r\n', 'Anas Raza', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(866, 9, 1, '16/247\r\n', 'Syed Umar Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(867, 9, 1, '16/249\r\n', 'Haroon Shah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(868, 9, 1, '16/254\r\n', 'Hamza Zahid', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(869, 9, 1, '16/255\r\n', 'Sherbaz Tariq Khan', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(870, 9, 1, '16/256\r\n', 'Omer Sajjad', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(871, 9, 1, '16/259\r\n', 'Muhammad Ali Mehmood', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(872, 9, 1, '16/266\r\n', 'Fahad Ullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(873, 9, 1, '16/271\r\n', 'Matiullah', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(874, 9, 1, '16/276\r\n', 'Ubaid Ahmad Alvi', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(875, 9, 1, '15/212\r\n', 'MUHAMMAD GHUFRAN', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(876, 9, 1, 'BDS-12/45\r\n', 'Abu Huraira ', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(877, 9, 1, 'BDS-15/217\r\n', 'Ihtisham ul Haq', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(878, 9, 1, 'BDS-15/171\r\n', 'IQRA AQDAS', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(879, 9, 1, 'BDS-16/250\r\n', 'Muhammad Saqib', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(880, 9, 1, 'BDS-16/248\r\n', 'Kamran Riaz', NULL, '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_attendances`
--

CREATE TABLE `student_attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `student_timetable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by_id` bigint(20) UNSIGNED NOT NULL,
  `attendance_date` date NOT NULL,
  `attendance_marked_type` tinyint(1) NOT NULL DEFAULT 1,
  `attendance_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_departments`
--

CREATE TABLE `student_departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_departments`
--

INSERT INTO `student_departments` (`id`, `student_id`, `department_id`, `created_by_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:59', NULL),
(2, 2, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(3, 3, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(4, 4, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(5, 5, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(6, 6, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(7, 7, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(8, 8, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(9, 9, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(10, 10, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(11, 11, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(12, 12, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(13, 13, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(14, 14, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(15, 15, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(16, 16, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(17, 17, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(18, 18, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(19, 19, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(20, 20, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(21, 21, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(22, 22, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(23, 23, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(24, 24, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(25, 25, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(26, 26, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(27, 27, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(28, 28, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(29, 29, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(30, 30, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(31, 31, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(32, 32, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(33, 33, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(34, 34, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(35, 35, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(36, 36, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(37, 37, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(38, 38, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(39, 39, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(40, 40, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(41, 41, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(42, 42, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(43, 43, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(44, 44, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(45, 45, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(46, 46, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(47, 47, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(48, 48, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(49, 49, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(50, 50, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(51, 51, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(52, 52, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(53, 53, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(54, 54, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(55, 55, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(56, 56, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(57, 57, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(58, 58, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(59, 59, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(60, 60, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(61, 61, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(62, 62, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(63, 63, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(64, 64, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(65, 65, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(66, 66, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(67, 67, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(68, 68, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(69, 69, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(70, 70, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(71, 71, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(72, 72, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(73, 73, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(74, 74, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(75, 75, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(76, 76, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(77, 77, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(78, 78, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(79, 79, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(80, 80, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(81, 81, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(82, 82, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(83, 83, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(84, 84, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(85, 85, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(86, 86, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(87, 87, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(88, 88, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(89, 89, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(90, 90, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(91, 91, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(92, 92, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(93, 93, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(94, 94, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(95, 95, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(96, 96, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(97, 97, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(98, 98, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(99, 99, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(100, 100, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(101, 101, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(102, 102, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(103, 103, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(104, 104, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(105, 105, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(106, 106, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(107, 107, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(108, 108, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(109, 109, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(110, 110, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(111, 111, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(112, 112, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(113, 113, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(114, 114, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(115, 115, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(116, 116, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(117, 117, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(118, 118, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(119, 119, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(120, 120, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(121, 121, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(122, 122, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(123, 123, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(124, 124, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(125, 125, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(126, 126, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(127, 127, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(128, 128, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(129, 129, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(130, 130, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(131, 131, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(132, 132, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(133, 133, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(134, 134, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(135, 135, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(136, 136, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(137, 137, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(138, 138, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(139, 139, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(140, 140, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(141, 141, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(142, 142, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(143, 143, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(144, 144, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(145, 145, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(146, 146, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(147, 147, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(148, 148, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(149, 149, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(150, 150, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(151, 151, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(152, 152, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(153, 153, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(154, 154, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(155, 155, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(156, 156, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(157, 157, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(158, 158, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(159, 159, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(160, 160, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(161, 161, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(162, 162, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(163, 163, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(164, 164, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(165, 165, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(166, 166, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(167, 167, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(168, 168, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(169, 169, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(170, 170, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(171, 171, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(172, 172, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(173, 173, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(174, 174, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(175, 175, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(176, 176, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(177, 177, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(178, 178, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(179, 179, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(180, 180, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(181, 181, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(182, 182, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(183, 183, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(184, 184, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(185, 185, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(186, 186, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(187, 187, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(188, 188, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(189, 189, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(190, 190, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(191, 191, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(192, 192, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(193, 193, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(194, 194, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(195, 195, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(196, 196, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(197, 197, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(198, 198, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(199, 199, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(200, 200, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(201, 201, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(202, 202, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(203, 203, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(204, 204, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(205, 205, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(206, 206, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(207, 207, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(208, 208, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(209, 209, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(210, 210, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(211, 211, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(212, 212, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(213, 213, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(214, 214, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(215, 215, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(216, 216, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(217, 217, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(218, 218, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(219, 219, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(220, 220, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(221, 221, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(222, 222, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(223, 223, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(224, 224, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(225, 225, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(226, 226, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(227, 227, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(228, 228, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(229, 229, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(230, 230, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(231, 231, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(232, 232, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(233, 233, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(234, 234, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(235, 235, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(236, 236, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(237, 237, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(238, 238, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(239, 239, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(240, 240, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(241, 241, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(242, 242, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(243, 243, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(244, 244, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(245, 245, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(246, 246, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(247, 247, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(248, 248, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(249, 249, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(250, 250, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(251, 251, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(252, 252, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(253, 253, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(254, 254, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(255, 255, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(256, 256, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(257, 257, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(258, 258, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(259, 259, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(260, 260, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(261, 261, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(262, 262, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(263, 263, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(264, 264, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(265, 265, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(266, 266, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(267, 267, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(268, 268, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(269, 269, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(270, 270, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(271, 271, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(272, 272, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(273, 273, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(274, 274, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(275, 275, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(276, 276, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(277, 277, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(278, 278, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(279, 279, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(280, 280, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(281, 281, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(282, 282, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(283, 283, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(284, 284, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(285, 285, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(286, 286, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(287, 287, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(288, 288, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(289, 289, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(290, 290, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(291, 291, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(292, 292, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(293, 293, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(294, 294, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(295, 295, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(296, 296, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(297, 297, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(298, 298, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(299, 299, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(300, 300, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(301, 301, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(302, 302, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(303, 303, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(304, 304, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(305, 305, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(306, 306, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(307, 307, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(308, 308, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(309, 309, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(310, 310, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(311, 311, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(312, 312, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(313, 313, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(314, 314, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(315, 315, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(316, 316, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(317, 317, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(318, 318, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(319, 319, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(320, 320, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(321, 321, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(322, 322, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(323, 323, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(324, 324, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(325, 325, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(326, 326, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(327, 327, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(328, 328, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(329, 329, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(330, 330, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(331, 331, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(332, 332, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(333, 333, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(334, 334, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(335, 335, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(336, 336, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(337, 337, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(338, 338, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(339, 339, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(340, 340, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(341, 341, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(342, 342, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(343, 343, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(344, 344, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(345, 345, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(346, 346, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(347, 347, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(348, 348, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(349, 349, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(350, 350, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(351, 351, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(352, 352, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(353, 353, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(354, 354, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(355, 355, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(356, 356, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(357, 357, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(358, 358, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(359, 359, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(360, 360, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(361, 361, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(362, 362, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(363, 363, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(364, 364, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(365, 365, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(366, 366, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(367, 367, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(368, 368, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(369, 369, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(370, 370, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(371, 371, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(372, 372, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(373, 373, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(374, 374, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(375, 375, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(376, 376, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(377, 377, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(378, 378, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(379, 379, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(380, 380, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(381, 381, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(382, 382, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(383, 383, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(384, 384, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(385, 385, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(386, 386, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(387, 387, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(388, 388, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(389, 389, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(390, 390, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(391, 391, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(392, 392, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(393, 393, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(394, 394, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(395, 395, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(396, 396, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(397, 397, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(398, 398, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(399, 399, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(400, 400, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(401, 401, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(402, 402, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(403, 403, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(404, 404, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(405, 405, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(406, 406, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(407, 407, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(408, 408, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(409, 409, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(410, 410, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(411, 411, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(412, 412, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(413, 413, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(414, 414, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(415, 415, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(416, 416, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(417, 417, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(418, 418, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(419, 419, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(420, 420, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(421, 421, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(422, 422, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(423, 423, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(424, 424, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(425, 425, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(426, 426, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(427, 427, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(428, 428, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(429, 429, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(430, 430, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(431, 431, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(432, 432, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(433, 433, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(434, 434, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(435, 435, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(436, 436, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(437, 437, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(438, 438, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(439, 439, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(440, 440, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(441, 441, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(442, 442, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(443, 443, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(444, 444, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(445, 445, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(446, 446, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(447, 447, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(448, 448, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(449, 449, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(450, 450, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(451, 451, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(452, 452, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(453, 453, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(454, 454, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(455, 455, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(456, 456, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(457, 457, 2, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(612, 612, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(613, 613, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(614, 614, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(615, 615, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(616, 616, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(617, 617, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(618, 618, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(619, 619, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(620, 620, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(621, 621, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(622, 622, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(623, 623, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(624, 624, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(625, 625, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(626, 626, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(627, 627, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(628, 628, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(629, 629, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(630, 630, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(631, 631, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(632, 632, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(633, 633, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(634, 634, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(635, 635, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(636, 636, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(637, 637, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(638, 638, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(639, 639, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(640, 640, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(641, 641, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(642, 642, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(643, 643, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(644, 644, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(645, 645, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(646, 646, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(647, 647, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(648, 648, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(649, 649, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(650, 650, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(651, 651, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(652, 652, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(653, 653, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(654, 654, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(655, 655, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(656, 656, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(657, 657, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(658, 658, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(659, 659, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(660, 660, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(661, 661, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(662, 662, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(663, 663, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(664, 664, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(665, 665, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(666, 666, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(667, 667, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(668, 668, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(669, 669, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(670, 670, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(671, 671, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(672, 672, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(673, 673, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(674, 674, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(675, 675, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(676, 676, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(677, 677, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(678, 678, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(679, 679, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(680, 680, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(681, 681, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(682, 682, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(683, 683, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(684, 684, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(685, 685, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(686, 686, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(687, 687, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(688, 688, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(689, 689, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(690, 690, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(691, 691, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(692, 692, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(693, 693, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(694, 694, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(695, 695, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(696, 696, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(697, 697, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(698, 698, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(699, 699, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(700, 700, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(701, 701, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(702, 702, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(703, 703, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(704, 704, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(705, 705, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(706, 706, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(707, 707, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(708, 708, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(766, 766, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(767, 767, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(768, 768, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(769, 769, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(770, 770, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(771, 771, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(772, 772, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(773, 773, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(774, 774, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(775, 775, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(776, 776, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(777, 777, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(778, 778, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(779, 779, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(780, 780, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(781, 781, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(782, 782, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(783, 783, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(784, 784, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(785, 785, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(786, 786, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(787, 787, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(788, 788, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(789, 789, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(790, 790, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(791, 791, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(792, 792, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(793, 793, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(794, 794, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(795, 795, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(796, 796, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(797, 797, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(798, 798, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(799, 799, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(800, 800, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(801, 801, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(802, 802, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(803, 803, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(804, 804, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(805, 805, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(806, 806, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(807, 807, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(808, 808, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(809, 809, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(810, 810, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(811, 811, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(812, 812, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(813, 813, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(814, 814, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(815, 815, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(816, 816, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(817, 817, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(818, 818, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(819, 819, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(820, 820, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(821, 821, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(822, 822, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(823, 823, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(824, 824, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(825, 825, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(826, 826, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(827, 827, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(828, 828, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(829, 829, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(830, 830, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(831, 831, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(832, 832, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(833, 833, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(834, 834, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(835, 835, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(836, 836, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(837, 837, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(838, 838, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(839, 839, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(840, 840, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(841, 841, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(842, 842, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(843, 843, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(844, 844, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(845, 845, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(846, 846, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(847, 847, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(848, 848, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(849, 849, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(850, 850, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(851, 851, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(852, 852, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(853, 853, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(854, 854, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(855, 855, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(856, 856, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(857, 857, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(858, 858, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(859, 859, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(860, 860, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(861, 861, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(862, 862, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(863, 863, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(864, 864, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(865, 865, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(866, 866, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(867, 867, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(868, 868, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(869, 869, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(870, 870, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(871, 871, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(872, 872, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(873, 873, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(874, 874, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(875, 875, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(876, 876, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(877, 877, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(878, 878, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(879, 879, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL),
(880, 880, 1, 1, 1, '2021-06-09 12:56:58', '2021-06-09 12:56:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_grades`
--

CREATE TABLE `student_grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_grades`
--

INSERT INTO `student_grades` (`id`, `grade_id`, `student_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(2, 1, 2, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(3, 1, 3, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(4, 1, 4, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(5, 1, 5, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(6, 1, 6, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(7, 1, 7, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(8, 1, 8, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(9, 1, 9, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(10, 1, 10, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(11, 1, 11, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(12, 1, 12, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(13, 1, 13, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(14, 1, 14, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(15, 1, 15, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(16, 1, 16, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(17, 1, 17, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(18, 1, 18, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(19, 1, 19, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(20, 1, 20, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(21, 1, 21, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(22, 1, 22, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(23, 1, 23, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(24, 1, 24, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(25, 1, 25, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(26, 1, 26, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(27, 1, 27, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(28, 1, 28, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(29, 1, 29, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(30, 1, 30, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(31, 1, 31, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(32, 1, 32, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(33, 1, 33, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(34, 1, 34, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(35, 1, 35, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(36, 1, 36, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(37, 1, 37, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(38, 1, 38, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(39, 1, 39, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(40, 1, 40, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(41, 1, 41, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(42, 1, 42, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(43, 1, 43, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(44, 1, 44, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(45, 1, 45, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(46, 1, 46, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(47, 1, 47, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(48, 1, 48, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(49, 1, 49, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(50, 1, 50, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(51, 1, 51, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(52, 1, 52, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(53, 1, 53, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(54, 1, 54, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(55, 1, 55, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(56, 1, 56, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(57, 1, 57, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(58, 1, 58, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(59, 1, 59, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(60, 1, 60, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(61, 1, 61, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(62, 1, 62, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(63, 1, 63, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(64, 1, 64, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(65, 1, 65, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(66, 1, 66, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(67, 1, 67, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(68, 1, 68, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(69, 1, 69, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(70, 1, 70, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(71, 1, 71, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(72, 1, 72, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(73, 1, 73, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(74, 1, 74, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(75, 1, 75, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(76, 1, 76, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(77, 1, 77, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(78, 1, 78, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(79, 1, 79, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(80, 1, 80, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(81, 1, 81, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(82, 1, 82, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(83, 1, 83, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(84, 1, 84, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(85, 1, 85, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(86, 1, 86, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(87, 1, 87, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(88, 1, 88, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(89, 1, 89, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(90, 1, 90, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(91, 1, 91, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(92, 1, 92, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(93, 1, 93, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(94, 1, 94, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(95, 1, 95, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(96, 1, 96, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(97, 1, 97, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(98, 1, 98, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(99, 1, 99, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(100, 1, 100, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(101, 2, 101, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(102, 2, 102, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(103, 2, 103, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(104, 2, 104, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(105, 2, 105, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(106, 2, 106, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(107, 2, 107, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(108, 2, 108, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(109, 2, 109, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(110, 2, 110, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(111, 2, 111, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(112, 2, 112, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(113, 2, 113, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(114, 2, 114, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(115, 2, 115, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(116, 2, 116, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(117, 2, 117, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(118, 2, 118, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(119, 2, 119, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(120, 2, 120, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(121, 2, 121, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(122, 2, 122, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(123, 2, 123, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(124, 2, 124, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(125, 2, 125, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(126, 2, 126, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(127, 2, 127, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(128, 2, 128, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(129, 2, 129, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(130, 2, 130, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(131, 2, 131, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(132, 2, 132, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(133, 2, 133, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(134, 2, 134, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(135, 2, 135, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(136, 2, 136, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(137, 2, 137, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(138, 2, 138, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(139, 2, 139, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(140, 2, 140, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(141, 2, 141, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(142, 2, 142, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(143, 2, 143, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(144, 2, 144, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(145, 2, 145, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(146, 2, 146, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(147, 2, 147, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(148, 2, 148, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(149, 2, 149, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(150, 2, 150, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(151, 2, 151, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(152, 2, 152, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(153, 2, 153, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(154, 2, 154, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(155, 2, 155, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(156, 2, 156, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(157, 2, 157, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(158, 2, 158, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(159, 2, 159, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(160, 2, 160, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(161, 2, 161, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(162, 2, 162, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(163, 2, 163, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(164, 2, 164, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(165, 2, 165, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(166, 2, 166, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(167, 2, 167, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(168, 2, 168, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(169, 2, 169, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(170, 2, 170, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(171, 2, 171, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(172, 2, 172, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(173, 2, 173, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(174, 2, 174, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(175, 2, 175, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(176, 2, 176, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(177, 2, 177, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(178, 2, 178, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(179, 2, 179, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(180, 2, 180, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(181, 2, 181, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(182, 2, 182, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(183, 2, 183, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(184, 2, 184, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(185, 2, 185, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(186, 2, 186, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(187, 2, 187, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(188, 2, 188, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(189, 2, 189, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(190, 2, 190, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(191, 2, 191, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(192, 2, 192, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(193, 2, 193, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(194, 2, 194, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(195, 2, 195, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(196, 2, 196, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(197, 2, 197, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(198, 2, 198, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(199, 2, 199, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(200, 2, 200, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(201, 2, 201, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(202, 3, 202, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(203, 3, 203, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(204, 3, 204, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(205, 3, 205, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(206, 3, 206, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(207, 4, 207, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(208, 4, 208, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(209, 4, 209, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(210, 4, 210, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(211, 4, 211, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(212, 4, 212, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(213, 4, 213, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(214, 4, 214, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(215, 4, 215, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(216, 4, 216, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(217, 4, 217, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(218, 4, 218, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(219, 4, 219, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(220, 4, 220, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(221, 4, 221, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(222, 4, 222, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(223, 4, 223, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(224, 4, 224, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(225, 4, 225, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(226, 4, 226, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(227, 4, 227, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(228, 4, 228, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(229, 4, 229, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(230, 4, 230, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(231, 4, 231, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(232, 4, 232, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(233, 4, 233, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(234, 4, 234, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(235, 4, 235, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(236, 4, 236, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(237, 4, 237, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(238, 4, 238, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(239, 4, 239, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(240, 4, 240, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(241, 4, 241, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(242, 4, 242, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(243, 4, 243, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(244, 4, 244, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(245, 4, 245, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(246, 4, 246, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(247, 4, 247, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(248, 4, 248, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(249, 4, 249, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(250, 4, 250, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(251, 4, 251, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(252, 4, 252, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(253, 4, 253, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(254, 4, 254, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(255, 4, 255, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(256, 4, 256, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(257, 4, 257, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(258, 4, 258, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(259, 4, 259, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(260, 4, 260, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(261, 4, 261, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(262, 4, 262, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(263, 4, 263, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(264, 4, 264, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(265, 4, 265, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(266, 4, 266, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(267, 4, 267, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(268, 4, 268, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(269, 4, 269, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(270, 4, 270, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(271, 4, 271, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(272, 4, 272, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(273, 4, 273, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(274, 4, 274, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(275, 4, 275, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(276, 4, 276, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(277, 4, 277, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(278, 4, 278, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(279, 4, 279, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(280, 4, 280, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(281, 4, 281, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(282, 4, 282, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(283, 4, 283, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(284, 4, 284, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(285, 4, 285, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(286, 4, 286, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(287, 4, 287, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(288, 4, 288, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(289, 4, 289, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(290, 4, 290, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(291, 4, 291, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(292, 4, 292, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(293, 4, 293, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(294, 4, 294, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(295, 4, 295, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(296, 4, 296, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(297, 4, 297, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(298, 4, 298, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(299, 4, 299, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(300, 4, 300, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(301, 4, 301, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(302, 4, 302, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(303, 4, 303, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(304, 4, 304, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(305, 4, 305, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(306, 4, 306, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(307, 4, 307, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(308, 4, 308, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(309, 4, 309, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(310, 4, 310, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(311, 4, 311, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(312, 4, 312, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(313, 4, 313, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(314, 4, 314, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(315, 4, 315, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(316, 4, 316, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(317, 4, 317, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(318, 4, 318, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(319, 4, 319, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(320, 4, 320, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(321, 4, 321, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(322, 4, 322, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(323, 4, 323, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(324, 4, 324, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(325, 4, 325, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(326, 4, 326, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(327, 4, 327, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(328, 4, 328, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(329, 4, 329, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(330, 4, 330, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(331, 4, 331, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(332, 4, 332, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(333, 4, 333, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(334, 4, 334, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(335, 4, 335, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(336, 4, 336, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(337, 4, 337, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(338, 4, 338, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(339, 4, 339, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(340, 4, 340, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(341, 4, 341, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(342, 4, 342, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(343, 4, 343, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(344, 4, 344, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(345, 4, 345, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(346, 4, 346, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(347, 5, 347, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(348, 5, 348, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(349, 5, 349, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(350, 5, 350, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(351, 5, 351, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(352, 5, 352, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(353, 5, 353, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(354, 5, 354, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(355, 5, 355, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(356, 5, 356, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(357, 5, 357, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(358, 5, 358, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(359, 5, 359, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(360, 5, 360, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(361, 5, 361, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(362, 5, 362, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(363, 5, 363, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(364, 5, 364, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(365, 5, 365, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(366, 5, 366, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(367, 5, 367, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(368, 5, 368, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(369, 5, 369, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(370, 5, 370, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(371, 5, 371, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(372, 5, 372, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(373, 5, 373, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(374, 5, 374, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(375, 5, 375, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(376, 5, 376, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(377, 5, 377, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(378, 5, 378, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(379, 5, 379, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(380, 5, 380, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(381, 5, 381, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(382, 5, 382, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(383, 5, 383, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(384, 5, 384, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(385, 5, 385, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(386, 5, 386, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(387, 5, 387, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(388, 5, 388, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(389, 5, 389, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(390, 5, 390, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(391, 5, 391, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(392, 5, 392, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(393, 5, 393, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(394, 5, 394, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(395, 5, 395, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(396, 5, 396, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(397, 5, 397, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(398, 5, 398, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(399, 5, 399, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(400, 5, 400, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(401, 5, 401, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(402, 5, 402, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(403, 5, 403, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(404, 5, 404, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(405, 5, 405, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(406, 5, 406, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(407, 5, 407, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(408, 5, 408, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(409, 5, 409, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(410, 5, 410, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(411, 5, 411, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(412, 5, 412, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(413, 5, 413, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(414, 5, 414, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(415, 5, 415, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(416, 5, 416, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(417, 5, 417, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(418, 5, 418, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(419, 5, 419, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(420, 5, 420, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(421, 5, 421, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(422, 5, 422, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(423, 5, 423, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(424, 5, 424, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(425, 5, 425, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(426, 5, 426, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(427, 5, 427, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(428, 5, 428, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(429, 5, 429, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(430, 5, 430, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(431, 5, 431, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(432, 5, 432, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(433, 5, 433, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(434, 5, 434, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(435, 5, 435, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(436, 5, 436, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(437, 5, 437, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(438, 5, 438, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(439, 5, 439, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(440, 5, 440, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(441, 5, 441, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(442, 5, 442, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(443, 5, 443, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(444, 5, 444, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(445, 5, 445, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(446, 5, 446, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(447, 5, 447, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(448, 5, 448, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(449, 5, 449, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(450, 5, 450, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(451, 5, 451, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(452, 5, 452, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(453, 5, 453, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(454, 5, 454, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(455, 5, 455, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(456, 5, 456, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(457, 5, 457, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(612, 6, 612, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(613, 6, 613, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(614, 6, 614, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(615, 6, 615, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(616, 6, 616, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(617, 6, 617, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(618, 6, 618, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(619, 6, 619, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(620, 6, 620, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(621, 6, 621, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(622, 6, 622, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(623, 6, 623, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(624, 6, 624, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(625, 6, 625, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(626, 6, 626, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(627, 6, 627, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(628, 6, 628, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(629, 6, 629, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(630, 6, 630, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(631, 6, 631, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(632, 6, 632, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(633, 6, 633, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(634, 6, 634, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(635, 6, 635, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(636, 6, 636, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(637, 6, 637, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(638, 6, 638, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(639, 6, 639, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(640, 6, 640, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(641, 6, 641, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(642, 6, 642, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(643, 6, 643, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(644, 6, 644, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(645, 6, 645, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(646, 6, 646, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(647, 6, 647, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(648, 6, 648, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(649, 6, 649, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(650, 6, 650, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(651, 6, 651, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(652, 6, 652, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(653, 6, 653, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(654, 6, 654, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(655, 6, 655, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(656, 6, 656, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(657, 6, 657, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(658, 6, 658, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(659, 6, 659, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(660, 7, 660, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(661, 7, 661, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(662, 7, 662, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(663, 7, 663, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(664, 7, 664, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(665, 7, 665, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(666, 7, 666, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(667, 7, 667, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(668, 7, 668, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(669, 7, 669, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(670, 7, 670, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(671, 7, 671, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(672, 7, 672, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(673, 7, 673, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(674, 7, 674, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(675, 7, 675, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(676, 7, 676, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(677, 7, 677, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(678, 7, 678, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(679, 7, 679, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(680, 7, 680, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(681, 7, 681, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(682, 7, 682, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(683, 7, 683, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(684, 7, 684, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(685, 7, 685, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(686, 7, 686, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(687, 7, 687, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(688, 7, 688, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(689, 7, 689, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(690, 7, 690, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(691, 7, 691, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(692, 7, 692, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(693, 7, 693, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(694, 7, 694, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(695, 7, 695, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(696, 7, 696, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(697, 7, 697, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(698, 7, 698, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(699, 7, 699, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(700, 7, 700, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(701, 7, 701, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(702, 7, 702, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(703, 7, 703, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(704, 7, 704, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(705, 7, 705, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(706, 7, 706, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(707, 7, 707, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(708, 7, 708, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(766, 7, 766, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(767, 7, 767, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(768, 7, 768, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(769, 8, 769, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(770, 8, 770, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(771, 8, 771, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(772, 8, 772, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(773, 8, 773, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(774, 8, 774, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(775, 8, 775, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(776, 8, 776, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(777, 8, 777, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(778, 8, 778, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(779, 8, 779, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(780, 8, 780, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(781, 8, 781, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(782, 8, 782, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(783, 8, 783, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(784, 8, 784, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(785, 8, 785, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(786, 8, 786, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(787, 8, 787, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(788, 8, 788, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(789, 8, 789, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(790, 8, 790, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(791, 8, 791, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(792, 8, 792, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(793, 8, 793, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(794, 8, 794, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(795, 8, 795, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(796, 8, 796, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(797, 8, 797, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(798, 8, 798, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(799, 8, 799, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(800, 8, 800, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(801, 8, 801, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(802, 8, 802, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(803, 8, 803, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(804, 8, 804, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(805, 8, 805, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(806, 8, 806, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(807, 8, 807, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(808, 8, 808, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(809, 8, 809, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(810, 8, 810, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(811, 8, 811, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(812, 8, 812, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(813, 8, 813, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(814, 8, 814, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(815, 8, 815, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(816, 8, 816, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(817, 8, 817, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(818, 8, 818, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(819, 8, 819, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(820, 8, 820, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(821, 8, 821, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(822, 8, 822, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(823, 8, 823, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(824, 8, 824, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(825, 8, 825, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(826, 9, 826, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(827, 9, 827, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(828, 9, 828, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(829, 9, 829, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(830, 9, 830, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(831, 9, 831, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(832, 9, 832, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(833, 9, 833, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(834, 9, 834, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(835, 9, 835, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(836, 9, 836, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(837, 9, 837, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(838, 9, 838, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(839, 9, 839, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(840, 9, 840, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(841, 9, 841, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(842, 9, 842, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(843, 9, 843, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(844, 9, 844, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(845, 9, 845, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(846, 9, 846, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(847, 9, 847, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(848, 9, 848, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(849, 9, 849, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(850, 9, 850, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(851, 9, 851, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(852, 9, 852, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(853, 9, 853, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(854, 9, 854, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(855, 9, 855, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(856, 9, 856, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(857, 9, 857, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(858, 9, 858, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(859, 9, 859, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(860, 9, 860, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(861, 9, 861, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(862, 9, 862, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(863, 9, 863, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(864, 9, 864, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(865, 9, 865, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(866, 9, 866, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(867, 9, 867, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(868, 9, 868, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(869, 9, 869, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(870, 9, 870, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(871, 9, 871, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(872, 9, 872, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(873, 9, 873, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(874, 9, 874, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(875, 9, 875, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(876, 9, 876, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(877, 9, 877, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(878, 9, 878, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(879, 9, 879, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL),
(880, 9, 880, 1, '2021-06-09 13:12:57', '2021-06-09 13:12:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_leaves`
--

CREATE TABLE `student_leaves` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED DEFAULT NULL,
  `leave_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_timetables`
--

CREATE TABLE `student_timetables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `session_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `teacher_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `date` date NOT NULL,
  `day` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_timetables`
--

INSERT INTO `student_timetables` (`id`, `grade_id`, `session_id`, `subject_id`, `teacher_id`, `status`, `start_time`, `end_time`, `date`, `day`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, 1, '22:24:00', '23:24:00', '2021-07-31', 'Monday', '2021-07-31 12:24:12', '2021-07-31 12:24:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Anatomy', 1, '2021-06-06 15:32:19', '2021-07-03 11:51:17', NULL),
(2, 'Physiology', 1, '2021-06-06 15:32:32', '2021-06-06 15:32:33', NULL),
(3, 'Biochemistry', 1, '2021-06-06 15:32:54', '2021-06-06 15:32:55', NULL),
(4, 'Islamic & Pakistan Studies', 1, '2021-06-06 15:33:08', '2021-06-06 15:33:09', NULL),
(5, 'Pharmacology & Therapeutics', 1, '2021-06-06 15:34:12', '2021-06-06 15:34:13', NULL),
(6, 'General Pathology & Microbiology', 1, '2021-06-06 15:34:23', '2021-06-06 15:34:24', NULL),
(7, 'Forensic Medicine', 1, '2021-06-06 15:34:33', '2021-06-06 15:34:34', NULL),
(8, 'Community Medicine', 1, '2021-06-06 15:34:52', '2021-06-06 15:34:53', NULL),
(9, 'Special Pathology', 1, '2021-06-06 15:35:01', '2021-06-06 15:35:02', NULL),
(10, 'Otorhinolaryngology (ENT)', 1, '2021-06-06 15:35:10', '2021-06-06 15:35:11', NULL),
(11, 'Ophthalmology', 1, '2021-06-06 15:35:18', '2021-06-06 15:35:19', NULL),
(12, 'Medicine including Psychiatry & Dermatology', 1, '2021-06-06 15:35:30', '2021-06-06 15:35:30', NULL),
(13, 'Surgery including Orthopaedics & Anaesthesia', 1, '2021-06-06 15:35:38', '2021-06-06 15:35:39', NULL),
(14, 'Obstetrics & Gynaecology', 1, '2021-06-06 15:35:48', '2021-06-06 15:35:49', NULL),
(15, 'Paediatrics', 1, '2021-06-06 15:35:56', '2021-06-06 15:35:57', NULL),
(16, 'General Anatomy', 1, '2021-06-06 15:36:19', '2021-06-06 15:36:20', NULL),
(17, 'General Physiology', 1, '2021-06-06 15:36:31', '2021-06-06 15:36:32', NULL),
(18, 'Science of Dental Materials', 1, '2021-06-06 15:36:40', '2021-06-06 15:36:41', NULL),
(19, 'Oral and Maxillofacial Surgery', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(20, 'Pathology', 1, '2021-06-06 15:38:15', '2021-06-06 15:38:15', NULL),
(21, 'Pharmacology', 1, '2021-06-06 15:38:38', '2021-06-06 15:38:39', NULL),
(22, 'Oral Biology and Tooth Morphology', 1, '2021-06-06 15:38:52', '2021-06-06 15:38:52', NULL),
(23, 'Community and Preventive Dentistry', 1, '2021-06-06 15:39:01', '2021-06-06 15:39:02', NULL),
(24, 'General Surgery', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:25', NULL),
(25, 'General Medicine', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(26, 'Oral Pathology', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(27, 'Periodontology', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(28, 'Oral Medicine', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(29, 'Prosthodontics', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(30, 'Operative Dentistry', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL),
(31, 'Orthodontics', 1, '2021-06-06 15:39:24', '2021-06-06 15:39:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `employee_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pmdc_reg_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finger_print` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `department_id`, `subject_id`, `employee_number`, `pmdc_reg_no`, `full_name`, `designation`, `qualification`, `address`, `mobile_number`, `remarks`, `finger_print`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 18, '2932-D', 'AF', 'Dr. Mumtaz ul Islam', 'Associate Professor', 'BDS, M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:50', NULL),
(2, 1, 18, '11002-D', '11625/11002-D/D', 'Dr.Hina Ashraf', 'Assistant Professor', 'BDS,', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(3, 1, 18, '23380-D', 'AF', 'Dr.Sameen Naveed', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(4, 1, 18, '', 'AF', 'Dr. Hamza Gul', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(5, 1, 22, '9057-D', '2058/9057-D/D', 'Dr.Malik Arshman Khan', 'Associate Professor', 'BDS, MSc ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(6, 1, 22, '13856-D', '23209/13856-D/D', 'Dr.Faiza Ijaz', 'Senior Lecturer', 'BDS, Mphil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(7, 1, 22, '18748-D', '33384/18748-D/D', 'Dr.Arooj ', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(8, 1, 22, '25705-D', 'AF', 'Dr.Nimra Khan', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(9, 1, 26, '4114-D', '2354/4114-D/D', 'Dr.Joharia Azhar', 'Professor ', 'BDS,M.Sc,MHPE', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(10, 1, 26, '7795-D', '0231/7795-D/D', 'Dr.Munazza Gillani', 'Assistant Professor', 'BDS, M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(11, 1, 26, '25706-D', 'AF', 'Dr. Maria Mustafa Khan', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(12, 1, 26, '25104-D', 'AF', 'Dr. Kinza Fatima', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(13, 1, 26, '16602-D', '33386/16602-D/D', 'Dr.Samia Tariq', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(14, 1, 23, '8374-D', 'AF', 'Dr.Huma Tahir', 'Assistant Professor', 'BDS, Mphil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(15, 1, 23, '25726-D', 'AF', 'Dr.Faryal Khattak', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(16, 1, 23, '', 'AF', 'Dr.Khola Basit', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(17, 1, 27, '24780-D', 'AF', 'Dr.Samavia Mazhar', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(18, 1, 27, '', 'AF', 'Dr.Zainab Javed', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(19, 1, 27, '22405-D', '33653/22405-D/D', 'Dr.Noor Jehanzeb ', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(20, 1, 27, '17650-D', 'AF', 'Dr. Rozeena Hameed', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(21, 1, 19, '7061-D', '7087/7061-D/D', 'Dr.Samia Shad', ' Professor', 'BDS, FCPS ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(22, 1, 19, '9702-D', 'AF', 'Dr.Hasan Sardar', 'Senior Registrar', 'BDS, FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(23, 1, 19, '8140-D', '30176/8140-D/D', 'Dr. Saqib Mehmood ', 'Assistant Professor', 'BDS, FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(24, 1, 19, '20403-D', '33738/20403-D/D', 'Dr. Bibi Zahra ', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(25, 1, 19, '24618-D', 'AF', 'Dr.Haditha Shakeel', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(26, 1, 19, '22375-D', 'AF', 'Dr.Kashif Ali Shah', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(27, 1, 30, '435-D', '3659/435-D/D', 'Prof.Dr. M.Bakhsh Sobhi', 'Professor (Honorary) ', 'BDS, MDS,MCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(28, 1, 30, '5466-D', '7329/5466-D/D', 'Dr.Adil Shahnawaz', ' Professor', 'BDS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(29, 1, 30, '15608-D', '29873/15608-D/D', 'Dr.Usman Nazir', 'Assistant Professor', 'BDS,', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(30, 1, 30, '26332-D', 'AF', 'Dr.Shahana Pervez', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(31, 1, 30, '25725-D', 'AF', 'Dr. Hina Arif Paracha', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(32, 1, 30, '20387-D', '33486/20387-D/D', 'Dr.Fahad Ashfaq', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(33, 1, 30, '20577-D', '32458/20577-D/D', 'Dr. M.Ibrahim Sobhi', 'Senior Registrar', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(34, 1, 29, '11654-D', 'AF', 'Dr. Aamir Syed', 'Assistant Professor', 'BDS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(35, 1, 29, '', 'AF', 'Dr.Zainab Khalil', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(36, 1, 29, '15635-D', 'AF', 'Dr.Afnan Rehman', 'Registrar', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(37, 1, 29, '15367-D', 'AF', 'Dr.Anhum Haroon Jadoon', 'Regsitrar', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(38, 1, 29, '9131-D', '29870/9131-D/D', 'Dr. Sidra tul Huda', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(39, 1, 28, '582-D', '3445/582-D/D', 'Dr.Rafiq Ahmad Banday', 'Assistant Professor', 'BDS, MCPS ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(40, 1, 28, '5482-D', '14136/5482-D/D', 'Dr.Tanweer Hussain Bangash', 'Assistant Professor', 'BDS.MOMSRCS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(41, 1, 28, '4468-D', '29139/4468-D/D', 'Dr. Abdul Baseer', 'Registrar', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(42, 1, 28, '22385-D', 'AF', 'Dr. Sumbal Naseem', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(43, 1, 28, '15061-D', 'AF', 'Dr.Ayesha Shahzad', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(44, 1, 31, '', 'AF', 'Dr. Ibadullah Kundi', 'Professor', 'BDS,', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(45, 1, 31, '5151-D', '16642/5151-D/D', 'Dr.Amber Saeed Khan', 'Associate Professor', 'BDS, FCPS ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(46, 1, 31, '14437-D', 'AF', 'Dr. Verda Ahmad Khan', 'Registrar', 'BDS,MDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(47, 1, 31, '25241-D', 'AF', 'Dr. Shehryar Khan', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(48, 1, 31, '25561-D', 'AF', 'Dr.Daniyal Hafeez', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(49, 1, 31, '22470-D', 'AF', 'Maimoona Afsar', 'Demonstrator', 'BDS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(50, 1, 1, '3249-N', '3158/3249-N/M', 'Prof. Dr. Rashid Ahmad', 'Professor', 'MBBS, FRCS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(51, 1, 1, '1411-N', '30552/1411-N/M', 'Dr. Mohib-ullah Wazir', 'Senior Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(52, 1, 1, '', 'AF', 'Dr.Maryum', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(53, 1, 1, '13395-N', '30525/13395-N/M', 'Dr. Atif Nawaz Khan', 'Senior Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(54, 1, 2, '2589-N', '3691/2589-N/M', 'Prof. Dr.Tariq Mehmood Khan ', 'Professor', 'MBBS, M.Phil ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(55, 1, 2, '13377-D', 'AF', 'Dr.Kinza Sammar', 'Assistant Professor', 'BDS, M.Phil    ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(56, 1, 2, '20960-N', '33387/20960-N/M', 'Dr.Maheen Shah ', 'Senior Lecturer', 'MBBS, M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(57, 1, 2, '25536-N', '33655/25536-N/M', 'Dr.Sara Khan ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(58, 1, 3, 'Non Medical', '8054/Non Medical', 'Prof. Dr. M. Hussain Khan', 'Professor', 'PhD', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(59, 1, 3, 'Non Medical', 'NR', 'Dr.Kehkeshan Akbar', 'Assistant Professor', ' M. Phil,P.hd', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(60, 1, 3, '11840-N', 'AF', 'Dr. Saadia Sadiq', 'Assistant Professor', 'MBBS, M. Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(61, 1, 21, '13678-N', '8277/13678-N/M', 'Dr Gul Mehnaz', 'Assistant Professor', 'MBBS, M.Phil, ', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(62, 1, 21, '67752-P', '31007/67752-P/M', 'Dr. Saba Yaqoob', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(63, 1, 21, '21869-N', '33654/21869-N/M', 'Dr.Nasar Khan ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(64, 1, 20, '3380-N', 'AF', 'Dr.Qazi M.Qasim ', 'Assistant Professor', 'MBBS, M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(65, 1, 20, '10224-N', '30542/10224-N/M', 'Dr. Sadaf Farid', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(66, 1, 20, '26840-N', '33388/26840-N/M', 'Dr.Maryam Shafiq ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(73, 2, 1, '3249-N', '3158/3249-N/M', 'Prof. Dr. Rashid Ahmad', 'Professor', 'MBBS, FRCS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(74, 2, 1, '1394-P', 'AF', 'Prof.Dr.Rameez Iqbal Hashmi', 'Professor', 'MBBS, M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(75, 2, 1, '17511-N', 'AF', 'Dr. Muhammad Kamran', 'Assistant Professor', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(76, 2, 1, ' ', 'AF', 'Dr.Bilal Hassan', 'Assistant Professor', 'MBBS,M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(77, 2, 1, '1411-N', '30552/1411-N/M', 'Dr. Mohib-ullah Wazir', 'Senior Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(78, 2, 1, '13395-N', '30525/13395-N/M', 'Dr. Atif Nawaz Khan', 'Senior Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(79, 2, 1, '31047-N', '35386/31047-N/M', 'Dr. Sadia Ejaz', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(80, 2, 1, '31139-N', '36084/31139-N/M', 'Dr.Awais Ahmad Khan', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(81, 2, 1, '22500-N', '30541/22500-N/M', 'Dr.Fazal Khaliq', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(82, 2, 1, '21887-N', 'AF', 'Dr.Aisha Jalal', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(83, 2, 1, '', 'AF', 'Dr. Izza Iftikhar', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(84, 2, 1, '', 'AF', 'Dr.Saad Niaz', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(85, 2, 1, '27749-N', 'AF', 'Dr. Virda Haroon', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(86, 2, 2, '2589-N', '3691/2589-N/M', 'Prof. Dr.Tariq Mehmood Khan ', 'Professor', 'MBBS, M.Phil (Physiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(87, 2, 2, '1920-P', '7817/1920-P/M', 'Prof. Dr.Masood Ahmed ', 'Professor', 'MBBS, M.Phil (Physiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(88, 2, 2, '13377-D', 'AF', 'Dr.Kinza Sammar', 'Assistant Professor', 'BDS, M.Phil    (Physiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(89, 2, 2, '20960-N', '33387/20960-N/M', 'Dr.Maheen Shah ', 'Assistant Professor', 'MBBS, M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(90, 2, 2, '27595-P', '30551/27595-P/M', 'Dr. Fouzia Naweed', 'Senior Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(91, 2, 2, '31242-N', '36511/31242-N/M', 'Dr.Sania Naseem Sabir', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(92, 2, 2, '30959-N', 'AF', 'Dr.Sofia Ali', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(93, 2, 2, '31140-N', '36086/31140-N/M', 'Dr.Aftab Ahmad', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(94, 2, 2, '', 'AF', 'Dr. Tooba Jadoon', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(95, 2, 2, '', 'AF', 'Dr. Fatima Ajmal', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(96, 2, 2, '', 'AF', 'Dr. Hamed Mahabat Khan', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(97, 2, 2, '25536-N', '33655/25536-N/M', 'Dr.Sara Khan ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(98, 2, 3, 'Non Medical', '8054/Non Medical', 'Prof. Dr. M. Hussain Khan', 'Professor', 'PhD', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(99, 2, 3, '2149-N', 'AF', 'Prof.Dr .Toqeer Iqbal', 'Professor', 'MBBS, M. Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(100, 2, 3, 'Non Medical', 'AF', 'Dr.Kehkeshan Akbar', 'Assistant Professor', 'M.SC, M. Phil,P.hd(Genetics)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(101, 2, 3, '29301-N', '36512/29301-N/M', 'Dr.Jawad Ahmad', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(102, 2, 3, '29283-N', '36083/29283-N/M', 'Dr.Ahmad Ali', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(103, 2, 3, '28367-N', '36024/28367-N/M', 'Dr.Maria Khalid', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(104, 2, 3, 'Non Medical', 'NR', 'Miss Anila Farid', 'Lec/ Lab Tech', 'M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(105, 2, 21, '585-N', '8058/585-N', 'Prof.Dr. Abdul Rashid', 'Professor', 'MBBS, M.Phil, \nDoctor of Medicine', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(106, 2, 21, '3532-P', 'AF', 'Dr. Rubeena Hameed', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(107, 2, 21, '30996-N', '36027/30996-N/M', 'Dr.Zanib Ali', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(108, 2, 21, '21869-N', '33654/21869-N/M', 'Dr. Nasar Khan ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(109, 2, 21, '67752-P', '31007/67752-P/M', 'Dr. Saba Yaqoob', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(110, 2, 21, '5656-B', '28857/5656-B/M', 'Dr. Afeefa Iqbal Tanoli ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(111, 2, 20, '1966-N', '0364/1966-N/M', 'Maj.Gen? .Prof. Dr.M. Ayub ', 'Professor', 'MBBS,Ph.D,MRCPATH, FRCPATH (UK)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(112, 2, 20, '2769-N', 'AF', 'Prof.Dr.Ali Fawad Nazar', 'Professor', 'MBBS,M.Phil(Microbiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(113, 2, 20, '', '', 'Prof. Dr. Arshad Pervaiz', 'Assistant Professor', '', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(114, 2, 20, '3380-N', 'AF', 'Dr.Qazi Muhammad Qasim', 'Assistant Professor', 'MBBS,M.Phil(Microbiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(115, 2, 20, '45275-P', '16326/45275-P/M', 'Dr. Amna Khalid', 'Assistant Professor', 'MBBS, M.Phil(Haematology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(116, 2, 20, '22440-N', '34456/22440-N/M', 'Dr. Muhammad Adil Ayub ', 'Assistant Professor', 'MBBS,M.Phil', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(117, 2, 20, '10224-N', '30542/10224-N/M', 'Dr. Sadaf Farid', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(118, 2, 20, '29398-N', 'AF', 'Dr.Nosheen Rahim', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(119, 2, 20, '26840-N', '33388/26840-N/M', 'Dr. Maryam Shafiq ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(120, 2, 20, 'Non Medical', 'NR', 'Miss Samina', 'Lec /Lab Tech', 'M.Phil (Microbiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(121, 2, 7, '4318-N', '4929/4318-N/M', 'Dr. Shagufta Shafi', 'Professor', 'MBBS, DIP of Medical Jurisprudence', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(122, 2, 7, '4834-N', '9584/4834-N/M', 'Dr.Sayed Muhammad Yadain', 'Associate Professor', 'MBBS, DIP of Medical Jurisprudence', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(123, 2, 7, '31129-N', 'AF', 'Dr. Anum Rafique', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(124, 2, 8, '1002-P', '8461/1002-P/M', 'Brig. ? Prof. Dr. Arshad Wahab Shah ', 'Professor', 'MBBS, DPH,M.Sc', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(125, 2, 8, '13336-N', '29982/13336-N/M', 'Dr. Anwar Shahzad', 'Associate  Professor', 'MBBS, MPH', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(126, 2, 8, '9597-N', '6130/9597-N/M', 'Dr. Rehana Rasool', 'Assistant Professor', 'MBBS, MPH', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(127, 2, 8, '1105-N', '11667/11055-N/M', 'Dr.Fardah Yunus M.Khan ', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(128, 2, 8, '26046-N', 'AF', 'Dr.Hafsa Habib', 'Lecturer', 'MBBS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(129, 2, 11, '2876-N', '3186/21867-N/M', 'Prof. Dr. Abdul Aziz Awan', 'Professor', 'MBBS, FRCS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(130, 2, 11, '2495-N', '1234/2495-N/M', 'Prof.Dr Abdus Salam Arif', 'Professor', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(131, 2, 10, '4154-N', '3154/4154-N/M', 'Prof. Dr. M. Yousaf', 'Professor', 'MBBS, DLO, MCPS, FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(132, 2, 10, '', 'AF', 'Dr. Tabassum Aziz', 'Assistant Professor', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(133, 2, 10, '', 'AF', 'Dr.Akif Rafi', 'Senior Registrar', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(141, 2, 15, '609-B', '3263/609-B/M', 'Prof. Dr. Mumtaz Burki', 'Professor', 'MBBS,FAMS,FVMA Paeds', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(142, 2, 15, '19059-N', '15205/19059-N/M', 'Dr.Amna Khan ', 'Assistant Professor', 'MBBS,FCPS (Paediatrics)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(143, 2, 15, '10972-N', 'AF', 'Dr.Mukhtiar ', 'Assistant Professor', 'MBBS,M.s,MPH', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(144, 2, 15, '17972-N', '11953/17972-N/M', 'Dr.Muhammad Adnan', 'Assistant Professor', 'MBBS,FCPS (Paediatrics)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(145, 2, 15, '64549-P', 'AF', 'Dr.Ifnan Shamraiz', 'Senior Registrar', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(146, 2, 14, '441-AJK', 'AF', 'Prof. Dr. Nasreen Abbasi', 'Professor', 'MBBS, FCPS,(Gynaechology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(147, 2, 14, '11441-N', 'AF', 'Dr.Saima Iltaf', 'Assistant Professor', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(148, 2, 14, '4602-N', 'AF', 'Brig. ? Dr.Shahida Sheraz   ', 'Assistant Professor', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(149, 2, 14, '1933-N', 'AF', 'Dr.Sara Khan', 'Senior Registrar', 'MBBS, FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(150, 2, 14, '20261-N', 'AF', 'Dr .Anaheeta Israr', 'Senior Registrar', 'MBBS ,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(151, 2, 14, '15488-N', '36090/15488-N/M', 'Dr.Sofia Shabbir Khan', 'Senior Registrar', 'MBBS, FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(162, 2, 13, '1096-N', '3744/1096-N/M', 'Prof. Dr.Shamsher Ali Khan', 'Professor', 'MBBS, MCPS, M.Sc(Anaesthesiology)', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(163, 2, 13, '14266-P', 'AF', 'Brig ? Prof.Dr Saleem Pervaz Bajwa', 'Associate Professor', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(164, 2, 12, '19114-N', 'AF', 'Dr.Nudrat Jehangir', 'Senior Registrar', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(169, 2, 13, '13054-N', 'AF', 'Dr.Muhammad Ali Usman', 'Assistant Professor ', 'MD,Masters Of Surgery', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL),
(170, 2, 13, '19037-N', 'NR', 'Dr.Babar Shehzad', 'Senior Registrar', 'MBBS,FCPS', '', '', '', '', '2021-07-31 14:42:48', '2021-07-31 14:42:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Rizwan Shah', 'rizwan@aimi.edu.pk', '0000-00-00 00:00:00', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'P84JaEAYBmvVKkRbbxeZVbNbVB9S05zmNOITWddCvfDEn1fpfKmMVfYcVpuY', '2021-07-31 14:42:48', '2021-07-31 14:42:48'),
(15, 2, 'Faisal Ibrahim', 'ifaisal88@gmail.com', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', NULL, '2021-07-31 14:42:48', '2021-07-31 14:42:48'),
(16, 3, 'Ahmad Faraz', 'faraz543@gmail.com', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', NULL, '2021-07-31 14:42:48', '2021-07-31 10:39:53'),
(17, 4, 'Faisal Haroon', 'faisal@gmail.com', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', NULL, '2021-07-31 14:42:48', '2021-07-31 14:42:48'),
(18, 5, 'Farazz', 'farazzz@gamil.com', NULL, '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', NULL, '2021-07-31 14:42:48', '2021-07-31 14:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_login_histories`
--

CREATE TABLE `user_login_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `logged_in_date` date NOT NULL,
  `browser_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_a_successful_attempt` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_module_permissions`
--

CREATE TABLE `user_module_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `permission_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_statuses`
--
ALTER TABLE `attendance_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grades_department_id_foreign` (`department_id`);

--
-- Indexes for table `grade_subjects`
--
ALTER TABLE `grade_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_subjects_subject_id_foreign` (`subject_id`),
  ADD KEY `grade_subjects_grade_id_foreign` (`grade_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slots`
--
ALTER TABLE `slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_grade_id_foreign` (`grade_id`),
  ADD KEY `students_department_id_foreign` (`department_id`);

--
-- Indexes for table `student_attendances`
--
ALTER TABLE `student_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_attendances_student_id_foreign` (`student_id`),
  ADD KEY `student_attendances_student_timetable_id_foreign` (`student_timetable_id`),
  ADD KEY `student_attendances_created_by_id_foreign` (`created_by_id`);

--
-- Indexes for table `student_departments`
--
ALTER TABLE `student_departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_departments_student_id_foreign` (`student_id`),
  ADD KEY `student_departments_department_id_foreign` (`department_id`),
  ADD KEY `student_departments_created_by_id_foreign` (`created_by_id`);

--
-- Indexes for table `student_grades`
--
ALTER TABLE `student_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_grades_grade_id_foreign` (`grade_id`),
  ADD KEY `student_grades_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_leaves_student_id_foreign` (`student_id`),
  ADD KEY `student_leaves_leave_type_id_foreign` (`leave_type_id`);

--
-- Indexes for table `student_timetables`
--
ALTER TABLE `student_timetables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_timetables_grade_id_foreign` (`grade_id`),
  ADD KEY `student_timetables_session_id_foreign` (`session_id`),
  ADD KEY `student_timetables_subject_id_foreign` (`subject_id`),
  ADD KEY `student_timetables_teacher_id_foreign` (`teacher_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teachers_department_id_foreign` (`department_id`),
  ADD KEY `teachers_subject_id_foreign` (`subject_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_login_histories`
--
ALTER TABLE `user_login_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_login_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_module_permissions`
--
ALTER TABLE `user_module_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_module_permissions_module_id_foreign` (`module_id`),
  ADD KEY `user_module_permissions_role_id_foreign` (`role_id`),
  ADD KEY `user_module_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_roles_user_id_foreign` (`user_id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_statuses`
--
ALTER TABLE `attendance_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `grade_subjects`
--
ALTER TABLE `grade_subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_types`
--
ALTER TABLE `leave_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slots`
--
ALTER TABLE `slots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT for table `student_attendances`
--
ALTER TABLE `student_attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_departments`
--
ALTER TABLE `student_departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT for table `student_grades`
--
ALTER TABLE `student_grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT for table `student_leaves`
--
ALTER TABLE `student_leaves`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_timetables`
--
ALTER TABLE `student_timetables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_login_histories`
--
ALTER TABLE `user_login_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_module_permissions`
--
ALTER TABLE `user_module_permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);

--
-- Constraints for table `grade_subjects`
--
ALTER TABLE `grade_subjects`
  ADD CONSTRAINT `grade_subjects_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`),
  ADD CONSTRAINT `grade_subjects_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `students_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`);

--
-- Constraints for table `student_attendances`
--
ALTER TABLE `student_attendances`
  ADD CONSTRAINT `student_attendances_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `teachers` (`id`),
  ADD CONSTRAINT `student_attendances_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `student_attendances_student_timetable_id_foreign` FOREIGN KEY (`student_timetable_id`) REFERENCES `student_timetables` (`id`);

--
-- Constraints for table `student_departments`
--
ALTER TABLE `student_departments`
  ADD CONSTRAINT `student_departments_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `student_departments_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `student_departments_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `student_grades`
--
ALTER TABLE `student_grades`
  ADD CONSTRAINT `student_grades_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`),
  ADD CONSTRAINT `student_grades_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `student_leaves`
--
ALTER TABLE `student_leaves`
  ADD CONSTRAINT `student_leaves_leave_type_id_foreign` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_types` (`id`),
  ADD CONSTRAINT `student_leaves_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `student_timetables`
--
ALTER TABLE `student_timetables`
  ADD CONSTRAINT `student_timetables_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`),
  ADD CONSTRAINT `student_timetables_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`),
  ADD CONSTRAINT `student_timetables_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`),
  ADD CONSTRAINT `student_timetables_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`);

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `teachers_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_login_histories`
--
ALTER TABLE `user_login_histories`
  ADD CONSTRAINT `user_login_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_module_permissions`
--
ALTER TABLE `user_module_permissions`
  ADD CONSTRAINT `user_module_permissions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`),
  ADD CONSTRAINT `user_module_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `user_module_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
