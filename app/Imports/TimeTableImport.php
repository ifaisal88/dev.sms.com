<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Illuminate\Validation\Rule;

class TimeTableImport implements ToModel, WithValidation, WithHeadingRow, SkipsOnError
{
    use Importable;
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        dd($row);
        // $teacher = Teacher::where('name', $row['company'])->first();
        // $status = AttendanceStatus::where('name', $row['attendance_status'])->first();
        // EmployeeAttendance::create([
        //     'employee_id' => $row['employee_id'],
        //     'company_id' => $company->id,
        //     'attendance_date' => $row['date'],
        //     'attendance_status_id' => !is_null($status) ? $status->id : '',
        //     'checkin_time' => $row['check_in_time'],
        //     'checkout_time' => $row['check_out_time'],
        //     'remarks' => $row['remarks']
        // ]);
    }

    public function rules(): array
    {
        return [
            'session' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Session is required');
                }
            },
            'day' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Day Id is required');
                }
            },
            'class' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Class is required');
                }
            },
            'subject' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Subject is required');
                }
            },
            'teacher' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Teacher is required');
                }
            },
            'date' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Date is required');
                }
            },
            'start_time' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Start Time is required');
                }
            },
            'end_time' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('End Time is required');
                }
            },
            'status' => function ($attribute, $value, $onFailure) {
                if ($value == '') {
                    $onFailure('Status is required');
                }
            },
        ];
    }

    public function onError(\Throwable $e)
    {
        throw new \Exception($e);
    }
}
