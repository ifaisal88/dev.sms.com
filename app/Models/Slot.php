<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Slot
 * 
 * @property int $id
 * @property string $name
 * @property Carbon $start_time
 * @property Carbon $end_time
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|StudentTimetable[] $student_timetables
 *
 * @package App\Models
 */
class Slot extends Model
{
	use SoftDeletes;
	protected $table = 'slots';

	protected $casts = [
		'status' => 'bool'
	];

	protected $dates = [
		'start_time',
		'end_time'
	];

	protected $fillable = [
		'name',
		'start_time',
		'end_time',
		'status'
	];

	public function student_timetables()
	{
		return $this->hasMany(StudentTimetable::class);
	}
}
