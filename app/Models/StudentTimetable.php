<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StudentTimetable
 *
 * @property int $id
 * @property int|null $grade_id
 * @property int|null $session_id
 * @property int|null $subject_id
 * @property int|null $slot_id
 * @property int|null $teacher_id
 * @property bool|null $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Grade|null $grade
 * @property Session|null $session
 * @property Slot|null $slot
 * @property Subject|null $subject
 * @property Teacher|null $teacher
 * @property Collection|StudentAttendance[] $student_attendances
 *
 * @package App\Models
 */
class StudentTimetable extends Model
{
	use SoftDeletes;
	protected $table = 'student_timetables';

	protected $casts = [
		'grade_id' => 'int',
		'session_id' => 'int',
		'subject_id' => 'int',
		'teacher_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'grade_id',
		'session_id',
		'subject_id',
		'teacher_id',
        'topic_name',
		'status',
		'start_time',
		'end_time',
		'day',
		'date'
	];

	public function grade()
	{
		return $this->belongsTo(Grade::class);
	}

	public function session()
	{
		return $this->belongsTo(Session::class);
	}

	public function subject()
	{
		return $this->belongsTo(Subject::class);
	}

	public function teacher()
	{
		return $this->belongsTo(Teacher::class);
	}

	public function student_attendances()
	{
		return $this->hasMany(StudentAttendance::class);
	}

	// public function getDateAttribute($date)
	// {
	// 	return new Carbon($date);
	// }

	public function scopeTimetableFilter($query, $startDate, $endDate, $session, $grade = null)
	{
		$query->whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('session_id', $session);

		if(!is_null($grade)){
			$query->where('grade_id', $grade->id);
		}

		$query->orderBy('date', 'ASC');

		return $query;
	}
}
