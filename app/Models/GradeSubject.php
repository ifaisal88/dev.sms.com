<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Teacher;

/**
 * Class GradeSubject
 *
 * @property int $id
 * @property int|null $subject_id
 * @property int|null $grade_id
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Grade|null $grade
 * @property Subject|null $subject
 *
 * @package App\Models
 */
class GradeSubject extends Model
{
	use SoftDeletes;
	protected $table = 'grade_subjects';

	protected $casts = [
		'subject_id' => 'int',
		'grade_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'subject_id',
		'grade_id',
		'status'
	];

	public function grade()
	{
		return $this->belongsTo(Grade::class);
	}

	public function subject()
	{
		return $this->belongsTo(Subject::class);
	}
}
