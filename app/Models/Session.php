<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Session
 * 
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|StudentTimetable[] $student_timetables
 *
 * @package App\Models
 */
class Session extends Model
{
	use SoftDeletes;
	protected $table = 'sessions';

	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function student_timetables()
	{
		return $this->hasMany(StudentTimetable::class);
	}

	public function scopeActiveSession($query)
	{
		$currentYear = Carbon::now();
        $nextYear = $currentYear->copy()->addYears(1);
        $sessionName = $currentYear->year . '-' . $nextYear->format('y');
        return $query->where('name', $sessionName);
	}
}
