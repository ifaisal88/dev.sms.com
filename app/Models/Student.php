<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Student
 *
 * @property int $id
 * @property string $registration_number
 * @property string $full_name
 * @property string|null $address
 * @property string|null $mobile_number
 * @property string|null $remarks
 * @property string|null $finger_print
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|StudentAttendance[] $student_attendances
 * @property Collection|Department[] $departments
 * @property Collection|Grade[] $grades
 * @property Collection|StudentLeafe[] $student_leaves
 *
 * @package App\Models
 */
class Student extends Model
{
	use SoftDeletes;
	protected $table = 'students';

	protected $fillable = [
		'registration_number',
		'full_name',
		'remarks',
		'finger_print',
		'grade_id',
		'department_id',
		'mobile_number',
		'address'
	];

	public function student_attendances()
	{
		return $this->hasMany(StudentAttendance::class, 'student_id');
	}

	public function department()
	{
		return $this->belongsTo(Department::class);
	}

	public function grade()
	{
		return $this->belongsTo(Grade::class);
	}

	public function student_leaves()
	{
		return $this->hasMany(StudentLeafe::class);
	}
}
