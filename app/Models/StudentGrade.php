<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StudentGrade
 * 
 * @property int $id
 * @property int|null $grade_id
 * @property int|null $student_id
 * @property bool|null $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Grade|null $grade
 * @property Student|null $student
 *
 * @package App\Models
 */
class StudentGrade extends Model
{
	use SoftDeletes;
	protected $table = 'student_grades';

	protected $casts = [
		'grade_id' => 'int',
		'student_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'grade_id',
		'student_id',
		'status'
	];

	public function grade()
	{
		return $this->belongsTo(Grade::class);
	}

	public function student()
	{
		return $this->belongsTo(Student::class);
	}
}
