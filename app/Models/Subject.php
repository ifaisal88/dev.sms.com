<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subject
 *
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|Grade[] $grades
 * @property Collection|StudentTimetable[] $student_timetables
 *
 * @package App\Models
 */
class Subject extends Model
{
	use SoftDeletes;
	protected $table = 'subjects';

	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function grades()
	{
		return $this->belongsToMany(Grade::class, 'grade_subjects')
					->withPivot('id', 'status', 'deleted_at')
					->withTimestamps();
	}

	public function student_timetables()
	{
		return $this->hasMany(StudentTimetable::class);
	}

	public function teachers()
	{
		return $this->hasMany(Teacher::class);
	}
}
