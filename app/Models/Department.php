<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Department
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $name
 * @property bool|null $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|Student[] $students
 *
 * @package App\Models
 */
class Department extends Model
{
	use SoftDeletes;
	protected $table = 'departments';

	protected $casts = [
		'parent_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'parent_id',
		'name',
		'status'
	];

	public function students()
	{
		return $this->belongsToMany(Student::class, 'student_departments')
					->withPivot('id', 'created_by_id', 'status', 'deleted_at')
					->withTimestamps();
	}

	public function teachers()
	{
		return $this->hasMany(Teacher::class);
	}

	public function grades()
	{
		return $this->hasMany(Grade::class);
	}
}
