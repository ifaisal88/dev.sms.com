<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserLoginHistory
 * 
 * @property int $id
 * @property int|null $user_id
 * @property Carbon $logged_in_date
 * @property string $browser_type
 * @property bool $is_a_successful_attempt
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property User|null $user
 *
 * @package App\Models
 */
class UserLoginHistory extends Model
{
	use SoftDeletes;
	protected $table = 'user_login_histories';

	protected $casts = [
		'user_id' => 'int',
		'is_a_successful_attempt' => 'bool'
	];

	protected $dates = [
		'logged_in_date'
	];

	protected $fillable = [
		'user_id',
		'logged_in_date',
		'browser_type',
		'is_a_successful_attempt'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
