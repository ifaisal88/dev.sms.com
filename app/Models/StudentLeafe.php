<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StudentLeafe
 * 
 * @property int $id
 * @property int|null $student_id
 * @property int|null $leave_type_id
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property string|null $remarks
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property LeaveType|null $leave_type
 * @property Student|null $student
 *
 * @package App\Models
 */
class StudentLeafe extends Model
{
	use SoftDeletes;
	protected $table = 'student_leaves';

	protected $casts = [
		'student_id' => 'int',
		'leave_type_id' => 'int',
		'status' => 'bool'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];

	protected $fillable = [
		'student_id',
		'leave_type_id',
		'start_date',
		'end_date',
		'remarks',
		'status'
	];

	public function leave_type()
	{
		return $this->belongsTo(LeaveType::class);
	}

	public function student()
	{
		return $this->belongsTo(Student::class);
	}
}
