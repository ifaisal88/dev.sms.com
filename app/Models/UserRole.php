<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserRole
 * 
 * @property int $id
 * @property int|null $user_id
 * @property int|null $role_id
 * @property bool|null $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Role|null $role
 * @property User|null $user
 *
 * @package App\Models
 */
class UserRole extends Model
{
	use SoftDeletes;
	protected $table = 'user_roles';

	protected $casts = [
		'user_id' => 'int',
		'role_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'user_id',
		'role_id',
		'status'
	];

	public function role()
	{
		return $this->belongsTo(Role::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
