<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AttendanceStatus
 * 
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|StudentAttendance[] $student_attendances
 *
 * @package App\Models
 */
class AttendanceStatus extends Model
{
	use SoftDeletes;
	protected $table = 'attendance_statuses';

	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function student_attendances()
	{
		return $this->hasMany(StudentAttendance::class);
	}
}
