<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserModulePermission
 * 
 * @property int $id
 * @property int|null $module_id
 * @property int|null $role_id
 * @property int|null $permission_id
 * @property bool|null $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Module|null $module
 * @property Permission|null $permission
 * @property Role|null $role
 *
 * @package App\Models
 */
class UserModulePermission extends Model
{
	use SoftDeletes;
	protected $table = 'user_module_permissions';

	protected $casts = [
		'module_id' => 'int',
		'role_id' => 'int',
		'permission_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'module_id',
		'role_id',
		'permission_id',
		'status'
	];

	public function module()
	{
		return $this->belongsTo(Module::class);
	}

	public function permission()
	{
		return $this->belongsTo(Permission::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class);
	}
}
