<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StudentDepartment
 * 
 * @property int $id
 * @property int|null $student_id
 * @property int|null $department_id
 * @property int|null $created_by_id
 * @property bool|null $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property User|null $user
 * @property Department|null $department
 * @property Student|null $student
 *
 * @package App\Models
 */
class StudentDepartment extends Model
{
	use SoftDeletes;
	protected $table = 'student_departments';

	protected $casts = [
		'student_id' => 'int',
		'department_id' => 'int',
		'created_by_id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'student_id',
		'department_id',
		'created_by_id',
		'status'
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'created_by_id');
	}

	public function department()
	{
		return $this->belongsTo(Department::class);
	}

	public function student()
	{
		return $this->belongsTo(Student::class);
	}
}
