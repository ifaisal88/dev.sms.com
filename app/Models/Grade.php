<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Grade
 *
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|Subject[] $subjects
 * @property Collection|Student[] $students
 * @property Collection|StudentTimetable[] $student_timetables
 *
 * @package App\Models
 */
class Grade extends Model
{
	use SoftDeletes;
	protected $table = 'grades';

	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function subjects()
	{
		return $this->belongsToMany(Subject::class, 'grade_subjects')
					->withPivot('id', 'status', 'deleted_at')
					->withTimestamps();
	}

	public function students()
	{
		// return $this->belongsToMany(Student::class, 'student_grades')
		// 			->withPivot('id', 'status', 'deleted_at')
		// 			->withTimestamps();
		return $this->hasMany(Student::class);
	}

	public function student_timetables()
	{
		return $this->hasMany(StudentTimetable::class);
	}

	public function department()
	{
		return $this->belongsTo(Department::class);
	}
}
