<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Teacher
 *
 * @property int $id
 * @property string $employee_number
 * @property string $full_name
 * @property string|null $address
 * @property string|null $mobile_number
 * @property string|null $remarks
 * @property string|null $finger_print
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|StudentTimetable[] $student_timetables
 *
 * @package App\Models
 */
class Teacher extends Authenticatable implements JWTSubject
{
    use SoftDeletes, Notifiable;
    protected $table = 'teachers';

    protected $fillable = [
        'employee_number',
        'full_name',
        'address',
        'mobile_number',
        'remarks',
        'pmdc_reg_no',
        'designation',
        'qualification',
        'finger_print',
        'department_id',
        'subject_id'
    ];

    public function student_timetables()
    {
        return $this->hasMany(StudentTimetable::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function scopeCurrentTimeTable($query)
    {
        $date = Carbon::now('Asia/Karachi');
        return $query->with(['student_timetables' => function ($q) use ($date) {
            $q->whereDate('date', $date)->whereTime('start_time', "<", $date)->whereTime('end_time', ">", $date);
        }]);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        // $timeTable = $this->currentTimeTable();
        return [
            'teacher_id' => $this->id,
            'time_table_id' => count($this->student_timetables) > 0?$this->student_timetables[0]->id:null
        ];
    }
}
