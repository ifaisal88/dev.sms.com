<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StudentAttendance
 * 
 * @property int $id
 * @property int|null $student_id
 * @property int|null $student_timetable_id
 * @property int|null $attendance_status_id
 * @property Carbon $attendance_date
 * @property bool $attendance_marked_type
 * @property int $created_by_id
 * @property string|null $remarks
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property AttendanceStatus|null $attendance_status
 * @property User $user
 * @property Student|null $student
 * @property StudentTimetable|null $student_timetable
 *
 * @package App\Models
 */
class StudentAttendance extends Model
{
	use SoftDeletes;
	protected $table = 'student_attendances';

	protected $casts = [
		'student_id' => 'int',
		'student_timetable_id' => 'int',
		'attendance_status_id' => 'int',
		'attendance_marked_type' => 'bool',
		'created_by_id' => 'int',
		'attendance_date' => 'date:Y-m-d',
	];

	protected $dates = [
		'attendance_date'
	];

	protected $fillable = [
		'student_id',
		'student_timetable_id',
		'attendance_status',
		'attendance_date',
		'attendance_marked_type',
		'created_by_id',
		'remarks'
	];

	public function attendance_status()
	{
		return $this->belongsTo(AttendanceStatus::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'created_by_id');
	}

	public function student()
	{
		return $this->belongsTo(Student::class);
	}

	public function student_timetable()
	{
		return $this->belongsTo(StudentTimetable::class);
	}

	public function createdBy()
	{
		return $this->belongsTo(Teacher::class, 'created_by_id');
	}
}
