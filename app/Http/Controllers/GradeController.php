<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GradeRequest;
use App\Models\Session;
use App\Models\StudentTimetable;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::all();
        return view('grades', ['grades' => $grades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeRequest $request)
    {
        Grade::create($request->all());
        return redirect()->back()->with('status', 'Grade created successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Grade::firstWhere('id', $id);
        return view('edit-grades', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GradeRequest $request, $id)
    {
        $grade = Grade::find($id);
        $grade->name = $request->name;
        $grade->status = $request->status;
        $grade->save();
        return redirect()->back()->with('status', 'Grade Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function subjects(Grade $grade)
    {
        $subjects = Subject::whereHas('grades', function ($query) use ($grade) {
            $query->where('grade_id', $grade->id);
        })->get(['subjects.id AS id', 'subjects.name AS text']);

        return response()->json(['items' => $subjects->toArray()]);
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function gradesAttendacne()
    {
        $teachers = Teacher::all();
        $grades = Grade::get();
        return view('custom-attendance', ['grades' => $grades,'teachers' => $teachers]);
    }

    /**
     * return students for calss.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function students(Grade $grade, Request $request)
    {
        $teacherId = $request->teacher_id;
        $subjectId = $request->subject_id;
        $gradeId = $request->grade_id;
        $currentYear = new Carbon($request->date);
        $currentYear = Carbon::createFromFormat('Y-m-d H:i:s', $request->date . ' ' . $request->start_time);
        $nextYear = $currentYear->copy()->addYears(1);
        $sessionName = $currentYear->year . '-' . $nextYear->format('y');
        $sessionId = Session::where('name', $sessionName)->pluck('id')->first();

        $timetbale = StudentTimetable::whereDate('date', $request->date)->where('grade_id', $gradeId)
            ->where('teacher_id', $teacherId)->where('subject_id', $subjectId)->where('session_id', $sessionId)
            ->whereTime('start_time', "<=", $currentYear)->whereTime('end_time', '>=', $currentYear)->first();
        if ($timetbale) {
            $students = Student::with(['student_attendances' => function ($query) use ($timetbale) {
                $query->where('student_timetable_id', $timetbale->id);
            }])->where('grade_id', $gradeId)->select(['students.id AS id', 'students.full_name AS name'])->get();
        } else {
            return response()->json(['data' => []]);
        }
        return response()->json(['data' => $students, 'student_timetable_id' => $timetbale->id]);
    }
    public function subjectsDropDown(Grade $grade)
    {
        $subjects = Subject::whereHas('grades', function ($query) use ($grade) {
            $query->where('grade_id', $grade->id);
        })->get(['subjects.id AS id', 'subjects.name AS text']);

        return response()->json(['items' => $subjects]);
    }
}
