<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Grade;
use App\Models\Student;
use App\Models\Department;
use App\Models\Teacher;
use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\Models\Session;
use App\Models\StudentAttendance;
use App\Models\StudentTimetable;
use App\Http\Requests\PromotionRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = auth()->user()->role;
        if($role->slug == 'mbbs-student-affairs'){
            $department = Department::where('name', 'MBBS')->get();
        }

        if($role->slug == 'bds-student-affairs'){
            $department = Department::where('name', 'BDS')->get();
        }

        if($role->slug == 'super-admin'){
            $department = Department::all();
            $grades = Grade::get();
            $students = Student::get();
        }else{

            $grades = Grade::where('department_id', $department[0]->id)->get();
            $students = Student::where('department_id', $department[0]->id)->get();
        }
        return view('students', ['department' => $department, 'grades' => $grades, 'students' => $students]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        Student::create($request->all());
        return redirect()->route('students.read.index')->with('status', 'Student Created Successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = auth()->user()->role;
        if($role->slug == 'mbbs-student-affairs'){
            $department = Department::where('name', 'MBBS')->get();
        }
        if($role->slug == 'bds-student-affairs'){
            $department = Department::where('name', 'BDS')->get();
        }

        if($role->slug == 'super-admin'){
            $department = Department::all();
            $grades = Grade::get();
        }else{

            $grades = Grade::where('department_id', $department->id)->get();
        }
        $data = Student::find($id);

        return view('edit-student', ['department' => $department, 'grades' => $grades, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(StudentRequest $request, $id)
    {
        $request->merge(['id' => $id]);
        Student::updateOrCreate(['id' => $id], [
            'registration_number' => $request->registration_number,
            'full_name' => $request->full_name,
            'department_id' => $request->department_id,
            'grade_id' => $request->grade_id,
            'mobile_number' => $request->mobile_number,
            'address' => $request->address,
            'remarks' => $request->remarks
        ]);
        return redirect()->back()->with('status', 'Student is Updated Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

    public function report()
    {
        $students = Student::all();

        return view('students-report', ['students' => $students]);
    }

    public function attendance(Request $request)
    {
        $timetable = StudentTimetable::where('grade_id', $request->grade_id)
                                     ->where('start_time', $request->start_time)
                                     ->where('end_time', $request->end_time)
                                     ->where('date', $request->date)
                                     ->where('teacher_id', $request->teacher_id)
                                     ->where('subject_id', $request->subject_id)
                                     ->with('grade')
                                     ->with('session')
                                     ->with('subject')
                                     ->with('teacher')
                                     ->get();

        // $attendances = StudentAttendance::where('student_timetable_id', $timetable[0]->id)->with('student')->get();
        
        if($timetable){            
            $students = Student::where('grade_id', $request->grade_id)->orderBy('full_name')->get();
            return view('manual-attendance', ['students' => $students, 'timetable' => $timetable]);
        } else {
            dd("No Timetable Found");
        }
    }

    public function finalAttendance(Request $request)
    {      
        $student_timetable_id = $request->student_timetable_id;
        $timetable_date = $request->timetable_date;
        $attendance_marked_type = $request->attendance_marked_type;
        $remarks = $request->remarks;
        $created_by_id = $request->teacher_id;
        
        $request->request->remove('student_timetable_id');
        $request->request->remove('timetable_date');
        $request->request->remove('attendance_marked_type');
        $request->request->remove('remarks');
        $request->request->remove('teacher_id');
        $count = count($request->all());
        $request->request->remove('checked');
        for($i=1; $i<=$count; $i++)
        {
            if($request[$i] != null)
            {            
                $explode = explode("-", $request->$i);
                StudentAttendance::updateOrCreate([
                    'student_id' => $explode[1],
                    'student_timetable_id' => $student_timetable_id,
                    'created_by_id' => $created_by_id,
                    'attendance_date' => $timetable_date,
                ],
                [
                    'student_id' => $explode[1],
                    'student_timetable_id' => $student_timetable_id,
                    'created_by_id' => $created_by_id,
                    'attendance_date' => $timetable_date,
                    'attendance_marked_type' => $attendance_marked_type,
                    'attendance_status' => $explode[0],
                    'remarks' => $remarks,
                    'created_at' => Carbon::now('Asia/Karachi'),
                    'updated_at' => Carbon::now('Asia/Karachi')
                ]);
            }
        }

        $teachers = Teacher::all();
        $grades = Grade::get();
        return view('custom-attendance', ['grades' => $grades,'teachers' => $teachers])->with('status', 'Attendance marked successfuly');
    }

    public function loadStudents()
    {
        $departments = Department::all();
        return view('student-promotion', ['departments' => $departments]);
    }

    public function showStudents(PromotionRequest $request)
    {
        $departments = Department::all();
        $students = Student::where('grade_id', $request->current_grade)->orderBy('full_name')->get();
        return  view('student-promotion',
        [
            'departments' => $departments,
            'students' => $students,
            'promote_to' => $request->promote_to,
            'current_grade' => $request->current_grade
        ]);
    }

    public function updateStudentGrade(Request $request)
    {
        // dd($request->all());
        $current_grade = $request->current_grade;
        $promote_to = $request->promote_to;
        $departments = Department::all();
        $request->request->remove('current_grade');
        $request->request->remove('promote_to');
        $count = count($request->checked);
        for($i=0; $i<$count; $i++)
        {
            Student::where('registration_number', $request->checked[$i])->update(['grade_id' => $promote_to]);
        }
        return view('student-promotion', ['departments' => $departments]);
    }

    public function gradeStudents($id)
    {
        $students = Student::where('grade_id', $id)->get(['students.id as id', 'students.full_name as  text']);
        return response()->json(['items' => $students]);
    }

    public function allFingerprints()
    {
        $students = Student::get(['students.id AS id', 'students.full_name AS text', 'students.finger_print AS fingerprint']);
        return response()->json($students->toArray());
    }
}
