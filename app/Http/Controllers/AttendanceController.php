<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\StudentTimetable;
use App\Models\StudentAttendance;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Session;
use App\Models\Student;
use App\Http\Requests\AttendanceRequest;
use App\Models\Grade;
use App\Models\Subject;
use Carbon\CarbonPeriod;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $months = [
        //     ['id' => 1, 'name' => 'January'],
        //     ['id' => 2, 'name' => 'February'],
        //     ['id' => 3, 'name' => 'March'],
        //     ['id' => 4, 'name' => 'April'],
        //     ['id' => 5, 'name' => 'May'],
        //     ['id' => 6, 'name' => 'June'],
        //     ['id' => 7, 'name' => 'July'],
        //     ['id' => 8, 'name' => 'August'],
        //     ['id' => 9, 'name' => 'September'],
        //     ['id' => 10, 'name' => 'October'],
        //     ['id' => 11, 'name' => 'November'],
        //     ['id' => 12, 'name' => 'December']
        // ];
        $departments = Department::get();
        return  view('attendance-report-all', ['departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * for Getting students list based on the month and class
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function studentsList(Request $request)
    {
        $currentYear = Carbon::now();
        $nextYear = $currentYear->copy()->addYears(1);
        $sessionName = $currentYear->year . '-' . $nextYear->format('y');
        $sessionId = Session::where('name', $sessionName)->pluck('id')->first();

        $month = new Carbon($currentYear->year . '-' . $request->month);
        $classId = $request->class_id;

        $resources = $events = [];
        $students = Student::with(['student_attendances' => function ($query) use ($sessionId, $classId, $month) {
            $query->whereHas('student_timetable', function ($query) use ($sessionId, $classId) {
                $query->where('session_id', $sessionId)->where('grade_id', $classId);
            })->whereMonth('attendance_date', '=', $month->month);
        }])->whereHas('student_attendances', function ($query) use ($sessionId, $classId, $month) {
            $query->whereHas('student_timetable', function ($query) use ($sessionId, $classId) {
                $query->where('session_id', $sessionId)->where('grade_id', $classId);
            })->whereMonth('attendance_date', '=', $month->month);
        })->get();

        foreach ($students as $student) {
            $resources[] = [
                'id' => 'stu-' . $student->id,
                'title' => $student->full_name,
            ];
            foreach ($student->student_attendances as $attendance) {
                $start = new Carbon($attendance->attendance_date);
                $end = new Carbon($attendance->attendance_date);
                $events[] = [
                    'id' => $attendance->id,
                    'resourceId' => 'stu-' . $student->id,
                    'start' => $start->format('Y-m-d') . "T09:00:00",
                    'end' => $end->format('Y-m-d') . "T16:00:00",
                    'eventBackgroundColor' => $attendance->attendance_status == 'present' ? '#008000' : '#ff0000',
                    'color' => $attendance->attendance_status == 'present' ? '#008000' : '#ff0000',
                    'title' => ucfirst($attendance->attendance_status),
                ];
            }
        }
        return response()->json(['resources' => $resources, 'events' => $events]);
    }

    public function attendanceList(AttendanceRequest $request)
    {
        $department = isset($request->department_id) ? $request->department_id : null;
        $grade = isset($request->grade_id) ? $request->grade_id : null;
        $subject = isset($request->subject_id) ? $request->subject_id : null;
        $startDate = isset($request->from) ? new Carbon($request->from) : null;
        $endDate = isset($request->to) ? new Carbon($request->to) : null;
        $studentsId = isset($request->students_id) ? $request->students_id : null;
        $attendanceStatus = isset($request->attendacne_status) ? $request->attendacne_status : null;

        $query = Student::with(['student_attendances' => function ($query) use ($subject, $startDate, $endDate, $attendanceStatus) {
            $query->whereHas('student_timetable', function ($query) use ($subject, $startDate, $endDate, $attendanceStatus) {
                $query->with('subject');
                if ($subject) {
                    $query->where('subject_id', $subject);
                }
                if ($startDate) {
                    $query->whereDate('date', ">=", $startDate);
                }
                if ($endDate) {
                    $query->whereDate('date', "<=", $endDate);
                }
                if (!is_null($attendanceStatus) && $attendanceStatus != 'all') {
                    $query->where('attendance_status', $attendanceStatus);
                }
            });
        }, 'student_attendances.student_timetable.subject'])->whereHas('student_attendances', function ($query) use ($subject, $startDate, $endDate, $attendanceStatus) {
            $query->whereHas('student_timetable', function ($query) use ($subject, $startDate, $endDate, $attendanceStatus) {
                if ($subject) {
                    $query->where('subject_id', $subject);
                }
                if ($startDate) {
                    $query->whereDate('date', ">=", $startDate);
                }
                if ($endDate) {
                    $query->whereDate('date', "<=", $endDate);
                }
                if (!is_null($attendanceStatus) && $attendanceStatus != 'all') {
                    $query->where('attendance_status', $attendanceStatus);
                }
            });
        })->where('department_id', $department);
        if ($grade) {
            $query->where('grade_id', $grade);
        }
        if ($studentsId) {
            $query->whereIn('id', $studentsId);
        }

        $data = $query->get();
        $departments = Department::get();
        if ($request->type == 'report') {
            $department = Department::find($request->department_id);
            $class = Grade::find($request->grade_id);
            $subject = Subject::find($request->subject_id);
            $allData = [
                'class' => $class,
                'department' => $department,
                'subject' => $subject,
                'data' => $data,
                'startDate' => $startDate,
                'endDate' => $endDate
            ];
            return  view('attendance-reports', $allData);
        }
        return  view('attendance-report-all', ['departments' => $departments, 'data' => $data]);
        // return $query->get();
    }
}
