<?php

namespace App\Http\Controllers;

use File;
use Input;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Session;
use App\Exports\TimeTableExport;
use App\Models\StudentTimetable;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditTimeTableRequest;
use App\Http\Requests\ExcelImportRequest;
use App\Http\Requests\PrintTimeTableRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Requests\StudentTimetableRequest;
use App\Imports\TimeTableImport;
use Symfony\Component\Console\Input\Input as InputInput;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Support\Facades\DB;
use PDF;

class TimeTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = auth()->user()->role;
        $timetables = $grades = $subjects = $teachers = $sessions = [];
        if ($role->slug == 'bds-timetable') {
            $grades = Grade::whereHas('department', function ($query) {
                $query->where('name', 'BDS');
            })->get();
            $timetables = StudentTimetable::whereHas('grade', function ($query) {
                $query->whereHas('department', function ($query) {
                    $query->where('name', 'BDS');
                });
            })->get();
        } else if ($role->slug == 'mbbs-timetable') {
            $grades = Grade::whereHas('department', function ($query) {
                $query->where('name', 'MBBS');
            })->get();
            $timetables = StudentTimetable::whereHas('grade', function ($query) {
                $query->whereHas('department', function ($query) {
                    $query->where('name', 'MBBS');
                });
            })->get();
        } else if ($role->slug == 'super-admin') {
            $grades = Grade::all();
            $timetables = StudentTimetable::all();
        }

        $teachers = Teacher::all();
        $subjects = Subject::all();
        $sessions = Session::where('status', 1)->get();
        return view('timetable', 
        [
            'timetables' => $timetables, 
            'grades' => $grades, 
            'subjects' => $subjects, 
            'sessions' => $sessions, 
            'teachers' => $teachers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentTimetableRequest $request)
    {
		$day=$request->day;
		$grade_id=$request->grade_id;
        StudentTimetable::create($request->all());
        return redirect()->route('timetable.read.index', 
        [
            'day'=>$day,
            'grade_id'=>$grade_id
        ])->with('status', 'Timetable created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentTimetable $timetable)
    {
        $grades = Grade::all();
        $subjects = $timetable->grade->subjects;
        $sessions = Session::all();
        $teachers = Teacher::all();
        return view('edit-timetable', 
        [
            'data' => $timetable, 
            'grades' => $grades, 
            'sessions' => $sessions, 
            'teachers' => $teachers, 
            'subjects' => $subjects
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditTimeTableRequest $request, StudentTimetable $timetable)
    {
        $timetable->update($request->all());
        return redirect()->back()->with('status', 'Timetable is Updated Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export()
    {
        return Excel::download(new TimeTableExport, 'timetbale.xlsx');
    }

    public function import(ExcelImportRequest $request)
    {
        $fileName = $request->file('excel_file')->getClientOriginalName() ? $request->file('excel_file')->getClientOriginalName() : null;
        $upload = Storage::disk('uploads')->put($fileName, File::get(Input::file('excel_file')));
        $file = "upload" . DIRECTORY_SEPARATOR . $fileName;
        dd($file);
        try {
            dd(Excel::import(new TimeTableImport, $file));
            return response()->json(['success' => 'Employees attendance added.']);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            dd($e->failures());
            $errors = [];
            foreach ($e->failures() as $failure) {
                $errors[] = $failure->values()['name'] . ' (' . $failure->values()['employee_id'] . ') ' . $failure->errors()[0];
            }
            throw new \Exception(response($errors));
        }
    }

    public function print()
    {
        $session = Session::orderBy('id', 'desc')->first();

        $startDate = new Carbon();
        $endDate = new Carbon();
        $departments = Department::get();
        $timetables = StudentTimetable::timetableFilter($startDate->startOfWeek()->format('Y-m-d'), $endDate->endOfWeek()->format('Y-m-d'), $session->id)->get();
        $data = [
            'gradeId' => 0,
            'departmentId' => 0,
            'timetables' => $timetables,
            'departments' => $departments,
            'startDate' => $startDate->startOfWeek()->format('Y-m-d'),
            'endDate' => $endDate->endOfWeek()->format('Y-m-d')
        ];
        return view('print-timetable', $data);
    }

    public function printFilter(PrintTimeTableRequest $request)
    {
        $session = Session::orderBy('id', 'desc')->first();
        // $department = isset($request->department_id) ? $request->department_id : null;
        $department = isset($request->department_id) ? Department::find($request->department_id) : null;
        $grade = isset($request->grade_id) ? Grade::find($request->grade_id) : null;
        $classSubjects = $grade->subjects()->pluck('subject_id')->toArray();
        $startDate = isset($request->from) ? new Carbon($request->from) : new Carbon();
        $endDate = isset($request->to) ? new Carbon($request->to) : new Carbon();

        $departments = Department::get();
        $timetables = StudentTimetable::timetableFilter($startDate, $endDate, $session->id, $grade)->orderBy('start_time')->get();

        if ($request->type == 'Print') {
            $currentWeek = $startDate->weekOfYear;
            $timetablesData = [];
            $allSubjects = [];
            foreach ($timetables as $timetable) {
                $date = new Carbon($timetable->date);
                if (in_array($timetable->subject_id, $classSubjects)) {
                    $allSubjects[] = $timetable->subject_id;
                }
                $timetablesData[$date->weekOfYear][$date->format('Y-m-d')][] = [
                    'id' => $timetable->id,
                    'startTime' => $timetable->start_time,
                    'endTime' => $timetable->end_time,
                    'teacher' => $timetable->teacher->full_name,
                    'subject' => $timetable->subject->name,
                    'date' => $date,
                ];
            }

            $data['timetables'] = $timetablesData;
            $data['grade'] = $grade;
            $data['department'] = $department;
            $data['startDate'] = $startDate;
            $data['endDate'] = $endDate;
            $data['subjects'] = array_unique($allSubjects);
            // $pdf = PDF::loadView('print-view-timetable', $data);
            // $pdf->setPaper('a3', 'landscape');
            // return $pdf->download($grade->name.' ('.$startDate->format('Y-m-d').' - '.$endDate->format('Y-m-d').').pdf');
            // return $pdf->download($grade->name . '.pdf');

            // $view = view('print-view-timetable', $data);
            // $html = $view->render();
            // $html = preg_replace('/>\s+</', "><", $html);
            // $pdf = PDF::loadHTML($html);
            // $pdf->setPaper('a3', 'landscape');
            // return $pdf->stream();
            // return $pdf->download('Timetable.pdf');
            return view('print-view-timetable', $data);
        } else {
            $data = [
                'gradeId' => $grade->id,
                'departmentId' => !is_null($department) ? $department->id : $department,
                'timetables' => $timetables,
                'departments' => $departments,
                'startDate' => $startDate->startOfWeek()->format('Y-m-d'),
                'endDate' => $endDate->endOfWeek()->format('Y-m-d')
            ];
            return view('print-timetable', $data);
        }
    }

    public function printView(Request $request)
    {
        //$session = Session::activeSession()->first();
        $session = Session::orderBy('id', 'desc')->first();
        $grade = isset($request->grade_id) ? $request->grade_id : null;
        $startDate = isset($request->from) ? new Carbon($request->from) : new Carbon();
        $endDate = isset($request->to) ? new Carbon($request->to) : new Carbon();
        $timetables = StudentTimetable::timetableFilter($startDate, $endDate, $session->id, $grade)->get()->groupBy(function ($timetable) {
            return Carbon::parse($timetable->date)->format('W');
        });
        return [$timetables];
        return view('print-view-timetable', []);
    }
}