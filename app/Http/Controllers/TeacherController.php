<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeacherRequest;
use App\Models\Department;
use App\Models\Subject;
use App\Models\Teacher;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = auth()->user()->role;
        if($role->slug == 'mbbs-student-affairs'){
            $department = Department::where('name', 'MBBS')->get();
        }
        if($role->slug == 'bds-student-affairs'){
            $department = Department::where('name', 'BDS')->get();
        } 
        if($role->slug == 'super-admin'){
            $department = Department::all();
        }

        if($role->slug == 'super-admin'){
            
            $teachers =Teacher::all();
        }else{
            $teachers = Teacher::where('department_id', $department[0]->id)->get();
        }
        return view('teachers', ['teachers' => $teachers, 'department' => $department]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {
        Teacher::create($request->all());
        return redirect()->route('teachers.read.index')->with('status', 'Teacher created successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        $role = auth()->user()->role;
        if($role->slug == 'mbbs-student-affairs'){
            $department = Department::where('name', 'MBBS')->get();
        }
        if($role->slug == 'bds-student-affairs'){
            $department = Department::where('name', 'BDS')->get();
        }
        if($role->slug == 'super-admin'){
            $department = Department::all();
        }

        $subjects = Subject::get();
        return view('edit-teacher', ['data' => $teacher, 'department' => $department, 'subjects' => $subjects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, $id)
    {
        $request->merge(['id' => $id]);
        Teacher::updateOrCreate(['id' => $id], [
            'department_id' => $request->department_id,
            'employee_number' => $request->employee_number,
            'full_name' => $request->full_name,
            'remarks' => $request->remarks,
            'pmdc_reg_no' => $request->pmdc_reg_no,
            'designation' => $request->designation,
            'mobile_number' => $request->mobile_number,
            'address' => $request->address,
            'qualification' => $request->qualification
        ]);
        return redirect()->back()->with('status', 'Teacher is Updated Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allData()
    {
        $teachers = Teacher::get(['teachers.id AS id', 'teachers.full_name AS text']);
        return response()->json($teachers->toArray());
    }

    public function allFingerprints()
    {
        $teachers = Teacher::get(['teachers.id AS id', 'teachers.full_name AS text', 'teachers.finger_print AS fingerprint']);
        return response()->json($teachers->toArray());
    }

    public function report()
    {
        $teachers = Teacher::all();
        return view('teacher-reports', ['teachers' => $teachers]);
    }
}
