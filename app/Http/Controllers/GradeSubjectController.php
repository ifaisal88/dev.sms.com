<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\GradeSubject;
use App\Models\Grade;
use App\Models\Subject;
use App\Http\Requests\GradeSubjectRequest;

class GradeSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = GradeSubject::all();
        $grades = Grade::all();
        $subjects = Subject::all();
        return view('grade-subjects', ['data' => $data, 'grades' => $grades, 'subjects' => $subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeSubjectRequest $request)
    {
        GradeSubject::create($request->all());
        return redirect()->back()->with('status', 'Grade-Subject created successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grades = Grade::all();
        $subjects = Subject::all();
        $data = GradeSubject::firstWhere('id', $id);
        return view('edit-grade-subject', ['data' => $data, 'grades' => $grades, 'subjects' => $subjects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GradeSubjectRequest $request, $id)
    {
        $gradeSubject = GradeSubject::find($id);
        $gradeSubject->grade_id = $request->grade_id;
        $gradeSubject->subject_id = $request->subject_id;
        $gradeSubject->status = $request->status;
        $gradeSubject->save();
        return redirect()->back()->with('status', 'Grade-Subject Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function classSubjects($grade){
        $subjects = Subject::whereHas('grades', function($query) use($grade){
            $query->where('grade_id', $grade->id);
        })->get(['subjects.id AS id', 'subjects.name AS text']);
        return response()->json(['items' => $subjects]);
    }
}
