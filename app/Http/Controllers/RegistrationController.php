<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\FingerPrintRequest;
use App\Http\Requests\StudentAttendanceRequest;
use App\Http\Requests\StudentFingerPrintRequest;
use App\Http\Resources\TeachersResource;
use App\Models\Student;
use App\Models\StudentAttendance;
use App\Models\StudentTimetable;
use App\Models\Teacher;
use Carbon\Carbon;
use Carbon\CarbonTimeZone;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function teacherFingerPrint(FingerPrintRequest $request)
    {
        try {
            $teacher = Teacher::where('employee_number', $request->employee_number)->first();
            if (!is_null($teacher)) {
                $update = $teacher->update(['finger_print' => $request->finger_print]);
                if ($update) {
                    return response()->json(['success' => 'figner print registered.']);
                } else {
                    return response()->json(['error' => 'finger print not registered.']);
                }
            } else {
                return response()->json(['error' => 'teracher not found.']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'unknown error contact admin']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function studentFingerPrint(StudentFingerPrintRequest $request)
    {
        try {
            $student = Student::where('registration_number', $request->registration_number)->first();
            if (!is_null($student)) {
                $update = $student->update(['finger_print' => $request->finger_print]);
                if ($update) {
                    return response()->json(['success' => 'figner print registered.']);
                } else {
                    return response()->json(['error' => 'finger print not registered.']);
                }
            } else {
                return response()->json(['error' => 'student not found.']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'unknown error contact admin']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function studentAttendance(StudentAttendanceRequest $request)
    {
        try {
            $date = Carbon::now('Asia/Karachi');
            $token = JWTAuth::getToken();
            $tokenInfo = JWTAuth::getPayload($token)->toArray();
            $timeTableId = $tokenInfo['time_table_id'];
            $teacherId = $tokenInfo['teacher_id'];
            // dd($teacherId);
            $attendance = StudentAttendance::create([
                'student_id' => $request->student_id,
                'student_timetable_id' => $timeTableId,
                'attendance_status' => 'present',
                'attendance_date' => $date->format('Y-m-d'),
                'created_by_id' => $teacherId,
                'attendance_marked_type' => true,
            ]);
            if ($attendance) {
                return response()->json(['success' => 'attendance marked.']);
            } else {
                return response()->json(['error' => 'attendance not marked.']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'unknown error contact admin']);
        }
    }

    public function teachersList()
    {
        $date =  Carbon::now('Asia/Karachi');
        $teachers = Teacher::whereHas('student_timetables', function ($query) use ($date) {
            $query->whereDate('date', $date)->whereTime('start_time', '<', $date)->whereTime('end_time', '>', $date);
        })->select(['id', 'employee_number', 'full_name', 'finger_print'])->get();
        return $teachers;
    }

    public function teachersListAll()
    {
        $teachers = Teacher::select(['id', 'employee_number', 'full_name', 'finger_print'])->get();
        return $teachers;
    }

    public function studentsList()
    {
        $date =  Carbon::now('Asia/Karachi');
        $token = JWTAuth::getToken();
        $tokenInfo = JWTAuth::getPayload($token)->toArray();
        if(!is_null($tokenInfo['time_table_id'])){
            $timeTable = StudentTimetable::find($tokenInfo['time_table_id']);
            $students = Student::where('grade_id', $timeTable->grade_id)->select(['id', 'registration_number', 'full_name', 'finger_print'])->get();
            return $students;
        }else{
            return 0;
        }
    }

    public function studentsListAll()
    {
        $students = Student::select(['id', 'registration_number', 'full_name', 'finger_print'])->get();
        return $students;
    }
}