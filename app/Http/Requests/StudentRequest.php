<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_number' => ['required', Rule::unique('students', 'registration_number')->ignore($this->student)],
            'department_id' => 'required',
            'grade_id' => 'required',
            'full_name' => 'required',
            'remarks' => 'nullable'
        ];
    }
}
