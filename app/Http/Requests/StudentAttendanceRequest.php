<?php

namespace App\Http\Requests;

use App\Models\Student;
use App\Models\StudentAttendance;
use App\Models\StudentTimetable;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Tymon\JWTAuth\Facades\JWTAuth;

class StudentAttendanceRequest extends FormRequest
{

    public function __construct()
    {
        app('validator')->extend('timetable_check', function ($attribute, $value, $parameters, $validator) {
            $date =  Carbon::now('Asia/Karachi');
            $token = JWTAuth::getToken();
            $apy = JWTAuth::getPayload($token)->toArray();
            $teacherId = $apy['teacher_id'];
            $timeTableId = $apy['time_table_id'];
            $student = Student::find($value);
            $timeTable = StudentTimetable::where('teacher_id', $teacherId)->where('grade_id', $student->grade_id)
                ->whereDate('date', $date)->whereTime('start_time', '<=', $date->format('H:i:s'))
                ->where('end_time', '>=', $date->format('H:i:s'))->first();
            if (!is_null($timeTable) && $timeTable->id == $timeTableId) {
                return true;
            } else {
                return false;
            }
        });

        app('validator')->extend('attendance_check', function ($attribute, $value, $parameters, $validator) {
            $date =  Carbon::now('Asia/Karachi');
            $token = JWTAuth::getToken();
            $apy = JWTAuth::getPayload($token)->toArray();
            $timeTableId = $apy['time_table_id'];
            $student = Student::find($value);
            $attendance = StudentAttendance::where('student_id', $student->id)->where('student_timetable_id', $timeTableId)->whereDate('attendance_date', $date)->count();
            if ($attendance == 0) {
                return true;
            } else {
                return false;
            }
        });
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required|string|timetable_check|attendance_check',
        ];
    }

    public function messages()
    {
        return [
            'student_id.timetable_check' => 'Student does not belong to this class.',
            'student_id.attendance_check' => 'Student attendance already marked.',
        ];
    }
}
