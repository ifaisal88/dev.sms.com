<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentTimetableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade_id' => 'required',
            'session_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'status' => 'required',
            'topic_name' => 'nullable',
            'start_time' => 'required',
            'end_time' => 'required',
            'date' => 'required',
            'day' => 'required',
        ];
    }
}
