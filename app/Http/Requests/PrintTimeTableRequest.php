<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrintTimeTableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade_id' => 'required|integer',
            'from' => 'required|date',
            'to' => 'required|date|after:from'
        ];
    }

    public function attributes()
    {
        return [
            'grade_id' => 'Class',
            'from' => 'Start Date',
            'to' => 'End Date',
        ];   
    }
}
