<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_number' =>  ['required', Rule::unique('teachers', 'employee_number')->ignore($this->teacher)],
            'full_name' => 'required',
            'pmdc_reg_no' =>  ['required', Rule::unique('teachers', 'pmdc_reg_no')->ignore($this->teacher)],
            'designation' => 'nullable',
            'qualification' => 'nullable',
            'remarks' => 'nullable'
        ];
    }
}
