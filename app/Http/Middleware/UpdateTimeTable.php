<?php

namespace App\Http\Middleware;

use App\Models\StudentTimetable;
use Closure;
use Illuminate\Http\Request;

class UpdateTimeTable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $timetbale = StudentTimetable::find($request->timetable);
        if($request->user()->role->slug == 'super-admin'){
            return $next($request);
        }elseif(($request->user()->role->slug == 'bds-admin' && $timetbale->grade->department->name == 'BDS')){
            return $next($request);
        }elseif($request->user()->role->slug == 'mbbs-admin' && $timetbale->grade->department->name == 'MBBS'){
            return $next($request);
        }else{
            abort(401, 'This action is unauthorized.');
        }

    }
}
