<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ReportsCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->role->slug == 'super-admin' || $request->user()->role->slug == 'report-admin' || auth()->user()->role->slug == 'admin' || $request->user()->role->slug == 'mbbs-attendance' || $request->user()->role->slug == 'bds-attendance') {
            return $next($request);
        } else {
            abort(401, 'This action is unauthorized.');
        }
    }
}
