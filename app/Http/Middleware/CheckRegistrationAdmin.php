<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRegistrationAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->role->slug == 'mbbs-student-affairs' || $request->user()->role->slug == 'bds-student-affairs' || $request->user()->role->slug == 'super-admin'){
            return $next($request);
        }else{
            abort(401, 'This action is unauthorized.');
        }
    }
}
