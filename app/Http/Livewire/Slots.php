<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Slot;

class Slots extends Component
{
    public $slots, $name, $start_time, $end_time, $status, $slot_id;
    public $isModalOpen = 0;

    public function render()
    {
        $this->slots = Slot::all();
        return view('livewire.slots');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->start_time = '';
        $this->end_time = '';
        $this->status = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'status' => 'required',
        ]);

        Slot::updateOrCreate(['id' => $this->slot_id], [
            'name' => $this->start_time,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'status' => $this->status,
        ]);

        session()->flash('message', $this->slot_id ? 'Slot updated.' : 'Slot created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $slot = Slot::findOrFail($id);
        $this->slot_id = $id;
        $this->name = $slot->name;
        $this->start_time = $slot->start_time;
        $this->end_time = $slot->end_time;
        $this->status = $slot->status;

        $this->openModalPopover();
    }

    public function delete($id)
    {
        Slot::find($id)->delete();
        session()->flash('message', 'Slot deleted.');
    }
}
