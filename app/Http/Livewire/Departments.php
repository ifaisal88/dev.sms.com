<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Department;

class Departments extends Component
{
    public $departments, $name, $status, $department_id;
    public $isModalOpen = '';

    public function render()
    {
        $this->departments = Department::all();
        return view('livewire.departments');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->status = '';
        $this->department_id = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        Department::updateOrCreate(['id' => $this->department_id], [
            'name' => $this->name,
            'status' => $this->status,
        ]);

        session()->flash('message', $this->department_id ? 'Department updated.' : 'Department created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $department = Department::findOrFail($id);
        $this->department_id = $id;
        $this->name = $department->name;
        $this->status = $department->status;
        $this->openModalPopover();
    }

    public function delete($id)
    {
        Department::find($id)->delete();
        session()->flash('message', 'Department deleted.');
    }
}
