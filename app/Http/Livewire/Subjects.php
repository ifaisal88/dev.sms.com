<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Subject;

class Subjects extends Component
{
    public $subjects, $name, $status, $subject_id;
    public $isModalOpen = 0;

    public function render()
    {
        $this->subjects = Subject::all();
        return view('livewire.subjects');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->status = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        Subject::updateOrCreate(['id' => $this->subject_id], [
            'name' => $this->name,
            'status' => $this->status,
        ]);

        session()->flash('message', $this->subject_id ? 'Subject updated.' : 'Subject created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $subject = Subject::findOrFail($id);
        $this->subject_id = $id;
        $this->name = $subject->name;
        $this->status = $subject->status;
        $this->openModalPopover();
    }

    public function delete($id)
    {
        Subject::find($id)->delete();
        session()->flash('message', 'Subject deleted.');
    }
}
