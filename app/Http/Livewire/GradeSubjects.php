<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\GradeSubject;
use App\Models\Grade;
use App\Models\Subject;

class GradeSubjects extends Component
{
    public $grade_subjects, $subject_id, $grade_id, $status, $grade_subject_id, $grades, $subjects;
    public $isModalOpen = 0;

    public function render()
    {
        $this->grade_subjects = GradeSubject::all();
        $this->grades = Grade::all();
        $this->subjects = Subject::all();
        return view('livewire.grade-subjects');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->subject_id = '';
        $this->grade_id = '';
        $this->status = '';
    }

    public function store()
    {
        $this->validate([
            'subject_id' => 'required',
            'grade_id' => 'required',
            'status' => 'required',
        ]);

        GradeSubject::updateOrCreate(['id' => $this->grade_subject_id], [
            'subject_id' => $this->subject_id,
            'grade_id' => $this->grade_id,
            'status' => $this->status,
        ]);

        session()->flash('message', $this->grade_subject_id ? 'Class-Subject updated.' : 'Class-Subject created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $grade = GradeSubject::findOrFail($id);
        $this->grade_subject_id = $id;
        $this->subject_id = $grade->subject_id;
        $this->grade_id = $grade->grade_id;
        $this->status = $grade->status;

        $this->openModalPopover();
    }

    public function delete($id)
    {
        GradeSubject::find($id)->delete();
        session()->flash('message', 'Class-Subject deleted.');
    }
}
