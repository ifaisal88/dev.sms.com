<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Session;

class Sessions extends Component
{
    public $sessions, $name, $status, $session_id;
    public $isModalOpen = 0;

    public function render()
    {
        $this->sessions = Session::all();
        return view('livewire.sessions');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->status = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        Session::updateOrCreate(['id' => $this->session_id], [
            'name' => $this->name,
            'status' => $this->status,
        ]);

        session()->flash('message', $this->session_id ? 'Session updated.' : 'Session created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $session = Session::findOrFail($id);
        $this->session_id = $id;
        $this->name = $session->name;
        $this->status = $session->status;

        $this->openModalPopover();
    }

    public function delete($id)
    {
        Session::find($id)->delete();
        session()->flash('message', 'Session deleted.');
    }
}
