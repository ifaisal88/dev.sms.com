<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Grade;

class Grades extends Component
{
    public $grades, $name, $status, $grade_id;
    public $isModalOpen = 0;

    public function render()
    {
        $this->grades = Grade::all();
        return view('livewire.grades');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->status = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        Grade::updateOrCreate(['id' => $this->grade_id], [
            'name' => $this->name,
            'status' => $this->status,
        ]);

        session()->flash('message', $this->grade_id ? 'Class updated.' : 'Class created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $grade = Grade::findOrFail($id);
        $this->grade_id = $id;
        $this->name = $grade->name;
        $this->status = $grade->status;

        $this->openModalPopover();
    }

    public function delete($id)
    {
        Grade::find($id)->delete();
        session()->flash('message', 'Class deleted.');
    }
}
