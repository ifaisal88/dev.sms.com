<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Teacher;

class Teachers extends Component
{
    public $teachers, $employee_number, $full_name, $address, $mobile_number, $remarks, $pmdc_reg_no, $designation, $qualification, $teacher_id;
    public $isModalOpen = 0;

    public function render()
    {
        $this->teachers = Teacher::all();
        return view('livewire.teachers');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->employee_number = '';
        $this->full_name = '';
        $this->address = '';
        $this->mobile_number = '';
        $this->remarks = '';
        $this->pmdc_reg_no = '';
        $this->disignation = '';
        $this->qualification = '';
    }

    public function store()
    {
        $this->validate([
            'employee_number' => 'required',
            'full_name' => 'required',
            'address' => 'required',
            'mobile_number' => 'required',
            'remarks' => 'required',
            'pmdc_reg_no' => 'required',
            'disignation' => 'required',
            'qualification' => 'required',
        ]);

        Teacher::updateOrCreate(['id' => $this->student_id], [
            'registration_no' => $this->registration_no,
            'full_name' => $this->full_name,
            'address' => $this->address,
            'mobile_number' => $this->mobile_number,
            'remarks' => $this->remarks,
        ]);

        session()->flash('message', $this->student_id ? 'Teacher updated.' : 'Teacher created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $grade = Teacher::findOrFail($id);
        $this->grade_subject_id = $id;
        $this->subject_id = $grade->subject_id;
        $this->grade_id = $grade->grade_id;
        $this->status = $grade->status;

        $this->openModalPopover();
    }

    public function delete($id)
    {
        Teacher::find($id)->delete();
        session()->flash('message', 'Teacher deleted.');
    }
}
