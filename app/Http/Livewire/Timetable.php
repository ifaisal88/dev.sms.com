<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\StudentTimetable;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Session;

class Timetable extends Component
{
    public $day, $grades, $grade_id, $subjects, $subject_id, $teachers, $teacher_id, $start_time, $end_time, $date, $timetable_id, $timetable, $session, $session_id, $status;

    public function render()
    {
        $this->session = Session::all();
        $this->grades = Grade::all();
        $this->subjects = Subject::all();
        $this->teachers = Teacher::all();
        $this->timetable = StudentTimetable::get();
        return view('livewire.timetable');
    }

    public function create()
    {
        $this->resetCreateForm();
        // $this->openModalPopover();
    }

    // public function openModalPopover()
    // {
    //     $this->isModalOpen = true;
    // }

    // public function closeModalPopover()
    // {
    //     $this->isModalOpen = false;
    // }

    private function resetCreateForm(){
        $this->grade_id = '';
        $this->session_id = '';
        $this->subject_id = '';
        $this->teacher_id = '';
        $this->status = '';
        $this->start_time = '';
        $this->end_time = '';
        $this->date = '';
        $this->day = '';
    }

    public function store()
    {
        $this->validate([
            'grade_id' => 'required',
            'session_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'status' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'date' => 'required',
            'day' => 'required',
        ]);
        dd($this);
        StudentTimetable::updateOrCreate(['id' => $this->timetable_id], [
            'grade_id' => $this->grade_id,
            'session_id' => $this->session_id,
            'subject_id' => $this->subject_id,
            'teacher_id' => $this->teacher_id,
            'status' => $this->status,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'date' => $this->date,
            'day' => $this->day,
        ]);

        session()->flash('message', $this->timetable_id ? 'Timetable updated.' : 'Timetable created.');

        // $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $timetable = StudentTimetable::findOrFail($id);
        $this->timetable_id = $id;
        $this->grade_id = $timetable->grade_id;
        $this->session_id = $timetable->session_id;
        $this->subject_id = $timetable->subject_id;
        $this->teacher_id = $timetable->teacher_id;
        $this->status = $timetable->status;
        $this->start_time = $timetable->start_time;
        $this->end_time = $timetable->end_time;
        $this->date = $timetable->date;
        $this->day = $timetable->day;
        // $this->openModalPopover();
    }

    public function delete($id)
    {
        Subject::find($id)->delete();
        session()->flash('message', 'Timetable deleted.');
    }

}
