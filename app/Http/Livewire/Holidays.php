<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Holiday;

class Holidays extends Component
{
    public $holidays, $name, $holiday_date, $status, $holiday_id;
    public $isModalOpen = '';

    public function render()
    {
        $this->holidays = Holiday::all();
        return view('livewire.holidays');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->status = '';
        $this->holiday_date = '';
        $this->holiday_id = '';
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'status' => 'required',
            'holiday_date' => 'required|date',
        ]);

        Holiday::updateOrCreate(['id' => $this->holiday_id], [
            'name' => $this->name,
            'status' => $this->status,
            'holiday_date' => $this->holiday_date,
        ]);

        session()->flash('message', $this->holiday_id ? 'Holiday updated.' : 'Holiday created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $holiday = Holiday::findOrFail($id);
        $this->holiday_id = $id;
        $this->name = $holiday->name;
        $this->status = $holiday->status;
        $this->holiday_date = $holiday->holiday_date;
        $this->openModalPopover();
    }

    public function delete($id)
    {
        Holiday::find($id)->delete();
        session()->flash('message', 'Holiday deleted.');
    }
}
