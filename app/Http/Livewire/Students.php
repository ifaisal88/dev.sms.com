<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Student;
use App\Models\Department;
use App\Models\Grade;

class Students extends Component
{
    public $students, $registration_number, $full_name, $address, $mobile_number, $remarks, $student_id, $departments, $department_id, $grades, $grade_id;
    public $isModalOpen = 0;

    public function render()
    {
        $this->students = Student::with('departments')->orderBy('registration_number', 'DESC')->get();
        $this->departments = Department::all();
        $this->grades = Grade::all();

        return view('livewire.students');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm()
    {
        $this->registration_number = '';
        $this->department_id = '';
        $this->grade_id = '';
        $this->full_name = '';
        $this->address = '';
        $this->mobile_number = '';
        $this->remarks = '';
    }

    public function store()
    {
        $this->validate([
            'registration_number' => 'required',
            'department_id' => 'required',
            'grade_id' => 'required',
            'full_name' => 'required',
            'remarks' => 'required',
        ]);
        $student = Student::updateOrCreate(['id' => $this->student_id], [
            'registration_number' => $this->registration_number,
            'full_name' => $this->full_name,
            'address' => $this->address,
            'mobile_number' => $this->mobile_number,
            'remarks' => $this->remarks,
        ]);
        // $student->depratments()->attach([$this->department_id]);

        session()->flash('message', $this->student_id ? 'Student updated.' : 'Student created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $grade = Student::findOrFail($id);
        $this->grade_subject_id = $id;
        $this->subject_id = $grade->subject_id;
        $this->grade_id = $grade->grade_id;
        $this->status = $grade->status;

        $this->openModalPopover();
    }

    public function delete($id)
    {
        Student::find($id)->delete();
        session()->flash('message', 'Student deleted.');
    }
}
