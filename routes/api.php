<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function ($router) {
    Route::group(['middleware' => 'auth:apiTeacher'], function () {
        Route::post('/refresh', [AuthController::class, 'refresh']);
        Route::get('/teacher', [AuthController::class, 'userProfile']);
        Route::get('/current/students', [RegistrationController::class, 'studentsList']);
        Route::post('/student-attendance', [RegistrationController::class, 'studentAttendance']);
    });

    Route::group(['middleware' => 'auth:apiAdmin'], function () {
        Route::post('/teachers/finger-print', [RegistrationController::class, 'teacherFingerPrint']);
        Route::post('/students/finger-print', [RegistrationController::class, 'studentFingerPrint']);
    });

    Route::get('/current/teachers', [RegistrationController::class, 'teachersList']);
    Route::get('/current/teachersAll', [RegistrationController::class, 'teachersListAll']);
    Route::get('/current/studentsAll', [RegistrationController::class, 'studentsListAll']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
});
