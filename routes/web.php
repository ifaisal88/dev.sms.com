<?php

use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\GradeSubjectController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TimeTableController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\SubjectController;
use App\Models\StudentAttendance;
use Illuminate\Support\Facades\Route;

require __DIR__ . '/auth.php';

Route::group(['prefix' => 'timetable', 'as' => 'timetable.'], function () {
    Route::get('/print', [TimeTableController::class, 'print'])->name('print');
    Route::post('/print', [TimeTableController::class, 'printFilter'])->name('print.filter');
    Route::get('/view', [TimeTableController::class, 'printView'])->name('print.view');
    Route::get('/', [TimeTableController::class, 'index'])->name('read.index');
    Route::post('/import', [TimeTableController::class, 'import'])->name('store.import');
    Route::get('/export', [TimeTableController::class, 'export'])->name('read.export');
    Route::post('/', [TimeTableController::class, 'store'])->name('submit.store');
    Route::get('/{timetable}/edit', [TimeTableController::class, 'edit'])->name('submit.edit');
    Route::put('/{timetable}/update', [TimeTableController::class, 'update'])->name('submit.update');
});

Route::group(['prefix' => 'public', 'as' => 'public.'], function () {
    Route::get('/allteachers/fingerprints', [TeacherController::class, 'allFingerprints'])->name('read.fingerprints');
    Route::get('/allstudents/fingerprints', [StudentController::class, 'allFingerprints'])->name('read.fingerprints');
});


Route::middleware(['auth'])->group(function () {

    Route::get('/', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::group(['prefix' => 'departments', 'as' => 'departments.'], function () {
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/', [DepartmentController::class, 'index'])->name('read.index');
            Route::post('/', [DepartmentController::class, 'store'])->name('submit.store');
            Route::get('/{department}/edit', [DepartmentController::class, 'edit'])->name('submit.edit');
            Route::patch('/{department}/update', [DepartmentController::class, 'update'])->name('submit.update');
        });
        Route::get('/all/data', [DepartmentController::class, 'allData'])->name('read.data');
        Route::get('/{department}/class', [DepartmentController::class, 'departmentClasses'])->name('read.classes');
    });

    Route::group(['prefix' => 'grades', 'as' => 'grades.'], function () {
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/', [GradeController::class, 'index'])->name('read.index');
            Route::post('/', [GradeController::class, 'store'])->name('submit.store');
            Route::get('/{grade}/edit', [GradeController::class, 'edit'])->name('submit.edit');
            Route::patch('/{grade}/update', [GradeController::class, 'update'])->name('submit.update');
            Route::get('/{grade}/students', [GradeController::class, 'students'])->name('read.students');
            Route::get('/attendance', [GradeController::class, 'gradesAttendacne'])->name('read.attendacne');
        });

        Route::get('/{grade}/subjects', [GradeController::class, 'subjects'])->name('read.subjects');
    });

    Route::group(['prefix' => 'subjects', 'as' => 'subjects.'], function () {
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/', [SubjectController::class, 'index'])->name('read.index');
            Route::post('/', [SubjectController::class, 'store'])->name('submit.store');
            Route::get('/{subject}/edit', [SubjectController::class, 'edit'])->name('submit.edit');
            Route::patch('/{subject}/update', [SubjectController::class, 'update'])->name('submit.update');
            Route::get('/{subject}/teachers', [SubjectController::class, 'teachers'])->name('read.teachers');
        });
        Route::get('/all/data', [SubjectController::class, 'allData'])->name('read.data');
    });

    Route::group(['prefix' => 'sessions', 'as' => 'sessions.', 'middleware' => 'admin'], function () {
        Route::get('/', [SessionController::class, 'index'])->name('read.index');
        Route::post('/', [SessionController::class, 'store'])->name('submit.store');
        Route::get('/{session}/edit', [SessionController::class, 'edit'])->name('submit.edit');
        Route::patch('/{session}/update', [SessionController::class, 'update'])->name('submit.update');
    });

    Route::group(['prefix' => 'gradeSubjects', 'as' => 'gradeSubjects.', 'middleware' => 'admin'], function () {
        Route::get('/', [GradeSubjectController::class, 'index'])->name('read.index');
        Route::post('/', [GradeSubjectController::class, 'store'])->name('submit.store');
        Route::get('/{id}/edit', [GradeSubjectController::class, 'edit'])->name('submit.edit');
        Route::patch('/{id}/update', [GradeSubjectController::class, 'update'])->name('submit.update');
    });

    Route::group(['prefix' => 'students', 'as' => 'students.', 'middleware' => 'student'], function () {
        Route::get('/', [StudentController::class, 'index'])->name('read.index');
        Route::post('/', [StudentController::class, 'store'])->name('submit.store');
        Route::post('/manual-attendance', [StudentController::class, 'attendance'])->name('store.attendance');
        Route::post('/finalize-attendance', [StudentController::class, 'finalAttendance'])->name('store.finalAttendance');
        Route::get('/{student}/edit', [StudentController::class, 'edit'])->name('submit.edit');
        Route::put('/{student}/update', [StudentController::class, 'update'])->name('submit.update');
        Route::get('/promote', [StudentController::class, 'loadStudents'])->name('load.students');
        Route::post('/grade', [StudentController::class, 'showStudents'])->name('show.students');
        Route::get('/grade/{id}', [StudentController::class, 'gradeStudents'])->name('grade.students');
        Route::post('/gradeUpdate', [StudentController::class, 'updateStudentGrade'])->name('update.grade');
    });

    Route::group(['prefix' => 'admissions', 'as' => 'admissions.'], function () {
        Route::get('/', function () {
            return view('admission-form');
        })->name('read.index');
    });

    Route::group(['prefix' => 'teachers', 'as' => 'teachers.', 'middleware' => 'student'], function () {
        Route::get('/all/data', [TeacherController::class, 'allData'])->name('read.data');
        // Route::get('/all/fingerprints', [TeacherController::class, 'allFingerprints'])->name('read.fingerprints');
        Route::get('/', [TeacherController::class, 'index'])->name('read.index');
        Route::post('/', [TeacherController::class, 'store'])->name('submit.store');
        Route::get('/{teacher}/edit', [TeacherController::class, 'edit'])->name('submit.edit');
        Route::put('/{teacher}/update', [TeacherController::class, 'update'])->name('submit.update');
    });

    Route::group(['prefix' => 'newTeachers', 'as' => 'newTeachers.'], function () {
        Route::get('/', function () {
            return view('new-teacher');
        })->name('read.index');
    });

    Route::group(['prefix' => 'holidays', 'as' => 'holidays.', 'middleware' => 'admin'], function () {
        Route::get('/', function () {
            return view('holidays');
        })->name('read.index');
    });

    // Route::group(['prefix' => 'timetable', 'as' => 'timetable.', 'middleware' => 'timetable'], function () {
    //     Route::get('/print', [TimeTableController::class, 'print'])->name('print');
    //     Route::post('/print', [TimeTableController::class, 'printFilter'])->name('print.filter');
    //     Route::get('/view', [TimeTableController::class, 'printView'])->name('print.view');
    //     Route::get('/', [TimeTableController::class, 'index'])->name('read.index');
    //     Route::post('/import', [TimeTableController::class, 'import'])->name('store.import');
    //     Route::get('/export', [TimeTableController::class, 'export'])->name('read.export');
    //     Route::post('/', [TimeTableController::class, 'store'])->name('submit.store');
    //     // Route::get('/{timetable}/edit', [TimeTableController::class, 'edit'])->name('submit.edit')->middleware('timetableUdpate');
    //     // Route::put('/{timetable}/update', [TimeTableController::class, 'update'])->name('submit.update')->middleware('timetableUdpate');
    //     Route::get('/{timetable}/edit', [TimeTableController::class, 'edit'])->name('submit.edit');
    //     Route::put('/{timetable}/update', [TimeTableController::class, 'update'])->name('submit.update');
    // });

    Route::group(['middleware' => 'superAdmin'], function () {
        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/', [UserController::class, 'index'])->name('read.index');
            Route::post('/', [UserController::class, 'store'])->name('submit.store');
            Route::get('/{user}/edit', [UserController::class, 'edit'])->name('submit.edit');
            Route::patch('/{user}/update', [UserController::class, 'update'])->name('submit.update');
        });

        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function () {
            Route::get('/all/data', [RoleController::class, 'allData'])->name('read.allData');
        });
    });

    Route::group(['prefix' => 'reports', 'as' => 'reports.', 'middleware' => 'reportAdmin'], function () {
        Route::get('/students', [StudentController::class, 'report'])->name('read.students');
        Route::get('/teachers', [TeacherController::class, 'report'])->name('read.teachers');
        Route::get('/attendance', [AttendanceController::class, 'index'])->name('read.attendance');
        // Route::get('/attendance/print', [AttendanceController::class, 'index'])->name('read.attendance');
        Route::get('/attendance/print', function () {
            return view('attendance-reports');
        })->name('read.attendancePrint');
        Route::post('/attendance', [AttendanceController::class, 'attendanceList'])->name('read.attendanceList');
        Route::get('grades/{grade}/subjects', [GradeSubjectController::class, 'classSubjects'])->name('read.subjects');
    });
});
